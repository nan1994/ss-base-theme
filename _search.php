<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package aa-base
 */


get_header();
$s = get_search_query();
$args =  array( 'posts_per_page' => -1, 's' => esc_attr( $s ), 'ajax_search' => true);

$the_query = new WP_Query();
$the_query->parse_query( $args );

relevanssi_do_query( $the_query );

$posts_found = array();
$posts_titles = array();
?>

<main id="main" class="site-main">
	<div class="col-main col-xl-8 offset-xl-2" >
<?php 
		if( $the_query->have_posts() ) :
			while( $the_query->have_posts() ): $the_query->the_post();
			
					ob_start();
						if(get_post_type() == 'product'){
							include get_template_directory().'/woocommerce/content-product.php';
						}else{
							include get_template_directory() . '/template-parts/courses/course-box.php';
						}
					$posts_found[get_post_type()][] = ob_get_clean();
			endwhile;
		endif;

		
		ksort($posts_found);

		
		
		if(isset($_GET['s']) && !empty($_GET['s'])): ?>
					<h1 class="page-title"><?php printf( esc_html__( 'Search results for: %s', 'ss_text_domain' ), '<span>"' . $_GET['s'] . '"</span>' ); ?></h1>
		<?php endif;
				
		foreach($posts_found as $post_type => $post_found){
			print '<h4 class="archive-heading small-heading">'.get_post_type_object($post_type)->labels->menu_name.'</h4>';
			print '<div class="listing-row"><div id="results-grid" class="row">';
				foreach($post_found as $post){
					print $post;
				}
			print '</div></div>';
		}
?>
	</div>
</main>

<?php
get_footer();
