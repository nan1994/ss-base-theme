<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="post__thumbnail">

		<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('thumbnail'); } else { echo "<img src='http://placehold.it/320x160' alt=''>"; } ?></a>

		</div><!-- /post-image --><div class="post__details">
			<h2 class="post__title">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			</h2>

			<time class="date"><?php the_time('F j, Y'); ?></time>
			<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>

		</div>
	</article>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
