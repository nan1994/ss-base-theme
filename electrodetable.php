<?php
	//global $includes;
	//global $product;
	//global $wpdb;

	$sprog=ICL_LANGUAGE_CODE;

	//if ($product['item'] == '07-512') echo $includes;
	//echo ($product['item'] . ' | ' . $product['includes']);

	$listitems = explode(',',$product['includes']);
	$listitems = array_map( 'trim', $listitems );
	//If its an array, convert to string
    if( is_array( $listitems ) ){ // workaround to allow wpdb->prepare to handle array of strings
    	$ListitemsCount = count($listitems);
    	$stringPlaceholders = array_fill(0, $ListitemsCount, '%s');
    	$placeholdersForlistitems = implode(', ', $stringPlaceholders);
        //$listitems = "'" . implode( ", '", $listitems ) . "'"; //e.g. "publish, draft"
        //echo $placeholdersForlistitems;
    }

	if ($sprog == 'de' ) {		
			//$sql = "SELECT *, nameDE as name FROM electrodes WHERE left(itemnumber,6) in (".$includes.") and type in ( '1' , '2' ) ORDER BY itemnumber asc";	
			$sql = $wpdb->prepare( 
				"SELECT *, nameDE as name FROM electrodes WHERE left(itemnumber,6) in ($placeholdersForlistitems) and type in ( '1' , '2' ) ORDER BY itemnumber asc",
				$listitems);
		}

	elseif ($sprog == 'fr' ) {	
			//$sql = "SELECT *, nameUK as name FROM electrodes WHERE left(itemnumber,6) in (".$includes.") and type in ( '1' , '2' ) ORDER BY itemnumber asc";
			$sql = $wpdb->prepare( 
				"SELECT *, nameFR as name FROM electrodes WHERE left(itemnumber,6) in ($placeholdersForlistitems) and type in ( '1' , '2' ) ORDER BY itemnumber asc",
				$listitems);
		}

	else	{		
			//$sql = "SELECT *, nameUK as name FROM electrodes WHERE left(itemnumber,6) in (".$includes.") and type in ( '1' , '2' ) ORDER BY itemnumber asc";
			$sql = $wpdb->prepare( 
				"SELECT *, nameUK as name FROM electrodes WHERE left(itemnumber,6) in ($placeholdersForlistitems) and type in ( '1' , '2' ) ORDER BY itemnumber asc",
				$listitems);
		}		

	$variants = $wpdb->get_results($sql,ARRAY_A);?>

	<div class="electrode_title"><?=$product['item']?> - <?=$product['webname']?></div>
	<div class="electrode_descr"><?=$product['description_short']?></div>
	<table width='100%' class='electrode_table'>
		<thead>
	        <th><?php _e('Order#','html5blank')?></th>
	        <th><?php _e('Description','html5blank')?></th>
	        <th><?php _e('From stock','html5blank')?></th>
	        <th><?php _e('Properties','html5blank')?></th>
	        <th><?php _e('Comments','html5blank')?></th>
        </thead>
        <tbody><?php 
	        foreach( $variants as $row ) : ?>
			<tr>
				<td><?=$row['itemnumber']?></td>
				<td><?=$row['name']?></td>
	            <td><?php 
					if ($row ['stock']== 'Yes') {	echo "<div class='iconsprite yes' title='".__('Delivered from stock','html5blank')."'></div>";}
					else {							echo "<div class='iconsprite call' title='".__('Produced to order - Call for quote','html5blank')."'></div>";}?>
				</td>
				<td ><?php
					if ($row['perforering'] <> NULL ) 	echo "<div class='iconsprite perforating' title='".__('Perforating action','html5blank')."'></div>";
					if ($row['lokke'] <> NULL ) 		echo "<div class='iconsprite punch' title='".__('Punching Action','html5blank')."'></div>";
					if ($row['singlesided'] <> NULL ) 	echo "<div class='iconsprite ssided' title='".__('Singlesided welding','html5blank')."'></div>";
					if ($row['doublesided'] <> NULL ) 	echo "<div class='iconsprite dsided' title='".__('Doublesided welding','html5blank')."'></div>";?>				
				</td>
				<td><?=$row['fitswith'] // findelectrodeusedwith($row['itemnumber'])?>
				</td>
			</tr><?php
	        endforeach?>
    	</tbody>
    </table>
