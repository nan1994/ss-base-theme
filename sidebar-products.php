<?php
  $language_extensions = ICL_LANGUAGE_CODE=='da' ? '_' : '_' . ICL_LANGUAGE_CODE . '_';

  $newsletter_text = get_option('options'. $language_extensions .'newsletter_text_text') ? get_option('options'. $language_extensions .'newsletter_text_text') : 'News that helps build your business';
  //$newsletter_text = _x('Sign up for news from Carmo','html5blank');
  $newsletter_link = get_option('options'. $language_extensions .'newsletter_text_link') ? get_option('options'. $language_extensions .'newsletter_text_link') : 'News that helps build your business';
  //Cases
  $sidebar_labels_cases_label = get_option('options'. $language_extensions .'sidebar_labels_cases_label') ? get_option('options'. $language_extensions .'sidebar_labels_cases_label') : 'Cases';
  $sidebar_labels_all_cases_link = get_option('options'. $language_extensions .'sidebar_labels_all_cases_link') ? get_option('options'. $language_extensions .'sidebar_labels_all_cases_link') : '/cases';
  $sidebar_labels_all_cases_label = get_option('options'. $language_extensions .'sidebar_labels_all_cases_label') ? get_option('options'. $language_extensions .'sidebar_labels_all_cases_label') : 'All Cases';

  $general_info = get_field('general_information', 'option');
  $newsletter = get_field('newsletter_text', 'option');
?>
<aside class="sidebar" role="complementary">

  <h5 class="widget-title"><?php _e('Contact Sales','html5blank');?></h5>

  <div class="contacts">
    <?php get_template_part('loop-contacts'); ?>
  </div><!-- /contacts -->

  <div class="box--related">
    <?php get_template_part('loop-related'); // Show relatedproducts based on webcategory keyword ?>
  </div><!-- /related -->
		
    <?php get_template_part('/template-parts/box-video');// Show video from Wistia based on webcategory keyword ?>


	 <!-- Newsletter Signup -->
    <a class="box box--signup" href="<?= $newsletter_link; ?>">
          <h3 class="title"><?php _e('Newsletter','html5blank');?></h3><p><?php echo($newsletter_text);?></p>
    </a>
	
    <!-- CASES -->
    <div class="box" style="padding-left:0px;">
        <h5 class="widget-title"><?= $sidebar_labels_cases_label; ?></h5>
            <?php
                $cases_id = icl_object_id(1417, 'page', false,ICL_LANGUAGE_CODE); // Get the id of the correct language version
                $args = array(
                    'post_type' => 'page',
                    'post_parent' => $cases_id,
                    'posts_per_page' => 3
                );
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() && is_technical() ) : 
                    while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <?php $case_image = get_field('case_image') ?>
                        <a href="<?= get_the_permalink(); ?>">
                            <div class="cases-box">
                                <div class="img-wrap">
                                    <!--img src="<?= $case_image ?>" alt=""-->
                                    <img src="<?=rtrim(site_url(),"/").'/thumbs/60x34xZC/'.ltrim(parse_url($case_image, PHP_URL_PATH),"/")?>" width="60" height="34" alt="<?= get_the_title(); ?>" >
                                </div>
                                <div class="title" style="text-transform: none; margin-bottom: 10px;"><?= get_the_title(); ?></div>
                            </div>
                        </a>
                    <?php endwhile;
                endif;
                wp_reset_postdata();  
            ?>
        <div class="btn-link">
            <a href="<?= $sidebar_labels_all_cases_link; ?>"><?= $sidebar_labels_all_cases_label; ?></a>
        </div>
    </div>
  </div>
	
  <?php //if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-products')) ?>

</aside><!-- /sidebar -->