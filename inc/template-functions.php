<?php
/**
 * Functions which enhance the theme by hooking into WordPress
*/
function support_revisions_for_pages() {
	add_post_type_support( 'page', 'revisions' );
	add_post_type_support( 'post', 'revisions' );
}
add_action( 'init', 'support_revisions_for_pages' );


if (function_exists('acf_set_options_page_menu')){
	if( function_exists('acf_set_options_page_title') ) {
		acf_set_options_page_title( __('Website Options', 'ss_text_domain') );
	}
}


function redirect_page_from_the_field() {
	if(get_field('redirect_to')){
		wp_redirect(get_permalink(get_field('redirect_to')[0]->ID), 301);
	}
}
add_action('template_redirect', 'redirect_page_from_the_field');


function main_wrap_start(){
	?>
	<section class="page-content-box">
		<div class="container">
			<div class="row">
	<?php
}

function main_wrap_end(){
	?>
			</div>
		</div>
	</section>
	<?php
}


function content_layout_start(){
	?>
		<div class="col-main order-1 order-xl-2 col-xl-9">
			<div class="main-box">
	<?php
}

function content_layout_end(){
	?>
		</div>
	</div>
	<?php
}

function sidebar_layout_start(){
	?>
	<aside class="col-sidebar order-2 order-xl-1 col-xl-3">
		<div class="main-box">
	<?php
}

function sidebar_layout_end(){
	?>
		</div>
	</aside>
	<?php
}



function ea_disable_editor( $id = false ) {

	$excluded_templates = array(
		'template-product.php',
	);

	$excluded_ids = array(
		// get_option( 'page_on_front' )
	);

	if( empty( $id ) )
		return false;

	$id = intval( $id );
	$template = get_page_template_slug( $id );

	return in_array( $id, $excluded_ids ) || in_array( $template, $excluded_templates );
}

function ea_disable_gutenberg( $can_edit, $post_type ) {

	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;

	if( ea_disable_editor( $_GET['post'] ) )
		$can_edit = false;

	return $can_edit;

}
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );

/* function ea_disable_classic_editor() {

	$screen = get_current_screen();
	if( 'page' !== $screen->id || ! isset( $_GET['post']) )
		return;

	if( ea_disable_editor( $_GET['post'] ) ) {
		remove_post_type_support( 'page', 'editor' );
	}

}
add_action( 'admin_head', 'ea_disable_classic_editor' ); */