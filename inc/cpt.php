<?php 
//include the php scripts from the CPT folder here to activate the custom post types
//include 'cpt/kursus.php';


/*------------------------------------*\
    Contacts Custom Post Type
\*------------------------------------*/

// Register Custom Post Type
function carmo_contacts() {

    $labels = array(
        'name'                => _x( 'Contacts', 'Post Type General Name', 'html5blank' ),
        'singular_name'       => _x( 'Contact', 'Post Type Singular Name', 'html5blank' ),
        'menu_name'           => __( 'Contacts', 'html5blank' ),
        'parent_item_colon'   => __( '', 'html5blank' ),
        'all_items'           => __( 'All Contacts', 'html5blank' ),
        'view_item'           => __( 'View Contact', 'html5blank' ),
        'add_new_item'        => __( 'Add New Contact', 'html5blank' ),
        'add_new'             => __( 'New Contact', 'html5blank' ),
        'edit_item'           => __( 'Edit Contact', 'html5blank' ),
        'update_item'         => __( 'Update Contact', 'html5blank' ),
        'search_items'        => __( 'Search contacts', 'html5blank' ),
        'not_found'           => __( 'No Contacts found', 'html5blank' ),
        'not_found_in_trash'  => __( 'No Contacts found in Trash', 'html5blank' ),
    );
    $args = array(
        'label'               => __( 'contact', 'html5blank' ),
        'description'         => __( 'Sales Contacts', 'html5blank' ),
        'labels'              => $labels,
        'supports'            => array( 'title', ),
		'taxonomies'            => array( 'category' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 70,
        'menu_icon'           => 'dashicons-admin-users',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
    register_post_type( 'contact', $args );

}

// Hook into the 'init' action
add_action( 'init', 'carmo_contacts', 0 );


// // Register Custom Post Type

// function carmo_case() {

// 	$labels = array(
// 		'name'                  => _x( 'Cases', 'Post Type General Name', 'html5blank' ),
// 		'singular_name'         => _x( 'Case', 'Post Type Singular Name', 'html5blank' ),
// 		'menu_name'             => __( 'Cases', 'html5blank' ),
// 		'name_admin_bar'        => __( 'Case', 'html5blank' ),
// 		'archives'              => __( 'Item Archives', 'html5blank' ),
// 		'attributes'            => __( 'Item Attributes', 'html5blank' ),
// 		'parent_item_colon'     => __( 'Parent Item:', 'html5blank' ),
// 		'all_items'             => __( 'All Cases', 'html5blank' ),
// 		'add_new_item'          => __( 'Add New Case', 'html5blank' ),
// 		'add_new'               => __( 'Add New', 'html5blank' ),
// 		'new_item'              => __( 'New Case', 'html5blank' ),
// 		'edit_item'             => __( 'Edit Case', 'html5blank' ),
// 		'update_item'           => __( 'Update Case', 'html5blank' ),
// 		'view_item'             => __( 'View Case', 'html5blank' ),
// 		'view_items'            => __( 'View Cases', 'html5blank' ),
// 		'search_items'          => __( 'Search Case', 'html5blank' ),
// 		'not_found'             => __( 'Not found', 'html5blank' ),
// 		'not_found_in_trash'    => __( 'Not found in Trash', 'html5blank' ),
// 		'featured_image'        => __( 'Featured Image', 'html5blank' ),
// 		'set_featured_image'    => __( 'Set featured image', 'html5blank' ),
// 		'remove_featured_image' => __( 'Remove featured image', 'html5blank' ),
// 		'use_featured_image'    => __( 'Use as featured image', 'html5blank' ),
// 		'insert_into_item'      => __( 'Insert into item', 'html5blank' ),
// 		'uploaded_to_this_item' => __( 'Uploaded to this case', 'html5blank' ),
// 		'items_list'            => __( 'Items list', 'html5blank' ),
// 		'items_list_navigation' => __( 'Items list navigation', 'html5blank' ),
// 		'filter_items_list'     => __( 'Filter items list', 'html5blank' ),
// 	);
// 	$args = array(
// 		'label'                 => __( 'Case', 'html5blank' ),
// 		'description'           => __( 'Case information page.', 'html5blank' ),
// 		'labels'                => $labels,
// 		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions' ),
// 		'taxonomies'            => array( 'category' ),
// 		'hierarchical'          => false,
// 		'public'                => true,
// 		'show_ui'               => true,
// 		'show_in_menu'          => true,
// 		'menu_position'         => 71,
// 		'menu_icon'             => 'dashicons-admin-comments',
// 		'show_in_admin_bar'     => true,
// 		'show_in_nav_menus'     => true,
// 		'can_export'            => true,
// 		'has_archive'           => true,
// 		'exclude_from_search'   => false,
// 		'publicly_queryable'    => true,
// 		'capability_type'       => 'page',
// 	);
// 	register_post_type( 'case', $args );

// }
// add_action( 'init', 'carmo_case', 0 );


// // Register Custom Post Type

// function products_post_type() {

// 	$labels = array(
// 		'name'                  => _x( 'Products', 'Post Type General Name', 'html5blank' ),
// 		'singular_name'         => _x( 'Product', 'Post Type Singular Name', 'html5blank' ),
// 		'menu_name'             => __( 'Products', 'html5blank' ),
// 		'name_admin_bar'        => __( 'Product', 'html5blank' ),
// 		'archives'              => __( 'Item Archives', 'html5blank' ),
// 		'attributes'            => __( 'Item Attributes', 'html5blank' ),
// 		'parent_item_colon'     => __( 'Parent Product:', 'html5blank' ),
// 		'all_items'             => __( 'All Products', 'html5blank' ),
// 		'add_new_item'          => __( 'Add New Product', 'html5blank' ),
// 		'add_new'               => __( 'New Product', 'html5blank' ),
// 		'new_item'              => __( 'New Item', 'html5blank' ),
// 		'edit_item'             => __( 'Edit Product', 'html5blank' ),
// 		'update_item'           => __( 'Update Product', 'html5blank' ),
// 		'view_item'             => __( 'View Product', 'html5blank' ),
// 		'view_items'            => __( 'View Items', 'html5blank' ),
// 		'search_items'          => __( 'Search products', 'html5blank' ),
// 		'not_found'             => __( 'No products found', 'html5blank' ),
// 		'not_found_in_trash'    => __( 'No products found in Trash', 'html5blank' ),
// 		'featured_image'        => __( 'Featured Image', 'html5blank' ),
// 		'set_featured_image'    => __( 'Set featured image', 'html5blank' ),
// 		'remove_featured_image' => __( 'Remove featured image', 'html5blank' ),
// 		'use_featured_image'    => __( 'Use as featured image', 'html5blank' ),
// 		'insert_into_item'      => __( 'Insert into item', 'html5blank' ),
// 		'uploaded_to_this_item' => __( 'Uploaded to this item', 'html5blank' ),
// 		'items_list'            => __( 'Items list', 'html5blank' ),
// 		'items_list_navigation' => __( 'Items list navigation', 'html5blank' ),
// 		'filter_items_list'     => __( 'Filter items list', 'html5blank' ),
// 	);
// 	$args = array(
// 		'label'                 => __( 'Product', 'html5blank' ),
// 		'description'           => __( 'Product information pages.', 'html5blank' ),
// 		'labels'                => $labels,
// 		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
// 		'taxonomies'            => array( 'category', 'post_tag' ),
// 		'hierarchical'          => true,
// 		'public'                => true,
// 		'show_ui'               => true,
// 		'show_in_menu'          => true,
// 		'menu_position'         => 5,
// 		'show_in_admin_bar'     => true,
// 		'show_in_nav_menus'     => true,
// 		'can_export'            => true,
// 		'has_archive'           => true,
// 		'exclude_from_search'   => false,
// 		'publicly_queryable'    => true,
// 		'capability_type'       => 'page',
// 	);
// 	register_post_type( 'product', $args );

// }
// add_action( 'init', 'products_post_type', 0 );

