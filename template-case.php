<?php /* Template Name: Case Story */ get_header(); ?>
<div class="container">
	<div class="row">
<div class="content">

	<nav class="nav--breadcrumbs" role="navigation">
		<?php the_breadcrumb(); ?>
	</nav>

	<?php if ( has_post_thumbnail() ) { ?>
	<div class="featured-image block">
		<?php the_post_thumbnail( 'featured' ); ?>
	</div>
	<?php } ?>

	<?php get_sidebar(); ?><main role="main" class="main block">
		<section>

			<header>
				<h1><?php the_title(); ?></h1>
			</header>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<?php comments_template( '', true ); // Remove if you don't want comments ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main><?php get_sidebar(); ?>
</div><!-- /content -->
		
		</div>
	</div>
<?php get_footer(); ?>
