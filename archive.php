<?php

get_header();
?>


<main id="main" class="site-main">
	<?php
		main_wrap_start();
		
			content_layout_start();
			
				$tmp_dir = get_template_directory();
				while ( have_posts() ) :
					the_post();
				
				endwhile; // End of the loop. ?>

				<?php
					the_posts_pagination( array(
						'mid_size' => 2,
						'prev_text' => __( '<', 'textdomain' ),
						'next_text' => __( '>', 'textdomain' ),
					));
				?>

			<?php
			content_layout_end();

			sidebar_layout_start();

			

			sidebar_layout_end();
	
		main_wrap_end();

	?>

</main>

<?php
get_footer();
