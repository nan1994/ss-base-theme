<?php

//Template name: Home

get_header();
?>

<main id="main" class="site-main">
	<?php
	while ( have_posts() ) :
		the_post();

		$template_parts = [
            'top-hero-section',
			'3-boxes-section',
			'section-left-right',
			'section-news'
		];

		foreach($template_parts as $part){
			include get_template_directory() . '/template-parts/' . $part . '.php';
		}

	endwhile;
    ?>
</main>

<?php
get_footer();