<?php
if (is_page_template( 'template-item.php' )) {
	global $product;
	$category = $product['webcategory'];    
} else {
    $category = get_field('webcategory');
}

$sprog = (ICL_LANGUAGE_CODE == 'en' ? 'gb' : ICL_LANGUAGE_CODE); 

if(is_numeric($category)):

		$wpdb->show_errors();

		$sql = "SELECT cat2.*,ifnull(cat2.new_path,cat2.current_path) as path from categories cat1 JOIN categories cat2 ON cat1.related LIKE concat('%',cat2.categoryid,'%')  AND cat2.categoryid > 10 WHERE cat2.sprog ='".$sprog."' and cat1.sprog ='".$sprog."' AND cat1.categoryid = ".$category;
		$rows = $wpdb->get_results( $sql );

		if (count($rows) > 0) : ?>
			<h5 class="widget-title"><?php _e('Related Products','html5blank'); ?></h5>
			<div class="box--related--grid">
			<?php foreach( $rows as $row): // variable must be called $post (IMPORTANT) ?>
				<div><a href="<?=$row->path?>" target="_top" target="_top" title="<?=$row->menu?>" onclick="_gaq.push(['_trackEvent','Link','Related','<?=$row->menu?>']);">
						<picture>
							<source type="image/webp" srcset="/thumbs/30x30xZC/<?=$row->imagename.'-('.$row->categoryid.')'?>.webp">
					  		<source type="image/jpg" srcset="/thumbs/30x30xZC/<?=$row->imagename.'-('.$row->categoryid.')'?>.jpg">
							<img src="/thumbs/30x30xZC/<?=$row->imagename.'-('.$row->categoryid.')'?>.jpg" alt="<?=$row->imagealt?>" width="30" height="30" />
						<picture>
					</a>
				</div>
				<div>
					<a href="<?=$row->path?>" target="_top" target="_top" title="<?=$row->menu?>" onclick="_gaq.push(['_trackEvent','Link','Related','<?=$row->menu?>']);">
						<?=$row->menu?>
					</a>
				</div>
			<?php endforeach; ?>
			</div>
		<?php endif; 
endif;  ?>
				






