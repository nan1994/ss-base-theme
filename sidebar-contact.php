<aside class="sidebar" role="complementary">

 	<?php //the_field('map', 'option'); ?>
		
	<p><strong><?php the_field('company_name', 'option'); ?></strong><br />
      
      <?php the_field('address', 'option'); ?><br />
      <?php the_field('zip_code', 'option'); ?>
	  <?php the_field('city', 'option'); ?><br />
      <a href="http://maps.google.dk/maps?f=q&amp;source=embed&amp;hl=da&amp;geocode=&amp;q=Carmo+A%2FS&amp;aq=0&amp;sll=56.010666,11.228027&amp;sspn=7.879904,19.665527&amp;ie=UTF8&amp;hq=Carmo+A%2FS&amp;hnear=&amp;ll=55.996845,12.534714&amp;spn=0.03072,0.05476&amp;z=13&amp;iwloc=A" style="color:#59f;text-align:left" target="_blank"><small><?php _e('Show map','html5blank'); ?></small></a>
	</p> 
	<p>
		<strong><?php _e('Tel.','html5blank').' : ' ?></strong> <a href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a><br />
		<strong><?php _e('Fax.','html5blank').' : ' ?></strong> <?php the_field('fax_number', 'option'); ?>
	</p>
	<p><strong><?php _e('Office Hours','html5blank') ?></strong><br />
	<?php the_field('office_hours', 'option'); ?>
    <?php get_template_part('loop-contacts-contactform'); ?>	
</aside><!-- /sidebar -->