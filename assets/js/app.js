(function ($) {

    var blazy;
    var current_scroll_position = document.documentElement.scrollTop;

    $(document).ready(function () {
        //Init the default web functionalities
        mobMenu();
        initAll();
        bLazyInit();
        toggleMobileSearch();
        //ajaxLiveSearch();
        $(window).on('load unload resize', adjustBodyPadding);
        $(window).on('load unload beforeload', bLazyInit);

        $('#video_button').on('click', function (e) {
            e.preventDefault();
            $('.video-banner').addClass('active');
            $('body').addClass('no-scroll');
        })

        $('.video-x').on('click', function (e) {
            e.preventDefault();
            $('.video-banner').removeClass('active');
            $('body').removeClass('no-scroll');
        })

        $(document).click(function (e) {
            if ($(e.target).closest('#s').length === 0 && $(e.target).closest('.js-search').length === 0) {
                closeSearchBar();
            }
        });

    });

    function closeSearchBar() {
        $('.header-search').removeClass('active');
    }

    function bLazyInit() {
        blazy = new Blazy({
            offset: 150,
            loadInvisible: true
        });
    }


    function initAll() {
        var swiper = new Swiper('.swiper-container', {
            pagination: {
                el: '.swiper-pagination',
            },
        });
        $(document).on('init load scroll', function () {
            if ($(this).scrollTop() > 20) {
                $('body').addClass('nav-bottom');
            } else {
                $('body').removeClass('nav-bottom');
            }
        });
    }

    function mobMenu() {
        $('#mobMenuBtn').on('click', function (e) {
            e.preventDefault();
            if ($(document).width() < 1200) {
                $('.mobile-nav .main-navigation ul li.menu-item-has-children').each(function () {
                    if ($(this).find('.btn-mob-sub').length === 0) {
                        $(this).append('<a class="btn-mob-sub" href="#"></a>');
                    }
                });
            }
            if ($('body').hasClass('mobile-active')) {
                $('body').removeClass('mobile-active').addClass('mobile-not-active');
                $('body').removeClass('overflow-active');
                $('#mob-menu .opened').removeClass('opened');
                $('html, body').scrollTop(current_scroll_position);
            } else {
                current_scroll_position = document.documentElement.scrollTop;
                $('body').removeClass('mobile-not-active').addClass('mobile-active');
                setTimeout(function () {
                    $('body').addClass('overflow-active');
                }, 150);
            }
        });


        $(document).on('touch click', '.btn-mob-sub', function (e) {
            e.preventDefault();
            var current_list = $(this).parent();
            if (!$(this).parent().hasClass('opened')) {
                $(this).parent().addClass('opened');
            } else {
                $(this).parent().removeClass('opened');
            }
            $(this).parent().parent().find('.opened').each(function () {
                if ($(this)[0] !== $(current_list)[0]) {
                    $(this).removeClass('opened');
                }
            });
        });

        $(document).keyup(function (e) {
            if (e.key === "Escape") { // escape key maps to keycode `27`
                $('body').removeClass('mobile-active').addClass('mobile-not-active');
            }
        });
    }

    function adjustBodyPadding() {
        $('body').css('padding-top', ($('.site-header').height()) + 'px');
    }

    function toggleMobileSearch() {
        if ($(window).outerWidth() < 768) {
            $('.js-search img').on('click', function (e) {
                e.preventDefault();
                if (!$(this).parent().hasClass('active')) {
                    $(this).parent().addClass('active');
                } else {
                    $(this).parent().removeClass('active');
                }
            });
        }
    }

    function ajaxLiveSearch() {

        $('.js-search').on('click', function (e) {
            e.preventDefault();
            $('body').addClass('search-active');
            $('.hdr-srch-results').html('');
            setTimeout(function () {
                $('#s').focus();
            }, 150);
        });

        //close the search bar in the header
        $('.js-search-close').on('click', function (e) {
            e.preventDefault();
            closeSearchBar();
        });

        $('.search-btn').on('click', function (e) {
            e.preventDefault();
            $(this).closest('form').submit();
        });

        //Event when user is typing to do a live search
        $('.hdr-srch #s').on('keyup', function (e) {
            if ($(this).val().length > 2) {
                $('.hdr-srch-results').remove('.hdr-srch-results .loader');
                $('.hdr-srch-results').html('<div class="loader"></div>');
                //if the key is not arrow down then do the search
                if (e.which !== 40) {
                    $.ajax({
                        url: ajax_url,
                        type: 'post',
                        dataType: 'html',
                        data: {action: 'wpb_ajax_fetch_search', keyword: $(this).val()},
                        success: function (data) {
                            if (data !== '') {
                                $('.hdr-srch-results').addClass('has-results');
                            } else {
                                $('.hdr-srch-results').removeClass('has-results');
                            }
                            $('.hdr-srch-results').remove('.hdr-srch-results .loader');
                            $('.hdr-srch-results').html(data);
                        }
                    });
                }
            } else {
                $('.hdr-srch-results').removeClass('has-results');
            }

        });

        $(document).keydown(function (e) {
            if ($('.hdr-srch-results li').length > 0) {
                if ([37, 38, 39, 40].indexOf(e.keyCode) > -1) {
                    e.preventDefault();
                }
                if (e.keyCode == 40) {
                    var current_focused = $('.hdr-srch-results li.focused');
                    if ($('.hdr-srch-results li.focused').next().length === 0) {
                        $('.hdr-srch-results li').first().addClass('focused');
                        $('.hdr-srch-results li').first().find('a').focus();
                    } else {
                        $('.hdr-srch-results li.focused').next().addClass('focused');
                        $('.hdr-srch-results li.focused').next().find('a').first().focus();
                    }

                    $(current_focused).removeClass('focused').blur();
                } else if (e.keyCode == 38) {
                    var current_focused = $('.hdr-srch-results li.focused');
                    if ($('.hdr-srch-results li.focused').prev().length === 0) {
                        $('.hdr-srch #s').focus();
                    } else {
                        $('.hdr-srch-results li.focused').prev().addClass('focused');
                        $('.hdr-srch-results li.focused').prev().find('a').focus();
                    }
                    $(current_focused).removeClass('focused').blur();
                }
            }
        });
    }

    $('.hdr-srch').on('click', function(e){
        if ($(e.target).attr('class')==='hdr-srch') {
            $('.js-search.active').removeClass('active');
        }
    });


    function closeSearchBar() {
        $('body').removeClass('search-active');
        $('.hdr-srch-results').removeClass('has-results');
        $('.hdr-srch-results').html('');
        $('#s').val('');
        $('.hdr-srch-results li').removeClass('focused').blur();
    }


})(jQuery);
