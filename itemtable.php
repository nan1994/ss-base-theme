<?php
function itemtableweb($itemnumber)
{

	//require_once($_SERVER["DOCUMENT_ROOT"].'/_catalog/include.php');
	//include_once('electrodewebfunction.php');


		
	global $wpdb;
	global $sprog;



	$picturepath = '/images/products/';
	$drawingpath = '/images/drawings/png/';
	$drawingwidth=100;
	$picturewidth=100;

	$wpdb->show_errors();

	// Write sql to get the selected product --- and check if 
	// 	a.it has a valid state (not in develoment and not discontinued) and 
	// 	b. is not blocked to specific customers	
	// 	c. has actual variants open in NAV
	$sql = $wpdb->prepare("SELECT p.item, categories.new_path as webfolder, t.webname, t.descriptionfull as description, t.descriptionshort as description_short, p.fitswith, p.drawing,p.tabletype,p.includes, p.mountswith,
				GROUP_CONCAT( DISTINCT (v.material) SEPARATOR ', ') AS materials,
				GROUP_CONCAT( DISTINCT (v.electrode) SEPARATOR ', ') AS electrode,
				IF(MAX(v.color1) IS NULL,NULL,IF(MAX(v.color2) IS NULL,1,2)) as nocolors,
				COUNT(DISTINCT v.itemnumber) as nVariants
				FROM products p 
				LEFT join variants v ON p.includes LIKE concat('%',v.item,'%') COLLATE utf8_unicode_ci
				LEFT JOIN texts t on t.item = p.item
				LEFT JOIN categories ON p.webcategory = categories.categoryid AND categories.sprog = t.sprog
				WHERE p.item = %s and t.sprog= %s
				and IFNULL(p.blocked,0) = 0 
				and p.status in (1,2)
				group by p.item, categories.new_path, t.webname, t.descriptionfull, t.descriptionshort, p.fitswith, p.drawing,p.tabletype,p.includes, p.mountswith
				having nVariants >= 0",
				$itemnumber,
				($sprog == 'en' ? 'gb' : $sprog) );
/*
	$sql = $wpdb->prepare("SELECT p.item, categories.new_path as webfolder, t.webname, t.descriptionfull as description, t.descriptionshort as description_short, p.fitswith, p.drawing,p.tabletype,p.includes, p.mountswith,
				GROUP_CONCAT( DISTINCT (v.material) SEPARATOR ', ') AS materials,
				GROUP_CONCAT( DISTINCT (v.electrode) SEPARATOR ', ') AS electrode,
				IF(MAX(v.color1) IS NULL,NULL,IF(MAX(v.color2) IS NULL,1,2)) as nocolors,
				COUNT(DISTINCT v.itemnumber) as nVariants
				FROM products p 
				LEFT join variants v ON p.includes LIKE concat('%',v.item,'%') COLLATE utf8_unicode_ci
				LEFT JOIN texts t on t.item = p.item
				LEFT JOIN categories ON p.webcategory = categories.categoryid AND categories.sprog = t.sprog
				WHERE p.item = %s and t.sprog= %s
				and IFNULL(p.blocked,0) = 0 
				and p.status in (1,2)
				group by p.item, categories.new_path, t.webname, t.descriptionfull, t.descriptionshort, p.fitswith, p.drawing,p.tabletype,p.includes, p.mountswith
				having nVariants >= 0",
				$itemnumber,
				$sprog);
*/

	
	
	$product = $wpdb->get_row($sql,ARRAY_A);

	//if (is_null($results)) die();
		
	$electr = $product['electrode'];
	
	$includes = explode(',',$product['includes']);	
	$includes = array_map( 'trim', $includes );
	$includes = "'".implode("','",$includes)."'";

//debugging
if ($itemnumber == 'CP9-electrodes') {
		//echo $sql;
		//var_dump($product);
	}

	/********************************************************************************************************* 
	*
	*								Treat Electrodes seperately
	*
	***********************************************************************************************************/
	//echo ($product['item']);

	if ($product['item'] == 'CP9-electrodes' || ($product['tabletype'] == 2 && $product['item'] <> '06-926'))  // machines & electrodes But EXCL Carmo SEAL
	{
		//get_template_part( 'electrodetable' ); 
		include( locate_template( 'electrodetable.php', false, false ) ); 
	}

	else // products
	{

		if ($sprog == 'de' ) {		
			$sql2 = "SELECT variants.*, nameDE as name,farve1.D AS farve1,farve2.D AS farve2 FROM variants 
			LEFT join farvekoder farve1 ON farve1.code = variants.color1
			LEFT JOIN farvekoder farve2 ON farve2.code = variants.color2
			WHERE item in (".$includes.") ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";		
			
			$sql3 = "SELECT DISTINCT v.col1borderhex,v.col1fillhex,v.col2borderhex,v.col2fillhex,farve1.D AS farve1,farve2.D AS farve2 
			FROM variants v
			LEFT join farvekoder farve1 ON farve1.code = v.color1
			LEFT JOIN farvekoder farve2 ON farve2.code = v.color2
			WHERE item IN (".$includes.")";
			}	// ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";}

		elseif ($sprog == 'fr' ) {	
				$sql2 = "SELECT variants.*, nameFR as name,farve1.FR AS farve1,farve2.FR AS farve2 FROM variants 
				LEFT join farvekoder farve1 ON farve1.code = variants.color1
				LEFT JOIN farvekoder farve2 ON farve2.code = variants.color2
				WHERE item in (".$includes.") ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";	
				
				$sql3 = "SELECT DISTINCT v.col1borderhex,v.col1fillhex,v.col2borderhex,v.col2fillhex,farve1.FR AS farve1,farve2.FR AS farve2 
				FROM variants v
				LEFT join farvekoder farve1 ON farve1.code = v.color1
				LEFT JOIN farvekoder farve2 ON farve2.code = v.color2
				WHERE item IN (".$includes.")";
				}	// ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";}

		elseif ($sprog == 'dk' ) {	
				$sql2 = "SELECT variants.*, nameDK as name,farve1.DK AS farve1,farve2.DK AS farve2 FROM variants 
				LEFT join farvekoder farve1 ON farve1.code = variants.color1
				LEFT JOIN farvekoder farve2 ON farve2.code = variants.color2
				WHERE item in (".$includes.") ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";	
				
				$sql3 = "SELECT DISTINCT v.col1borderhex,v.col1fillhex,v.col2borderhex,v.col2fillhex,farve1.DK AS farve1,farve2.DK AS farve2 
				FROM variants v
				LEFT join farvekoder farve1 ON farve1.code = v.color1
				LEFT JOIN farvekoder farve2 ON farve2.code = v.color2
				WHERE item IN (".$includes.")";
				}	// ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";}
				
		else	{		
				$sql2 = "SELECT variants.*, webname as name,farve1.UK AS farve1,farve2.UK AS farve2 FROM variants 
				LEFT join farvekoder farve1 ON farve1.code = variants.color1
				LEFT JOIN farvekoder farve2 ON farve2.code = variants.color2
				LEFT JOIN texts t on t.item = variants.item and t.sprog='gb'
				WHERE variants.item in (".$includes.") ORDER BY variants.item, material ASC, stock DESC, col1name ASC, itemnumber asc";	
				
				$sql3 = "SELECT DISTINCT v.col1borderhex,v.col1fillhex,v.col2borderhex,v.col2fillhex,farve1.UK AS farve1,farve2.UK AS farve2 
				FROM variants v
				LEFT join farvekoder farve1 ON farve1.code = v.color1
				LEFT JOIN farvekoder farve2 ON farve2.code = v.color2
				WHERE item IN (".$includes.")";
				}	// ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";}		



		$variants = $wpdb->get_results($sql2,ARRAY_A);

		$colors = $wpdb->get_results($sql3,ARRAY_A);

		// get the available materials 
		$materials = $wpdb->get_var("SELECT group_concat(distinct material) FROM variants v WHERE item IN (".$includes.")");

		// get the products that fits this product
		$fitswithresult = fitswith($product['fitswith']);


		/********************************************************************************************************* 
		*
		*								Full table for all other products
		*
		***********************************************************************************************************/
		
		if (count($variants) > 0 && $itemnumber<>'xx-xxx') 
		{ ?>
			<div class="item__table">

				<div><a name="<?=$product['item']?>"></a><h2><?=$product['item']?></h2></div>
				<div><h2><?=$product['webname']?></h2></div>
				<!--image-->
				<div ><?php 
					if (file_exists($_SERVER['DOCUMENT_ROOT'].$picturepath.$itemnumber.".jpg")) : 
						$imagefilename = $product['webfolder'].filenamesafe($product['webname']).'_'.$product['item'].'.jpg';
						$size1 = file_exists($_SERVER['DOCUMENT_ROOT'].'/thumbs/100x100'.$imagefilename) ? getimagesize($_SERVER['DOCUMENT_ROOT'].'/thumbs/100x100'.$imagefilename) : array(100,80);
						$size2 = file_exists($_SERVER['DOCUMENT_ROOT'].'/thumbs/200x200'.$imagefilename) ? getimagesize($_SERVER['DOCUMENT_ROOT'].'/thumbs/200x200'.$imagefilename) : array(200,150);
						if (preg_match("/^\d{2,2}-\d{3,3}$/", $itemnumber)): ?>
							<a class = "thumbnail1" href="<?=$product['webfolder'].$product['item']?>/">
								<picture>
									<source type="image/webp" srcset="<?=site_url();?>/thumbs/100x100<?=str_replace(".jpg", ".webp", $imagefilename)?>">
					  				<source type="image/jpeg" srcset="<?=site_url();?>/thumbs/100x100<?=$imagefilename?>">
					        		<img src="<?=site_url();?>/thumbs/100x100<?=$imagefilename?>" width="<?=$size1[0]?>" height="<?=$size1[1]?>" alt="<?=strip_tags(htmlspecialchars($product['item'].' '.$product['webname']))?>"/>
								</picture>
								<div class="noprint">
									<picture>
										<source type="image/webp" srcset="<?=site_url();?>/thumbs/200x200<?=str_replace(".jpg", ".webp", $imagefilename)?>">
					  					<source type="image/jpeg" srcset="<?=site_url();?>/thumbs/200x200<?=$imagefilename?>">
										<img src="<?=site_url();?>/thumbs/200x200<?=$imagefilename?>" alt="<?=strip_tags(htmlspecialchars($product['item'].' '.$product['webname']))?>" width="<?=$size2[0]?>" height="<?=$size2[1]?>"/> 
									</picture>
				  					<div class="caption"><?=$product['item'].' '.$product['webname']?></div>
								</div>
							</a><?php
						endif; 
					endif; ?>
		        </div>
		        <!-- data -->
		        <div>
		        	<!-- Description -->
		        	<div class='description' ><?=$product['description_short']?></div>
		        	<div class="center">
						<!-- materials -->
			        	<div class="itemdetail__header"><?php _e('Materials','html5blank')?></div>
			        	<div><?=$materials?></div>
			        	<!-- colors -->
			        	<div class="itemdetail__header"><?php _e('Colors','html5blank')?></div>
			        	<div class="itemdetail__color"><ul><?php
			        		foreach($colors as $row) :  ?>
			        			<li><div class="colorsprite x<?=substr($row['col1borderhex'],-6)?>x<?=substr($row ['col1fillhex'],-6)?>" title="<?=$row['farve1']?>"></div><?php			 
								if ($row['farve2']<>null) :?> 
									<div class="colorsprite x<?=substr($row ['col2borderhex'],-6)?>x<?=substr($row ['col2fillhex'],-6)?>" title="<?=$row['farve2']?>"></div><?php
								endif; ?>
									</li><?php
							endforeach ?></ul>


			        		<?php /*
			        		$i = 0;
			        		foreach($colors as $row) {
			        			if ($i++ >0 && $product['nocolors'] == 2 ) echo "|"; ?>
			        				<span class = 'thumbnail1'><div class="colorsprite x<?=substr($row['col1borderhex'],-6)?>x<?=substr($row ['col1fillhex'],-6)?>"></div><span class='noprint'><?=$row['farve1']?></span></span><?php			 
								if ($row['farve2']<>null) {?> 
									<span class = 'thumbnail1'><div class="colorsprite x<?=substr($row ['col2borderhex'],-6)?>x<?=substr($row ['col2fillhex'],-6)?>"></div><span class='noprint'><?=$row['farve2']?></span></span><?php
								} 
							} */?>
			        	</div>
			        	<!-- fits-with --><?php 
			        	if (!empty($fitswithresult)) : ?>
				        	<div class="itemdetail__header"><?php _e('Fits with','html5blank')?></div>
				        	<div class="itemdetail__fitswith"><ul><?php
				        		foreach($fitswithresult as $row) :  ?>
				        			<li><a class="fitswithlink thumbnail3" href="<?=$row->webfolder.$row->item?>/"><?PHP 
				        				echo($row->item);

										$webname = isset($row->webname) ? $row->webname : '';

										if (file_exists($_SERVER['DOCUMENT_ROOT'].$picturepath.$row->item.".jpg")) : 
											$imagefilename = $row->webfolder.filenamesafe($webname).'_'.$row->item.'.jpg';
											$size1 = file_exists($_SERVER['DOCUMENT_ROOT'].'/thumbs/100x100'.$imagefilename) ? getimagesize($_SERVER['DOCUMENT_ROOT'].'/thumbs/100x100'.$imagefilename) : array(100,80);
											if (preg_match("/^\d{2,2}-\d{3,3}$/", $row->item)): ?>
													<div class="noprint">
														<picture>
															<source type="image/webp" srcset="<?=site_url();?>/thumbs/100x100<?=str_replace(".jpg", ".webp", $imagefilename)?>">
					  										<source type="image/jpeg" srcset="<?=site_url();?>/thumbs/100x100<?=$imagefilename?>">
															<img src="<?=site_url();?>/thumbs/100x100<?=$imagefilename?>" alt="<?=strip_tags(htmlspecialchars($row->item.' '.$webname))?>" width="<?=$size1[0]?>" height="<?=$size1[1]?>"/> 
									  					</piicture>
									  					<div class="caption"><?=$row->item.' '.$webname?></div>
													</div><?php
											endif; 
										endif; ?>
				        				</a></li><?php
								endforeach?></ul>
							</div><?php 
						endif ?>
		        	</div>
		        	<!-- interested -->
		        	<div><?php _e('Interested in other colors or materials? Please call for quote...','html5blank')?></div>
		        	<!-- datasheets -->
		        	<div><?php if( $GLOBALS['NonLimit'] || true) { ?>
		        		<span><span class='carmosprite pdf2'></span><a class="download" href='/productsheet/<?=$sprog?>/<?=$itemnumber?>/' target="_blank" rel="noopener noreferrer" 
						onclick="dataLayer.push({
										    'event': 'GAevent',
										    'eventCategory': 'Download',
										    'eventAction': 'Productsheet',
										    'eventLabel': '<?=$product['item']?>'
										});"><?php _e('Download Productsheet','html5blank'); ?></a></span> <?php } ?>
					</div>
		        </div><!-- mid section -->
		        <!-- drawing -->
				<div ><?php 
					if (strlen($product["drawing"]) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'].$drawingpath.$product["drawing"])) : 
						$imagefilename = $product['webfolder'].sanitize_file_name($product['webname']).'_'.$product['item'].'.jpg';
						$size1 = file_exists($_SERVER['DOCUMENT_ROOT'].'/thumbs/100x100'.$drawingpath.$product["drawing"]) ? getimagesize($_SERVER['DOCUMENT_ROOT'].'/thumbs/100x100'.$drawingpath.$product["drawing"]) : array(100,80);
						$size2 = file_exists($_SERVER['DOCUMENT_ROOT'].'/thumbs/200x200'.$drawingpath.$product["drawing"]) ? getimagesize($_SERVER['DOCUMENT_ROOT'].'/thumbs/200x200'.$drawingpath.$product["drawing"]) : array(200,150);
						if (preg_match("/^\d{2,2}-\d{3,3}$/", $itemnumber)): ?>
							<a class = "thumbnail2" href="<?=$product['webfolder'].$product['item']?>/">
								<picture>
									<source type="image/webp" srcset="<?=site_url();?>/thumbs/100x100<?=$drawingpath.str_replace(".png", ".webp", $product["drawing"])?>">
					  				<source type="image/png" srcset="<?=site_url();?>/thumbs/100x100<?=$drawingpath.$product["drawing"]?>">
					        		<img src="<?=site_url();?>/thumbs/100x100<?=$drawingpath.$product["drawing"]?>" width="<?=$size1[0]?>" height="<?=$size1[1]?>" alt="<?=strip_tags(htmlspecialchars('Small drawing of '.$product["item"].' '.$product['webname']))?>"/>
					        	</picture>
								<div class="noprint">
									<picture>
										<source type="image/webp" srcset="<?=site_url();?>/thumbs/200x200<?=$drawingpath.str_replace(".png", ".webp", $product["drawing"])?>">
					  					<source type="image/png" srcset="<?=site_url();?>/thumbs/200x200<?=$drawingpath.$product["drawing"]?>">
										<img src="<?=site_url();?>/thumbs/200x200<?=$drawingpath.$product["drawing"]?>" alt="<?=strip_tags(htmlspecialchars('Big drawing of '.$product["item"].' '.$product['webname']))?>" width="<?=$size2[0]?>" height="<?=$size2[1]?>"/> 
									</picture>
				  					<div><?=__('Nominal measurements in mm','html5blank')?></div>
								</div>
							</a>
							<span class="caption disclaimer"><?=__('Nominal measurements in mm','html5blank')?></span><?php
						endif; 
					endif; ?>
		        </div>
		        <!-- button -->
		        <div><?php if (preg_match("/^\d{2,2}-\d{3,3}$/", $itemnumber)) { ?>
		        	<a href='//<?=$_SERVER["HTTP_HOST"].$product['webfolder'].$product['item'] ?>/' onclick="_gaq.push(['_trackEvent','Link','Detailbutton','<?=$product['item']?>']);">
		        	<!--a href='<?=get_permalink().$product['item']?>/' onclick="_gaq.push(['_trackEvent','Link','Detailbutton','<?=$product['item']?>']);"-->
		        		<button class='button' ><?php _e('Full description','html5blank'); ?></button>
		        	</a> 
		        <?php }?>
				</div><!-- button -->
		    </div><!-- dessin --><?php 
		}


	 /*
			// List fits with if relevant
			if (strlen($product['fitswith'])>1)        
			  {	echo "<tr>";
				  echo "<td width = '65' valign = 'top' align='left' ><strong>".$texts[2]."</strong></td> ";
				  ?><td><?php
					$fitswithresult = fitswith($product['fitswith']);
					if (mysqli_num_rows($fitswithresult) > 0)
					{
						if ($cellwidth == 0) $cellwidth = 30;
						$picheight = intval($cellwidth * 2 / 3);
						$kolonner = mysqli_num_rows($fitswithresult);
						
						mysqli_data_seek ( $fitswithresult , 0 ); // reset result
						
								for ($i = 0; $i<$kolonner; $i++) 
								{ 
									$q = mysqli_fetch_array($fitswithresult); 
									
									$picture = $picturepath.$q[0].".jpg";
									if (file_exists($_SERVER['DOCUMENT_ROOT'].$picture)) 
										{
											
											$size = getimagesize($_SERVER['DOCUMENT_ROOT'].$picture);
											$ratio = ($size[1]/$size[0]);		
											?><a class='thumbnail' style ="font-size:11px; border-bottom: 3px double; color: black;" href="https://<?=$_SERVER["HTTP_HOST"].$q[2]."#p".$q[0] ?>" >
	                                      
													<?=$q[0]?>
													<span class='noprint' style='font-style: italic;width:150px;font-size:11px;'>
														<img class='noprint' src="http://<?=$_SERVER["HTTP_HOST"]?>/thumbs/150x150<?=$picture?>" width="150" height="<?=intval(150*$ratio)?>" alt="<?=$q[1]?>" border="0" /><br />
														<?=$q[1]?>
													</span></a><?php
										}
										else 
										{ ?> <img src='emptysquare.php?width=<?=$cellwidth?>' width=<?=$cellwidth?>' alt=''/> <?php }
									}  
					 
				  }
				  mysqli_free_result($fitswithresult);
				  
			  } */
	}
}?>
