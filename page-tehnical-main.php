<?php

//Template name: Product Categories Page

get_header();
?>

<div class="page-tehnical-main">
    <?php 
        $product_categories = get_field('product_categories');
    ?>
    <div class="top-category-boxes">
        <div class="container-fluid">
            <div class="row">
                <?php foreach($product_categories as $cat): ?>
                    <div class="col-md-6 col-lg-4 col-xl-3">
                        <div class="category-box">
                            <a href="<?= $cat['category_url']; ?>">
                                <div class="btn-link">
                                    <?= $cat['category_title']; ?>
                                </div>
                                <div class="img-wrap">
                                    <div class="img" style="background-image: url('<?= $cat['category_image']['url']; ?>');"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="custom-design-box">
        <div class='container'>
            <div class="row">
                <div class="col-12">
                    <nav class="nav--breadcrumbs" role="navigation">
                        <?php the_breadcrumb(); ?>
                    </nav>
                </div>
                <div class="col-md-8">
                    <div class="box">
                        <h1 class="title"><?= get_the_title(); ?></h1>
                        <div class="text"><?= get_the_content(); ?></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php include get_template_directory() . '/template-parts/sidebar.php'; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();