<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package aa-base
 */

$general_info = get_field( 'general_information', 'option' );
$newsletter   = get_field( 'newsletter_text', 'option' );

$language_extensions = ICL_LANGUAGE_CODE == 'en' ? '_' : '_' . ICL_LANGUAGE_CODE . '_';
$newsletter_text     = get_option( 'options' . $language_extensions . 'newsletter_text_text' ) ? get_option( 'options' . $language_extensions . 'newsletter_text_text' ) : 'News that helps build your business';
$newsletter_link     = get_option( 'options' . $language_extensions . 'newsletter_text_link' ) ? get_option( 'options' . $language_extensions . 'newsletter_text_link' ) : 'News that helps build your business';

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer">
	<?php
	$footer = get_field( 'footer', 'option' );
	?>
    <div class="middle-footer">
        <div class="container">
            <div class="row justify-content-center">
				<?= $newsletter_link ? '<a class="row justify-content-center" href="' . $newsletter_link . '">' : ''; ?>
                <img src="<?= get_template_directory_uri() . '/build/images/icons/news.svg' ?>">
                <div class="footer-newsletter"><?= $newsletter_text; ?></div>
				<?= $newsletter_link ? '</a>' : ''; ?>
            </div>
        </div>
    </div>
    <div class="main-footer">
        <div class='container'>
            <div class='row justify-content-center d-md-none d-lg-flex'>
                <div class="col-lg-4">
                    <div class="footer-info">
                        <div class="info-img">
                            <img alt="<?= get_bloginfo( 'name' ); ?>"
                                 src="<?= get_field( 'logo', 'option' )['url']; ?>"/>
                        </div>
                        <div class="info-text">
							<?= $footer['info_text']; ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="footer-box-1">
                        <h5 class="footer-title"><?= $footer['menu_1_title']; ?></h5>
                        <div class="content-box-1">
							<?= $footer['content_box_1']; ?>
                        </div>
                    </div>
                    <div class="footer-box-2">
                        <h5 class="footer-title"><?= $footer['menu_2_title']; ?></h5>
                        <div class="content-box-2">
							<?= $footer['content_box_2']; ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="footer-box-3">
                        <h5 class="footer-title"><?= $footer['menu_3_title']; ?></h5>
                        <div class="footer-menu-1">
							<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-1',
								'menu_id'        => 'footer-menu-1',
							) );
							?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="footer-box-4">
                        <h5 class="footer-title"><?= $footer['menu_4_title']; ?></h5>
                        <div class="footer-menu-2">
							<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-2',
								'menu_id'        => 'footer-menu-2',
							) );
							?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="footer-box-5">
                        <h5 class="footer-title"><?= $footer['menu_5_title']; ?></h5>
                        <div class="footer-menu-3">
							<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-3',
								'menu_id'        => 'footer-menu-3',
							) );
							?>
                        </div>
                    </div>
                </div>
            </div>
            <div class='row justify-content-center d-none d-md-flex d-lg-none'>
                <div class="col-4">
                    <div class="footer-info">
                        <div class="info-img">
                            <img alt="<?= get_bloginfo( 'name' ); ?>"
                                 src="<?= get_field( 'logo', 'option' )['url']; ?>"/>
                        </div>
                        <div class="info-text">
							<?= $footer['info_text']; ?>
                        </div>
                        <div class="footer-box-1">
                            <h5 class="footer-title"><?= $footer['menu_1_title']; ?></h5>
                            <div class="content-box-1">
								<?= $footer['content_box_1']; ?>
                            </div>
                        </div>
                        <div class="footer-box-2">
                            <h5 class="footer-title"><?= $footer['menu_2_title']; ?></h5>
                            <div class="content-box-2">
								<?= $footer['content_box_2']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="footer-box-3">
                        <h5 class="footer-title"><?= $footer['menu_3_title']; ?></h5>
                        <div class="footer-menu-1">
							<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-1',
								'menu_id'        => 'footer-menu-1',
							) );
							?>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="footer-box-4">
                        <h5 class="footer-title"><?= $footer['menu_4_title']; ?></h5>
                        <div class="footer-menu-2">
							<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-2',
								'menu_id'        => 'footer-menu-2',
							) );
							?>
                        </div>
                    </div>
                    <div class="footer-box-5">
                        <h5 class="footer-title"><?= $footer['menu_5_title']; ?></h5>
                        <div class="footer-menu-3">
							<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-3',
								'menu_id'        => 'footer-menu-3',
							) );
							?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</footer>

</div>

<?php wp_footer(); ?>


<script type="text/javascript">
    WebFontConfig = {
        google: {
            families: ['Roboto:300,400,500,700']
        }
    };
    (function () {
        var wf = document.createElement('script');
        wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>

<script async defer
        src="<?= get_template_directory_uri() . '/build/js/app.min.js?ver=' . filemtime( get_template_directory() . '/build/js/app.min.js' ); ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCr9TdczdwFlbItg0yJY8NHLZ4VlYtB9GU"></script>

<!-- Cludo -->
<?php $CludoengineId = array( 'en' => 9544, 'de' => 10196, 'fr' => 10197 ); ?>
<script type="text/javascript" src="https://customer.cludo.com/scripts/bundles/search-script.min.js"></script>
<script type="text/javascript">
    var CludoSearch;
    (function () {
        var cludoSettings = {
            customerId: 1993,
            engineId: <?= isset($CludoengineId[ICL_LANGUAGE_CODE]) ? $CludoengineId[ ICL_LANGUAGE_CODE ] : 9544; ?>,
            type: 'standardOverlay',
            customerTemplate: "imagesOverlay",
            useStandardSearchTemplate: false,
            isOverlaySearch: true,
            //searchUrl: "/assets/1993/<?= isset($CludoengineId[ICL_LANGUAGE_CODE]) ? $CludoengineId[ ICL_LANGUAGE_CODE ] : 9544; ?>/index.html",
            searchInputs: ["cludo-search-form", "cludo-search-content-form", "cludo-search-mobile-form"],
            initSearchBoxText: "<?php _e( 'Search', 'html5blank' );?>",
            loading: "<img src='//customer.cludo.com/img/loading.gif' alt='Loading' class='loading' role='progressbar' /><div class='loading-more-text'><?php _e( 'Loading results. Please wait ...', 'html5blank' );?></div>",
            language: '<?=ICL_LANGUAGE_CODE?>'
        };
        CludoSearch = new Cludo(cludoSettings);
        CludoSearch.init();
    })();
</script>

</body>
</html>
