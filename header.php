<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package aa-base
 */
//CSI hack to suppress warning from line 156 + 157
error_reporting(0);
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">


	<?php
	//do not remove these lines!!!
	aa_header_preload();
	wp_head();
	?>
    <link rel="dns-prefetch" href="https://customer.cludo.com">

    <style><?= file_get_contents( get_stylesheet_directory() . '/build/atf/css/atf.min.css' ); ?></style>


    <!-- <link rel="preload" href="https://customer.cludo.com/css/overlay/cludo-search-with-images.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'"></noscript>
	<noscript><link href="https://customer.cludo.com/css/overlay/cludo-search-with-images.min.css" type="text/css" rel="stylesheet"></noscript> -->
    <link href="https://customer.cludo.com/css/overlay/cludo-search-with-images.min.css" type="text/css"
          rel="stylesheet">

    <!-- <link rel="preload" href="https://customer.cludo.com/assets/1993/9544/cludo-search.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'"></noscript>
	<noscript><link href="https://customer.cludo.com/assets/1993/9544/cludo-search.min.css" type="text/css" rel="stylesheet" /></noscript> -->
    <link href="https://customer.cludo.com/assets/1993/9544/cludo-search.min.css" type="text/css" rel="stylesheet"/>
    <!--link href="//wp-content/themes/carmo/css/cludo-search.min.css" type="text/css" rel="stylesheet" /-->
    <!--[if lte IE 9]>
    <script src="https://api.cludo.com/scripts/xdomain.js" slave="https://api.cludo.com/proxy.html"></script>
    <![endif]-->

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P833KR5');</script>
	<!-- End Google Tag Manager --> 

</head>

<body <?php body_class(); ?>>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P833KR5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<?php if ( get_field( 'gtm_id' ) ): ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=<?= get_field( 'gtm_id' ); ?>"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php endif; ?>

<div id="page" class="site">

    <header id="masthead" class="site-header nav-down">
        <div class="container">
            <div class="row d-flex d-xl-flex">
                <div class="col-2">
                    <div class="col-logo">
                        <a title="<?= get_bloginfo( 'name' ); ?>" alt="<?= get_bloginfo( 'name' ); ?>"
                           href="<?= get_site_url(); ?>">
                            <img alt="<?= get_bloginfo( 'name' ); ?>"
                                 src="<?= get_field( 'logo', 'option' )['url']; ?>"/>
                        </a>
                    </div>
                </div>
                <div class="col-10 right">
                    <div class="d-flex justify-content-end site-header__wrap">
                        <div class="col-nav d-none d-xl-flex"">
                            <nav id="site-navigation" class="main-navigation">
								<?php
								wp_nav_menu( array(
									'theme_location' => 'header-menu',
									'menu_id'        => 'primary-menu',
								) );
								?>

                            </nav>
                        </div>
                        <div class="col-search">
                            <div class="search-box hdr-box">
                                <a class="header-search js-search" href="#">
                                    <img src="<?= get_template_directory_uri() . '/build/images/icons/carmo-search-icon.svg' ?>">
                                    <div class="hdr-srch">
                                        <div class="hdr-srch-inner">
					                        <?php get_template_part( 'searchform_cludo' ); ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="search-box hdr-box language-select">
								<?php do_action( 'icl_language_selector' ); ?>
                            </div>
                            <div class="col-mob-menu d-flex d-xl-none">
                                <a id="mobMenuBtn" class="menu-btn" href="#">
                                    <div></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row align-items-center d-none">
                <div class="col-4 col-logo">
                    <a title="<?= get_bloginfo( 'name' ); ?>" alt="<?= get_bloginfo( 'name' ); ?>"
                       href="<?= get_site_url(); ?>">
                        <img alt="<?= get_bloginfo( 'name' ); ?>" src="<?= get_field( 'logo', 'option' )['url']; ?>"/>
                    </a>
                </div>
                <div class="col-8">
                    <div class="col-search">
                        <div class="search-box hdr-box">
                            <a class="header-search js-search" href="#">
                                <img src="<?= get_template_directory_uri() . '/build/images/icons/carmo-search-icon.svg' ?>">
                                <div class="hdr-srch">
                                    <div class="hdr-srch-inner">
										<?php get_template_part( 'searchform_cludo' ); ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="search-box hdr-box language-select">
							<?php do_action( 'icl_language_selector' ); ?>
                        </div>
                        <div class="col-mob-menu">
                            <a id="mobMenuBtn" class="menu-btn" href="#">
                                <div></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
    </header>

    <div class="mob-menu-wrap">
        <div id="mob-menu" class="mobile-nav">
            <div class="container menu-wrap">
                <div class="menu-box menu-nav-box">
                    <nav id="site-mobile-navigation" class="main-navigation">
						<?php
						wp_nav_menu( array(
							'theme_location' => 'header-menu',
							'menu_id'        => 'primary-mobile-menu',
							'container_id'   => 'container-mobile-menu'
						) );
						?>
                        <ul class='menu'>
                            <li>
                                <a class="header-search" href="<?= get_field( 'english_page', 'option' )['link']; ?>">
									<?= get_field( 'english_page', 'option' )['label']; ?>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="site-content">
