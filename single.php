<?php get_header(); ?>
<div class="container">
    <div class="row justify-content-center">
		<div class="col-md-9">
			<div class="content content--single">
				<nav class="nav--breadcrumbs" role="navigation">
					<?php the_breadcrumb(); ?>
				</nav>
				<main role="main" class="main block">
					<!-- section -->
					<section class="hentry">

					<?php if (have_posts()): while (have_posts()) : the_post(); ?>

						<!-- article -->
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							

										<header>
										<!-- post thumbnail -->
										<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
										<div class="post__image">
											<?php the_post_thumbnail( 'full', array( 'title' => get_the_title() ) ); ?>
										</div>
										&nbsp;
										<?php endif; ?>
										<!-- /post thumbnail -->
										<!-- /entry-title -->

										<!-- post details -->
										</header>

										<section class="post__container">
											<div class="entry-content">
												<h1 class="entry-title"><?php the_title(); ?></h1>
												<?php the_content(); // Dynamic Content ?>
												<?php edit_post_link(); // Always handy to have Edit Post Links available ?>
											</div><!-- --><div class="entry-meta">
												<time class="published updated date box box--date icon icon--calendar" datetime="<?php the_time('c'); ?>"><?php the_time('F j, Y'); ?></time>
												<div>
													<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
													<script type="IN/FollowCompany" data-id="1673524" data-counter="right"></script>
												</div>
												<?php //get_sidebar(); ?>
											</div>
											<div>
												<span class="vcard author author_name">Author: <span class="fn"><?php ucwords(the_author()); ?></span></span>
											</div>
										</section>

										<footer role="contentInfo">
										<?php the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>

										<?php comments_template(); ?>
										</footer>
						</article>
						<!-- /article -->

					<?php endwhile; ?>

					<?php else: ?>

						<!-- article -->
						<article>

							<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

						</article>
						<!-- /article -->

					<?php endif; ?>

					</section>
					<!-- /section -->
				</main>

				<aside class="sidebar" role="complementary">
					<?php the_field('post_sidebar'); ?>
				</aside>
			</div><!-- /content -->
		</div>
	</div>
</div>

<?php get_footer(); ?>
