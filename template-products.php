<?php /* Template Name: Product Categories Page */ get_header(); ?>
<div class="content">

	<nav class="nav--breadcrumbs" role="navigation">
		<?php the_breadcrumb(); ?>
	</nav>

<?php if(get_field('product_categories')): ?>
  <header class="categories block">
    <?php while(has_sub_field('product_categories')): ?><div class="categories__item">
      <a href="<?php the_sub_field('category_url'); ?>">

    <?php

    $image = get_sub_field('category_image');

        // vars
    $url = $image['url'];

        // thumbnail
    $size = 'square';
    $thumb = $image['sizes'][ $size ];
    $width = $image['sizes'][ $size . '-width' ];
    $height = $image['sizes'][ $size . '-height' ];

	
    if( !empty($image) ): ?>

    <img src="/thumbs/230x230xZC<?php echo parse_url($image['url'])['path']; ?>" alt="<?php echo $image['alt']; ?>" width="230" height="230" />
		  
<!--img src="<?php echo $thumb; ?>" alt="<?php echo $image['alt']; ?>" width="230" height="230" /-->

  <?php endif; ?>

      <h3 class="categories__title"><?php the_sub_field('category_title'); ?></h3>
    </a>
      </div><?php endwhile; ?>
</header><!-- /categories -->
<?php endif; ?>

	<aside class="sidebar" role="complementary">

  <?php the_field('post_sidebar'); ?>

</aside><!-- /sidebar --><main role="main" class="main block">
		<section>

			<header>
				<h1><?php the_title(); ?></h1>
			</header>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<?php comments_template( '', true ); // Remove if you don't want comments ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main><?php get_sidebar('products'); ?>
</div><!-- /content -->
<?php get_footer(); ?>
