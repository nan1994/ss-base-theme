<?php get_header(); ?>
<div class="container">
    <div class="row">
<div class="content content--category">

    <nav class="nav--breadcrumbs" role="navigation">
        <?php the_breadcrumb(); ?>
    </nav>

    <main role="main" class="main block">

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9">
                    <!-- section -->
                    <header>
                        <h1><?php _e( 'News', 'html5blank' ); ?></h1>
                    </header>

                    <div class="post-list">
                        <?php get_template_part('loop'); ?>
                    </div>

                    <?php 
                    the_posts_pagination( array(
                        'mid_size' => 2,
                        'prev_text' => '<',
                        'next_text' => '>',
                    ));
                    ?>
                </div>
            </div>
        </div>

        <!-- /section -->
    </main><?php get_sidebar('news'); ?>
</div><!-- /content -->
        
        </div>
    </div>

<?php get_footer(); ?>
