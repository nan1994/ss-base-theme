<?php
	global $includes;
	global $product;

	$sprog=ICL_LANGUAGE_CODE;


	if ($sprog == 'de' ) {		
		$sql_variants = $wpdb->prepare( "SELECT variants.*, nameDE as name,farve1.D AS farve1,farve2.D AS farve2,stocklevel.AvailQty FROM variants 
		LEFT join farvekoder farve1 ON farve1.code = variants.color1
		LEFT JOIN farvekoder farve2 ON farve2.code = variants.color2
		LEFT JOIN stocklevel on stocklevel.itemnumber = variants.itemnumber
		WHERE item in ('%s') ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc", $includes);
	}		

	elseif ($sprog == 'fr' ) {		
		$sql_variants = $wpdb->prepare( "SELECT variants.*, nameFR as name,farve1.FR AS farve1,farve2.FR AS farve2,stocklevel.AvailQty FROM variants 
		LEFT join farvekoder farve1 ON farve1.code = variants.color1
		LEFT JOIN farvekoder farve2 ON farve2.code = variants.color2
		LEFT JOIN stocklevel on stocklevel.itemnumber = variants.itemnumber
		WHERE item in ('%s') ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc", $includes);
	}

	elseif ($sprog == 'en' ) {		
		$sql_variants = $wpdb->prepare( "SELECT variants.*, nameUK as name,farve1.UK AS farve1,farve2.UK AS farve2, stocklevel.AvailQty FROM variants 
		LEFT join farvekoder farve1 ON farve1.code = variants.color1
		LEFT JOIN farvekoder farve2 ON farve2.code = variants.color2
		LEFT JOIN stocklevel on stocklevel.itemnumber = variants.itemnumber
		WHERE item in ('%s') ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc", $includes);
	}		

	else {		
		$sql_variants = $wpdb->prepare( "SELECT variants.*, nameDK as name,farve1.DK AS farve1,farve2.DK AS farve2, stocklevel.AvailQty FROM variants 
		LEFT join farvekoder farve1 ON farve1.code = variants.color1
		LEFT JOIN farvekoder farve2 ON farve2.code = variants.color2
		LEFT JOIN stocklevel on stocklevel.itemnumber = variants.itemnumber
		WHERE item in ('%s') ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc", $includes);
	}

	$variants = $wpdb->get_results($sql_variants,ARRAY_A);

	?>
	<table class='variants_table' >
		<thead>
            <th><?php _e('Order#','html5blank');?></th>
            <th><?php _e('Material','html5blank');?></th>
            <th><?php _e('Color','html5blank');?></th>
            <th><?php _e('From stock','html5blank');?></th>
            <th><?php _e('Standard Pack','html5blank');?></th>
	        <th><?php _e('Proper-<br>ties','html5blank');?></th>
            <th><?php _e('Use','html5blank');?></th>
            <th><?php _e('Comments','html5blank');?></th> 
            <th><?php _e("In stock<br>('000 pcs)","html5blank");?></th>
        </thead>
        <tbody><?php
          foreach( $variants as $row ) 
          {	?>
             <tr>
             <td><?=$row['itemnumber']?></td>
             <td><?=$row['material']?></td>
             <td><div class="colorsprite x<?=substr($row ['col1borderhex'],-6)?>x<?=substr($row ['col1fillhex'],-6)?>" title="<?=$row ['farve1']?>"></div>			 
			 <?php if ($row ['col2name']<>null) {?> <div class="colorsprite x<?=substr($row ['col2borderhex'],-6)?>x<?=substr($row ['col2fillhex'],-6)?>" title="<?=$row ['farve2']?>"></div><?php } ?>		 
			 </td>
             <td><?php 
						if ($row ['stock']== 'Yes') {	echo "<div class='iconsprite yes' title='".__('Delivered from stock','html5blank')."'></div>";}
						else {							echo "<div class='iconsprite call' title='".__('Produced to order - Call for quote','html5blank')."'></div>";}?>
			 </td>
             <td><?php echo($row['stdpack'] > '' ? $row['stdpack']: $product['stdpack']);
				if ($row['stacked'] == 1 ) 				echo "<div class='iconsprite stacked' title='".__('Delivered stacked for loading directly into mounting machines','html5blank')."'></div>";?>
             </td>
			 <td><?php 	
					$test = intval(trim($row ['hardness']));
					if ($test > 0 && $test < 60) 		echo "<div class='iconsprite shore0' title='".__('ShoreA < 60','html5blank')."'></div>";
					if ($test >= 60 && $test < 70) 		echo "<div class='iconsprite shore1' title='".__('ShoreA 60-70','html5blank')."'></div>";
					if ($test >= 70 && $test < 80) 		echo "<div class='iconsprite shore2' title='".__('ShoreA 70-80','html5blank')."'></div>";
					if (($test >= 80) && ($test < 90)) 	echo "<div class='iconsprite shore3' title='".__('ShoreA 80-90','html5blank')."'></div>";
					if ($test >= 90 ) 					echo "<div class='iconsprite shore4' title='".__('ShoreA > 90','html5blank')."'></div>";
					//if (trim($row['bloedgoerer']) == 'DINP') echo "<img src='".$iconpath."no.png' alt='' hspace = '1'/>";
					if ($row['uvbestandig'] == 1 ) 		echo "<div class='iconsprite sun' title='".__('Increased UV durability','html5blank')."'></div>";
					if ($row['kuldebestandig'] == 1 ) 	echo "<div class='iconsprite cold' title='".__('Improved low temp. performance','html5blank')."'></div>";
					if ($row['oliebestandig'] == 1 ) 	echo "<div class='iconsprite oil' title='".__('Increased oil resistance','html5blank')."'></div>";
					if ($row['armeret'] == 1 ) 			echo "<div class='iconsprite armed' title='".__('Reinforced composite material','html5blank')."'></div>";
					if ($row['sterilizable'] == 1 ) 	echo "<div class='iconsprite medico' title='".__('Increased durability during sterilization','html5blank')."'></div>";						
			 	?>
			 </td>
			 <td><?php
				if ($row['ultrasonicweldable'] == 1 ) 	echo "<div class='iconsprite ultrasonic' title='".__('Ultrasonic weldable','html5blank')."'></div>";
				if ($row['hfweldable'] == 1 ) 			echo "<div class='iconsprite hf' title='".__('High Frequency (HF) weldable','html5blank')."'></div>";
				if ($row['gluable'] == 1 ) 				echo "<div class='iconsprite seal' title='".__('Gluable using Carmo Seal','html5blank')."'></div>";
				?>				
			 </td>


			 <?php // Build comment field
			 	$comment = '';
				//$comment = trim($row['bloedgoerer']);
				if (strlen($row ['comments1']) > 0) {
					if (strlen($comment)  > 0 )
						{$comment = $comment.", ".$row ['comments1'];}
					else {$comment = $row ['comments1'];}
					}
				/*
				if ($row['stock'] <> 'Yes') 
					{
						if (strlen($comment)>0 ) 
							{$comment = $comment." / Call for quote"; }
						else {$comment = "Call for quote";}
					}
					*/
			 ?>
             <td><?=$comment?></td>

			 <!-- Insert Available Qty --> 
			 <td><?php echo ($row['AvailQty'] > 0 ? number_format($row['AvailQty']/1000,3,',','.') : "-"); ?>				
			 </td> 

             </tr><?php           
		}?>
		</tbody>
		<tfoot>
			<tr>
		        <td><hr /></td>
		        <td><hr /></td>
		        <td><hr /></td>
		        <td><hr /></td>
		        <td><hr /></td>
		        <td><hr /></td>
				<td><hr /></td>
				<td><hr /></td>
				<td><hr /></td>
		    </tr>
		</tfoot>
    </table>
