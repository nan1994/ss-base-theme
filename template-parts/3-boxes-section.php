<div class="section-3-boxes">
    <?php $section_3_boxes = get_field('section_3_boxes'); ?>
    <div class="container">
        <div class="row">
            <?php foreach($section_3_boxes as $box): ?>
                <div class="col-lg-4">
                    <div class="single-box">
                        <div class="img-wrap">
                            <a href="<?= $box['button']['link']; ?>">
                            <img src="<?= $box['image'] ? $box['image']['url'] : ''; ?>" alt="<?= $box['title']; ?>" >
                            </a>
                            <?php  if($box['image']) : ?>
                                <?php list($width, $height) = getimagesize($box['image']['url']);?>
                                <!--img src="<?= $box['image']['url']  ?>" alt="<?= $box['title']; ?>" width="<?= $width ?>" height="<?= $height ?>"-->
                            <?php endif; ?>
                        </div>
                        <div class="content-wrap">
                            <div class="title"><a href="<?= $box['button']['link']; ?>"><?= $box['title']; ?></div></a>
                            <div class="text"><?= $box['text']; ?></div>
                            <div class="btn-link">
                                <a href="<?= $box['button']['link']; ?>"><?= $box['button']['label']; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>