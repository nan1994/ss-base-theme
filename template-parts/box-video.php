<?php
/* Loads videos based on categories table and videos table in MySQL
CSI 26.03.2018
*/
if (is_page_template( 'template-item.php' )) {
	global $product;
	$category = $product['webcategory'];    
} else {
    $category = get_field('webcategory');
}
	
if(is_numeric($category)){
		//$wpdb->show_errors();
		
		$sql = "select videos.ref from categories join videos on instr(categories.videos ,videos.id) > 0 where categories.categoryid=".$category." order by RAND() LIMIT 1";

		$ref = $wpdb->get_var( $sql );

		if ($ref > '')
		{   
		  ?><div class="box--video"><?php
			if ($popup === false) echo('<script src="https://fast.wistia.com/embed/medias/'.$ref.'.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative; "><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_'.$ref.' videoFoam=true" style="height:100%;width:100%">&nbsp;</div></div></div>');
			else echo('<script src="https://fast.wistia.com/embed/medias/'.$ref.'.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_'.$ref.' popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>');
		  ?></div><!-- /video --><?php
		}

	}
?>