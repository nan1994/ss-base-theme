<?php $section_40_60 = get_field('section_40_60'); ?>
<?php if($section_40_60): ?>
    <div class="section-40-60">
        <div class="container">
            <?php $count = 1; ?>
            <?php foreach($section_40_60 as $box): ?>
                <div class="row">
                    <div class="col-lg-4 <?= $count % 2 ? '' : 'order-lg-2' ?>">
                        <div class="content-box">
                            <div class="box">
                                <h2 class="title"><?= $box['small_box']['title']; ?></h2>
                                <div class="text"><?= $box['small_box']['text']; ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 <?= $count % 2 ? '' : 'order-lg-1' ?>">
                        <div class="content-box">
                            <div class="box">
                                <h2 class="title"><?= $box['big_box']['title']; ?></h2>
                                <div class="text"><?= $box['big_box']['text']; ?></div>
                            </div>
                            <div class="img-wrap">
                                <img src="<?= $box['big_box']['image']['url'] ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            <?php $count++; endforeach; ?>
        </div>
    </div>
<?php endif; ?>