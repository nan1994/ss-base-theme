<?php 
include_once ('./include/cloudinary/src/Cloudinary.php');
include_once ('./include/cloudinary/settings.php');

$language_extensions = ICL_LANGUAGE_CODE=='da' ? '_' : '_' . ICL_LANGUAGE_CODE . '_';

$sidebar_labels_contact_sales_label = get_option('options'. $language_extensions .'sidebar_labels_contact_sales_label') ? get_option('options'. $language_extensions .'sidebar_labels_contact_sales_label') : 'Contact Sales';
$sidebar_labels_send_a_request_link = get_option('options'. $language_extensions .'sidebar_labels_send_a_request_link') ? get_option('options'. $language_extensions .'sidebar_labels_send_a_request_link') : '/contact';
$sidebar_labels_send_a_request_label = get_option('options'. $language_extensions .'sidebar_labels_send_a_request_label') ? get_option('options'. $language_extensions .'sidebar_labels_send_a_request_label') : 'Send a request';

?>

<div class="contact-sidebar">
    <?php if(!is_page_template('template-team.php')): ?>
        <?php $sales_contacts = get_field('sales_contacts'); ?>
        <?php if($sales_contacts): ?>
            <div class="contact-wrap">
                <div class="title"><?= $sidebar_labels_contact_sales_label; ?></div>
                <?php foreach($sales_contacts as $contact): ?>
                    <?php $contact_id = $contact->ID ?>
                    <?php $contact_phone = get_field('contact_phone', $contact_id) ?>
                    <?php $contact_img   = get_field('contact_profile_picture', $contact_id) ?>
                    <?php $contact_title = get_field('contact_title', $contact_id) ?>

                    <div class="content-box">
                        <div class="img-wrap">
                            <img src="<?=cloudinary_url("People/".get_field('contact_picture_url', $contact->ID), 
                                array(
                                    "gravity"=>"face",
                                    "effect"=>"grayscale",
                                    "quality"=>"jpegmini",
                                    "sign_url"=>true, 
                                    "height"=>70, 
                                    "radius"=>70, 
                                    "width"=>70, 
                                    "crop"=>"thumb",
                                    "zoom"=>0.80,
                                    array("effect"=>"sharpen:100")
                                    ))?>" alt="<?= $contact->post_title; ?>" width="70" height="70" />
                        </div>
                        <div class="info-box">
                            <div class="name"><?= $contact->post_title; ?></div>
                            <div class="phone"><small><?= $contact_title; ?></small></div>
                            <div class="phone"><?= $contact_phone; ?></div>
                            <div class="btn-link">
                                <a href="<?= $sidebar_labels_send_a_request_link; ?>"><?= $sidebar_labels_send_a_request_label; ?></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php the_field('post_sidebar'); ?>
</div>