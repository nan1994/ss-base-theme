<?php 
    $slides = get_field('slides'); 

    $language_extensions = ICL_LANGUAGE_CODE=='da' ? '_' : '_' . ICL_LANGUAGE_CODE . '_';

//News
    $sidebar_labels_news_label = get_option('options'. $language_extensions .'sidebar_labels_news_label') ? get_option('options'. $language_extensions .'sidebar_labels_news_label') : 'News';
    $sidebar_labels_all_news_link = get_option('options'. $language_extensions .'sidebar_labels_all_news_link') ? get_option('options'. $language_extensions .'sidebar_labels_all_news_link') : '/news';
    $sidebar_labels_all_news_label = get_option('options'. $language_extensions .'sidebar_labels_all_news_label') ? get_option('options'. $language_extensions .'sidebar_labels_all_news_label') : 'All news';

?>
<div class="top-hero-section">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php foreach($slides as $slide): ?>
                <div class="swiper-slide" 
                <?php if(!$slide['slide_video']): ?>
                    style="background-image: url('<?= $slide['slide_image']['url'] ?>');"
                <?php endif; ?>>
                    <?php if($slide['slide_video']): ?>
                        <video playsinline autoplay muted loop title="<?= $slide['alt_title']; ?>" src="<?= $slide['slide_video']['url']; ?>"></video>
                    <?php endif; ?>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="top-hero-box">
                                    <h2 class="hero-box-title">
                                        <?= $slide['slide_heading']; ?>
                                    </h2>
                                    <div class="btn-wrap">
                                        <a href="<?= $slide['slide_button_link']; ?>"><?= $slide['slide_button_text']; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <div class="news-button">
        <!--a href="/news"><img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?= get_template_directory_uri().'/build/images/icons/news.svg' ?>" alt="News icon" >News</a-->
        <a href="<?= $sidebar_labels_all_news_link; ?>"><img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?= get_template_directory_uri().'/build/images/icons/news.svg' ?>" alt="News icon" ><?= $sidebar_labels_news_label; ?></a>
    </div>
</div>