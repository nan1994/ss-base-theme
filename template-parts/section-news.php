<?php 

$section_news = get_field('section_news', 'option'); 
$language_extensions = ICL_LANGUAGE_CODE=='da' ? '_' : '_' . ICL_LANGUAGE_CODE . '_';

$section_news_news_label = get_option('options'. $language_extensions .'section_news_news_label') ? get_option('options'. $language_extensions .'section_news_news_label') : 'News';
$section_news_all_news_link = get_option('options'. $language_extensions .'section_news_all_news_link') ? get_option('options'. $language_extensions .'section_news_all_news_link') : '/news';
$section_news_all_news_label = get_option('options'. $language_extensions .'section_news_all_news_label') ? get_option('options'. $language_extensions .'section_news_all_news_label') : 'All News';
$section_news_read_more_label = get_option('options'. $language_extensions .'section_news_read_more_label') ? get_option('options'. $language_extensions .'section_news_read_more_label') : 'Read full story';

?>

<div class="section-news">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="subtitle"><?= $section_news_news_label; ?></div>
                <div class="btn-link">
                    <a href="<?= $section_news_all_news_link; ?>"><?= $section_news_all_news_label; ?></a>
                </div>
            </div>
        </div>
        <div class="row">
        <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 3
            );
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) : 
                while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="col-lg-4">
                    <div class="news-box">
                        <div class="date"><?= get_the_date('F d, Y', get_the_ID()); ?></div>
                        <div class="title"><?= get_the_title(); ?></div>
                        <div class="text"><?= wp_trim_words(get_the_content(), 25, '...'); ?></div>
                        <div class="btn-link">
                            <a href="<?= get_the_permalink(); ?>"><?= $section_news_read_more_label; ?></a>
                        </div>
                    </div>
                </div>
                <?php endwhile;
            endif;
            wp_reset_postdata();  
        ?>
        </div>
    </div>
</div>