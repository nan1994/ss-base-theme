<?php 
include_once ('./include/cloudinary/src/Cloudinary.php');
include_once ('./include/cloudinary/settings.php');

$language_extensions = ICL_LANGUAGE_CODE=='da' ? '_' : '_' . ICL_LANGUAGE_CODE . '_';

$sidebar_labels_contact_sales_label = get_option('options'. $language_extensions .'sidebar_labels_contact_sales_label') ? get_option('options'. $language_extensions .'sidebar_labels_contact_sales_label') : 'Contact Sales';

$sidebar_labels_send_a_request_link = get_option('options'. $language_extensions .'sidebar_labels_send_a_request_link') ? get_option('options'. $language_extensions .'sidebar_labels_send_a_request_link') : '/contact';
$sidebar_labels_send_a_request_label = get_option('options'. $language_extensions .'sidebar_labels_send_a_request_label') ? get_option('options'. $language_extensions .'sidebar_labels_send_a_request_label') : 'Send a request';

// CSO code copied from 
//$sidebar_labels_send_a_request_link = get_permalink(apply_filters( 'wpml_object_id', 1690, 'page', false,ICL_LANGUAGE_CODE));
//$sidebar_labels_send_a_request_label = _e('Send a request','html5blank') ?? 'Send a request';


//News
$sidebar_labels_news_label = get_option('options'. $language_extensions .'sidebar_labels_news_label') ? get_option('options'. $language_extensions .'sidebar_labels_news_label') : 'News';
$sidebar_labels_all_news_link = get_option('options'. $language_extensions .'sidebar_labels_all_news_link') ? get_option('options'. $language_extensions .'sidebar_labels_all_news_link') : '/news';
$sidebar_labels_all_news_label = get_option('options'. $language_extensions .'sidebar_labels_all_news_label') ? get_option('options'. $language_extensions .'sidebar_labels_all_news_label') : 'All news';

//Cases
$sidebar_labels_cases_label = get_option('options'. $language_extensions .'sidebar_labels_cases_label') ? get_option('options'. $language_extensions .'sidebar_labels_cases_label') : 'Cases';
$sidebar_labels_all_cases_link = get_option('options'. $language_extensions .'sidebar_labels_all_cases_link') ? get_option('options'. $language_extensions .'sidebar_labels_all_cases_link') : '/cases';
$sidebar_labels_all_cases_label = get_option('options'. $language_extensions .'sidebar_labels_all_cases_label') ? get_option('options'. $language_extensions .'sidebar_labels_all_cases_label') : 'All Cases';


?>

<div class="contact-sidebar">
    <?php if(!is_page_template('template-team.php')): ?>
        <?php $sales_contacts = get_field('sales_contacts') ?>
        <?php if($sales_contacts): ?>
            <div class="contact-wrap">
                <div class="title"><?= $sidebar_labels_contact_sales_label; ?></div>
                <?php foreach($sales_contacts as $contact): ?>
                    <?php $contact_id = $contact->ID ?>
                    <?php $contact_phone = get_field('contact_phone', $contact_id) ?>
                    <?php $contact_img = get_field('contact_profile_picture', $contact_id) ?>

                    <div class="content-box">
                        <div class="img-wrap">
                            <img src="<?=cloudinary_url("People/".get_field('contact_picture_url', $contact->ID), 
                                array(
                                    "gravity"=>"face",
                                    "effect"=>"grayscale",
                                    "quality"=>"jpegmini",
                                    "sign_url"=>true, 
                                    "height"=>70, 
                                    "radius"=>70, 
                                    "width"=>70, 
                                    "crop"=>"thumb",
                                    "zoom"=>0.80,
                                    array("effect"=>"sharpen:100")
                                    ))?>" alt="<?= $contact->post_title; ?>" width="70" height="70" />
                        </div>
                        <div class="info-box">
                            <div class="name"><?= $contact->post_title; ?></div>
                            <div class="phone"><?= $contact_phone ?></div>
                            <div class="btn-link">
                                <a href="<?= $sidebar_labels_send_a_request_link; ?>"><?= $sidebar_labels_send_a_request_label; ?></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <div class="news-wrap">
        <div class="title"><?= $sidebar_labels_news_label; ?></div>
        <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 3
            );
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) : 
                while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <a href="<?= get_the_permalink(); ?>">
                        <div class="news-box">
                            <div class="date"><?= get_the_date('F d, Y', get_the_ID()); ?></div>
                            <div class="title"><?= get_the_title(); ?></div>
                        </div>
                    </a>
                <?php endwhile;
            endif;
            wp_reset_postdata();  
        ?>
        <div class="btn-link">
            <a href="<?= $sidebar_labels_all_news_link; ?>"><?= $sidebar_labels_all_news_label; ?></a>
        </div>
    </div>
    <div class="cases-wrap">
        <div class="title"><?= $sidebar_labels_cases_label; ?></div>
            <?php
                $cases_id = icl_object_id(1417, 'page', false,ICL_LANGUAGE_CODE);
                $args = array(
                    'post_type' => 'page',
                    'post_parent' => $cases_id,
                    'posts_per_page' => 3
                );
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) : 
                    while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <?php $case_image = get_field('case_image') ?>
                        <a href="<?= get_the_permalink(); ?>">
                            <div class="cases-box">
                                <div class="img-wrap">
                                    <!--img src="<?= $case_image ?>" alt=""-->
                                    <img src="<?=rtrim(site_url(),"/").'/thumbs/60x34xZC/'.ltrim(parse_url($case_image, PHP_URL_PATH),"/")?>" alt="<?= get_the_title(); ?>" >
                                </div>
                                <div class="title"><?= get_the_title(); ?></div>
                            </div>
                        </a>
                    <?php endwhile;
                endif;
                wp_reset_postdata();  
            ?>
        <div class="btn-link">
            <a href="<?= $sidebar_labels_all_cases_link; ?>"><?= $sidebar_labels_all_cases_label; ?></a>
        </div>
    </div>
</div>