<?php $section_left_right = get_field('section_image_text_50-50'); ?>
<?php if($section_left_right): ?>
<div class="section-left-right">
    <div class="container-fluid">
        <?php $count = 1; ?>
        <?php foreach($section_left_right as $box): ?>
            <div class="row">
                <div class="col-lg-6 <?= $count % 2 ? '' : 'order-lg-2' ?>">
                    <?php echo( (isset($box['link_to']) && $box['link_to']) ? '<a href="'.$box['link_to'].'">' : '');  ?>
                        <div class="img-wrap" style="background-image: url('<?= $box['image']['url'] ?>');"></div>
                    <?php echo( (isset($box['link_to']) && $box['link_to']) ? '</a>' : ''); ?>
                </div>
                <div class="col-lg-6 <?= $count % 2 ? '' : 'order-lg-1' ?>">
                    <div class="content-box">
                        <?php echo( (isset($box['link_to']) && $box['link_to']) ? '<a href="'.$box['link_to'].'">' : '');  ?>
                            <h2 class="title"><?= $box['title']; ?></h2>
                        <?php echo( (isset($box['link_to']) && $box['link_to']) ? '</a>' : ''); ?>
                        <div class="text"><?= $box['text']; ?></div>
                        <div class="btn-link">
                            <?php if(isset($box['link_to']) && $box['link_to']): ?>
                                <a href="<?= $box['link_to']; ?>"><?= $box['button']['label']; ?></a>
                            <?php //endif; ?>
                            <?php elseif(isset($box['button']) && $box['button']): ?>
                                <a href="<?= $box['button']['link']; ?>"><?= $box['button']['label']; ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php $count++; endforeach; ?>
    </div>
</div>
<?php endif; ?>