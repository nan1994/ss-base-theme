<?php
include_once ('./include/cloudinary/src/Cloudinary.php');
include_once ('./include/cloudinary/settings.php');

$posts = get_field('sales_contacts');

if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
    <div class="single-team-box" style="float:left;display:block; background: #FFFFFF; border-bottom: 1px solid #E1E1E1; position: relative; margin-right: 10px; margin-top:10px;padding-bottom:10px; min-height: 179px;">
        <div class="" style="width:100px;margin-right:10px;float:left">
			<img src="<?=cloudinary_url("People/".(empty(get_field('contact_picture_url', $p->ID)) ? 'default-person.png' : get_field('contact_picture_url', $p->ID)), 
				array(
					"default_image"=>"default-person.png",
					"gravity"=>"face", 
					"quality"=>"jpegmini",
					//"format"=>"auto", // obs: this fails
					"sign_url"=>true, 
					"height"=>150, 
					"radius"=>0, 
					"width"=>100, 
					"crop"=>"thumb",
					"effect"=>"sharpen:100",
					"zoom"=>0.72))?>" alt="<?php the_title(); ?>" width="100" height="150" border="1" />
		</div>
		<div>
            <div><strong><?php the_field('contact_title'); ?></strong></div>
			<div class="fn"><?php the_title(); ?></div>
			<?php if(get_field('contact_phone')): 	?><div>Tel: <span class="tel"><?php the_field('contact_phone'); ?></span></div><?php endif; ?>
			<?php if(get_field('contact_fax')):		?><div>Fax: <span class="fax"><?php the_field('contact_fax'); ?></span></div><?php endif; ?>
			<?php if(get_field('contact_initials')):?><script type="text/javascript">ewrite("<?php the_field('contact_initials');?>","carmo","dk");"></script><?php endif; ?>
			<?php if(get_field('contact_linkedin')):?><div><a style="text-decoration: none;" href="<?php the_field('contact_linkedin'); ?>" target="_blank" rel="noopener nofollow"><span style="font: 80% Arial,sans-serif; color: #0783b6;"><img style="vertical-align: middle;" src="https://www.linkedin.com/img/webpromo/btn_in_20x15.png" alt="View <?php the_field('contact_initials');?> LinkedIn profile" width="20" height="15" border="0" />View my profile</span></a></div><?php endif; ?>
			<?php if(get_field('contact_initials')):?><div><a style="text-decoration: none;" href="/include/vcard/vcard.php?id=<?php the_field('contact_initials');?>" target="_blank" rel="noopener"><span style="font: 80% Arial, sans-serif; color: #0783b6;"><img src="/images/vcard_icon.jpg" alt="" width="16" height="12" border="0" /> Download my vCard</span></a></div><?php endif; ?>

        </div><!-- /contact__details -->
    </div><!-- /contact -->
    <?php endforeach; ?>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>
<?php 
/* original contact photo based on local image stored on wenserver. Changed to cloudinary 15.04.2020 CSI
if(get_field('contact_picture_url')): ?><img src="/thumbs/150x150/images/people/<?php the_field('contact_picture_url'); ?>" alt="<?php the_title(); ?>" width="100" height="150" border="1" />
*/?>







