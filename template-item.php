<?php 
/* 
Template Name: Item Page
Template Post Type: page
*/
global $wp_query;
global $dessinnumber;
global $wpdb;

$picturepath	= "/images/products/";
$pictureusagepath = "/images/products/usage/";
$iconpath		= "/images/icons/";
$drawingpath	= "/images/drawings/png/";
$datasheets		= ".".$_SERVER['DOCUMENT_ROOT']."/datasheet/";
$drawingwidth	= 120;
$picturewidth	= 120;
$cellwidth		= 100;

	$itemnumber = get_query_var('dessin');
	$itemnumber = substr($itemnumber,0,6);

	//$parentID = get_query_var( 'parentid' ) ?? 0;
	$sprog = (ICL_LANGUAGE_CODE == 'en' ?  'gb' : ICL_LANGUAGE_CODE);
	
	require_once('include_wp.php');
	
	$wpdb->show_errors(); 

	$sql =  $wpdb->prepare( 
				"SELECT p.item,cat_da.new_path as webfolderda,cat_gb.new_path as webfolderuk, cat_de.new_path as webfolderde, cat_fr.new_path as webfolderfr, categories.new_path as webfolder, t.webname, t.descriptionfull as description, t.descriptionshort as description_short, t.webmetatitle, t.webmetadescription, t.usage, t.techspec, fitswith, p.drawing,p.tabletype,p.includes, p.mountswith, p.webcategory,p.usp,p.related,p.stdpack,p.imageusage,
				GROUP_CONCAT( DISTINCT (v.material) SEPARATOR ', ') AS materials,
				GROUP_CONCAT( DISTINCT (v.electrode) SEPARATOR ', ') AS electrode,
				IF(MAX(v.color1) IS NULL,NULL,IF(MAX(v.color2) IS NULL,1,2)) as nocolors,
				COUNT(DISTINCT v.itemnumber) as nVariants,
				max(v.hfweldable) as hfweldable
				FROM products p 
				LEFT join variants v ON p.includes LIKE concat('%',v.item,'%') COLLATE utf8_unicode_ci
				LEFT JOIN texts t on t.item = p.item
				LEFT JOIN categories ON p.webcategory = categories.categoryid AND categories.sprog = t.sprog
				LEFT JOIN categories cat_da ON p.webcategory = cat_da.categoryid AND cat_da.sprog = 'da'
				LEFT JOIN categories cat_gb ON p.webcategory = cat_gb.categoryid AND cat_gb.sprog = 'gb'
				LEFT JOIN categories cat_de ON p.webcategory = cat_de.categoryid AND cat_de.sprog = 'de'
				LEFT JOIN categories cat_fr ON p.webcategory = cat_fr.categoryid AND cat_fr.sprog = 'fr'				
				WHERE p.item = %s and t.sprog= %s
				and IFNULL(p.blocked,0) = 0 
				and p.status in (1,2)
				group by p.item, cat_da.new_path, cat_gb.new_path, cat_de.new_path, cat_fr.new_path, categories.new_path, t.webname, t.descriptionfull, t.descriptionshort, t.webmetatitle, t.webmetadescription, fitswith, p.drawing,p.tabletype,p.includes, p.mountswith,p.webcategory,p.related,p.stdpack
				having nVariants >= 0", 
				$itemnumber, 
				$sprog 
	);

	$product = $wpdb->get_row($sql,ARRAY_A);


	// ******************************************************************************************
	// *******************   PRODUCT does not exist (or no active itemnumbers)"   ***************
	// ******************************************************************************************
	if ($product === null){
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 );
		//wp_redirect( "http://www.my-blog.com/a-new-destination", 301 );
	}

	// ******************************************************************************************
	// *******************   PRODUCT IS OK  ----  "Read rows from DB"; **************************
	// ******************************************************************************************

	$electr = $product['electrode'];
	$webcategory = $product['webcategory'];

	$includes = explode(',',$product['includes']);	
	$includes = array_map( 'trim', $includes );
	//$includes = "'".implode("','",$includes)."'";
	$includes = implode("','",$includes);

	// ******************************************************************************************
	// *                     If releted to is empty on the product, 							*
	// *                    identify other products in the same webcategory 					*
	// *						for the related products table  								*
	// ******************************************************************************************

	if ($product['related'] === NULL) 
	{
		
		$sql =  $wpdb->prepare( 
				"select GROUP_CONCAT(distinct item SEPARATOR ', ') as related from (select item from products where webcategory = %s and item <> %s limit 4) a",
				$webcategory,
				$itemnumber);
		
		//echo($webcategory);

		$relatedresult = $wpdb->get_row($sql,ARRAY_A);
		//$relatedstring = $relatedresult[0];
		$relatedstring = $relatedresult['related'];

	} else {$relatedstring=$product['related'];}

	// padding for injection into sql
	/*
	$relatedstring = explode(',',$relatedstring);	
	$relatedstring = array_map( 'trim', $relatedstring );
	$relatedstring = implode("','",$relatedstring);
*/

	// ******************************************************************************************
	// *					    Get Variant information depending on the 						*
	// *									    product type									*
	// *						(componentvs machines & electrodes)								*
	// ******************************************************************************************

if ($product['tabletype'] == 2 && $product['item'] <> '06-926')  // machines & electrodes But EXCL Carmo SEAL
{

	$sql2 = $wpdb->prepare( 
				"SELECT *, itemnameuk as name FROM electrodes WHERE left(itemnumber,6) in (%s) and type in ( '1' , '2' ) ORDER BY itemnumber asc",
				$includes);
}

else // products
{

	
	// See if this product is replaced orreplacement
	$sql_replaces = $wpdb->prepare("select * from item_replaces where item = %s",$itemnumber);	
	
	$replaces = $wpdb->get_row($sql_replaces,ARRAY_A);
	
}// <-- end preparation for products-->

?>

<?php get_header(); ?>
<div class="container">
	<div class="row">
<div class="content">
	<?php //if(is_page_template('Item Page')) echo ('true'); else echo ('false');?>

	<nav class="nav--breadcrumbs" role="navigation">
		<?php the_breadcrumb(); ?>
	</nav>

	<div itemscope itemtype="http://schema.org/Product">
	<meta itemprop='productID' content='sku:<?=$itemnumber?>'/>
	<meta itemprop='manufacturer' content='Carmo A/S'/>
	<meta itemprop='brand' content='Carmo'/>

	<header>
		<h1 class="producttemplate"><?=$product['item']?> - <span itemprop="name"><?=$product['webname']?></span></h1>
	</header>
	
	<aside class="sidebar">
		
		<!-- Webcategory picture -->
		<?php
		// Insert main picture
		if (file_exists($_SERVER['DOCUMENT_ROOT'].$picturepath.$itemnumber.".jpg")) 
		{

			
			$size = getimagesize($_SERVER['DOCUMENT_ROOT'].$picturepath.$itemnumber.".jpg");
			$ratio = ($size[1]/$size[0]);?>
					  
			<p>
				<picture>
					<source type="image/webp" srcset="<?=site_url();?>/thumbs/230x230<?=$product['webfolder'].filenamesafe($product['webname']).'_'.$product['item'].'.webp'?>">
					<source type="image/jpg" srcset="<?=site_url();?>/thumbs/230x230<?=$product['webfolder'].filenamesafe($product['webname']).'_'.$product['item'].'.jpg'?>">	
					<img class='' itemprop='image' src="<?=site_url();?>/thumbs/230x230<?=$product['webfolder'].filenamesafe($product['webname']).'_'.$product['item'].'.jpg'?>" width="230" height="<?=intval(230*$ratio)?>" alt="<?=strip_tags(htmlspecialchars($product['webname']))?>" title="<?=strip_tags(htmlspecialchars($product['item'].' '.$product['webname']))?>" border="0"/>
				</picture>
			<p class='drawingfootnote'><?=_e('Click for larger picture','html5blank')?></p> 
			</p><?php
		} 

		// Insert main drawing
		if (strlen($product["drawing"]) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'].$drawingpath.$product["drawing"]))
		{
			$size = getimagesize($_SERVER['DOCUMENT_ROOT'].$drawingpath.$product["drawing"]);
			$ratio = ($size[1]/$size[0]);
			?>
				<picture>

					<img class='' src='<?=site_url();?>/thumbs/230x230<?=$drawingpath.$product["drawing"] ?>' width='230' height='<?=intval(230*$ratio)?>' alt='Small technical drawing of <?=strip_tags(htmlspecialchars($product["item"]." ".$product['webname']))?>' style="padding-top:20px;" align="middle"/>
				</picture>
			
			<p class='drawingfootnote'><?=_e('Nominal measurements in mm','default')?></p><?php
		}
			
		// Insert usage image if exists
		if (strlen($product["imageusage"]) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'].$pictureusagepath.$product["imageusage"]))
		{
			$size = getimagesize($_SERVER['DOCUMENT_ROOT'].$pictureusagepath.$product["imageusage"]);
			$ratio = ($size[1]/$size[0]);
			?><img class='' src='<?=site_url();?>/thumbs/230x230<?=$pictureusagepath.$product["imageusage"] ?>' width='230' height='<?=intval(230*$ratio)?>' alt='' style="padding-top:20px;" align="middle"/>
			<?php	
			
		} 	?>
		<!-- /Webcategory picture -->
	
	</aside><!-- /sidebar --><main role="main" class="main block">
	
		<section>

			<!-- article -->
			<article >

				<?php /*
				<meta itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				    <meta itemprop="seller" itemscope itemtype="http://schema.org/Organization">
				    <meta itemprop="name" content="Carmo A/S"</span>
				    <link itemprop="itemCondition" href="http://schema.org/NewCondition"/>
				    <link itemprop="availability" href="http://schema.org/InStock"/>
				</div>
				*/?>

				<div style="margin-top:10px;" >
				<p description itemprop="description"><?=$product['description']?></p><?php		

				// List usage and electrode to use if relevant
				if (strlen($product['usage'])>0) echo '<p>'.$product['usage'].'</p>'; 
				if (strlen($product['techspec'])>0) echo '<p>'.$product['techspec'].'</p>';     

				// List replaces / replaced by
				if ($replaces && count($replaces) > 0) 
				{
					$chain = $replaces;
					
					if (strlen($chain['replaces']) > 0) echo ('<p>'.__('This product replaces','html5blank').' : '.$chain['replaces'].'.</p>');
					if (strlen($chain['replacedby']) > 0) echo ('<p>'.__('This product is replaced by','html5blank').' : '.$chain['replacedby'].'.</p>');
				}
				
				// If HF weldable, print reference to Injection Moulding
				if ($product['hfweldable'] > 0) : 
					$link = get_permalink(apply_filters( 'wpml_object_id', 2802, 'page', TRUE , ICL_LANGUAGE_CODE ));

					$link_im = ICL_LANGUAGE_CODE == 'da' ? 
								get_permalink(9210) :
								get_permalink(apply_filters( 'wpml_object_id', 2802, 'page', TRUE , ICL_LANGUAGE_CODE ));

					$link_quality = get_permalink(6793);

					$link_sustainability = get_permalink(apply_filters( 'wpml_object_id', 6338, 'page', TRUE , ICL_LANGUAGE_CODE ));
					  if      ($sprog == 'de' ) { ?><p>Diese <a href="<?=$link?>"  title="Spritzgussteile">Spritzgussteile</a> werden in Dänemark hergestellt</p><?php }
					  else if ($sprog == 'fr' ) { ?>Ces composants <a href="<?=$link?>"  title="Injection Moulding">moulés par injection</a> sont fabriqués au Danemark</p><?php }
					  else if ($sprog == 'da' ) { ?><p>Disse <a href="<?=$link_im?>"  title="Læs mere om sprøjtestøbning i plast og plaststøbning hos Carmo">sprøjtestøbte</a> komponenter er produceret i Danmark, hvilket sikrer høj ensartethed, kvalitet og <a href="<?=$link_quality?>" title="Læs mere om kvalitetssikring og kvalitetsstyring hos Carmo A/S">kvalitetssikring</a>, lav stykpris og fokus på <a href="<?=$link_sustainability?>" title="Læs mere om bæredygtighed hos Carmo A/S">bæredygtighed</a>.<?php }
					  else { 					  ?><p>These components are manufactured in Denmark by <a href="<?=$link?>" title="Read more on Injection Moulding">Injection Molding</a>, securing high quality, low unit prices, focus on <a href="<?=$link_sustainability?>" title="Read more on Carmo Sustainability">sustainability</a> and respect for the enveronmental impact of production.</p><?php }
				endif;  

				// If electrode, print reference to HF Welding
				if (strlen($product['electrode'])>1 && $product['hfweldable'] > 0) : ?>
					<p><a href="<?=get_permalink(apply_filters( 'wpml_object_id', 1650, 'page', TRUE , ICL_LANGUAGE_CODE ))?>"><?php    
					  if      ($sprog == 'de' ) { ?>Für Hochfrequenzschweißen</a> mit unserer HF-Elektrode <?=$electr?> geeignet</p><?php }
					  else if ($sprog == 'fr' ) { ?>Soudable par frèquence haute</a> avec electrode HF <?=$electr?></p><?php }
					  else if ($sprog == 'da' ) { ?>Kan højfrekvenssvejses</a> med elektrode <?=$electr?></p><?php }
					  else { 					  ?>Can be High Frequency welded</a> using our electrode <?=$electr?></p><?php }
				endif;   
				
				// Print USP list
				if( strlen($product['usp'])>1) get_template_part( 'usplist' );
				
				if ($GLOBALS['NonLimit'] || true) : ?>
					<div><?php
					// Download datasheet
					if (preg_match("/^\d{2,2}-\d{3,3}$/", $itemnumber) ) : ?>
						<p><a class="download" href="/productsheet/<?=$sprog?>/<?=$itemnumber?>/" target="_blank" rel="noopener noreferrer" title = "<?=_e('Download Datasheet','html5blank');?>"
						onclick="
							dataLayer.push({
							    'event': 'GAevent',
							    'eventCategory': 'Download',
							    'eventAction': 'Productsheet',
							    'eventLabel': '<?=$product['item']?>'
							});"><span class='iconsprite pdf2'"></span><?=_e('Download Datasheet','html5blank');?></a></p><?php
					// Download full catalog 1956?>
						<p><a href='<?=get_permalink(apply_filters( 'wpml_object_id', 1956, 'page', TRUE , ICL_LANGUAGE_CODE ))?>' target='_blank' rel="noopener noreferrer"title = "<?=_e('Download full catalog','html5blank');?>"><span class='iconsprite pdf2'"></span><?=_e('Download full catalog','html5blank');?></a></div><?php
					endif;
					?></p><?php
				endif;?>
		
				<?php
				// Print custom design / Rapid prototyping section
				if ($sprog == 'da' ) : 
					//$link_rapidprototyping = get_permalink(apply_filters( 'wpml_object_id', 6338, 'page', TRUE , ICL_LANGUAGE_CODE ));
					$link_rapidprototyping = get_permalink(9230); ?>
					  <p>Carmo's komponenter kan fremstilles i flere materialer og farver. Har du brug for specielle geometrier kan vores <a href="<?=$link_rapidprototyping?>"  title="Læs om Carmo's unikke Rapid Prototyping setup">Rapid Prototyping</a> setup hjælpe dig hurtigt og billigt i gang.</p><?php
				endif;?>   

		
				<!-- Insert main variant table -->				  				  
				<?php
					if ($product['tabletype'] == 1)  get_template_part( 'varianttable' );
					elseif ($product['tabletype'] == 2) get_template_part( 'electrodetable' ); 
				?>


				<!-- List fitswith info -->
				<table class="fitswith" > 
					<?php
					// *************************************************************************************
					// *******************      List fits with if relevant  ********************************
					// *************************************************************************************

					if (strlen($product['fitswith'])>1)        
					  {	echo "<tr>";
						  echo "<td class='legend' >".__('Fits with','html5blank')."</td> ";
						  
							$fitswithresult = fitswith($product['fitswith']);			
							
							if ($fitswithresult > 0) : ?>
								<td valign = 'top' align='left'>
							 		<?php linktable($fitswithresult,80); ?>
							 	</td><?php
							 endif; 
						  echo "</tr>";
					  }

					// *************************************************************************************
					// *******************      List Mounts with if relevant  ******************************
					// *************************************************************************************
					//var_dump($product['mountswith']);
					if (strlen($product['mountswith'])>0)        
						{	echo "<tr>";
							echo "<td class='legend' >".__('Mounts with','html5blank')."</td> ";
					   
							$mountswithresult = fitswith($product['mountswith']);
							if ($mountswithresult > 0)
							{
							  echo "<td valign = 'top' align='left'>";
							 linktable($mountswithresult,80);
							 echo "</td>";
						  }
						  echo "</tr>";
					}

					// *************************************************************************************
					// *******************      List Related with if relevant  *****************************
					// *************************************************************************************
					//echo ('related : ');print_r($relatedstring); die();
					if (strlen($relatedstring)>0)        
					  {	echo "<tr>";
						  echo "<td class='legend' >".__('Related products','html5blank')."</td> ";
							$relatedresult = fitswith($relatedstring);
							if ($relatedresult > 0)
							{
							  echo "<td valign = 'top' align='left'>";

							 linktable($relatedresult,80);
							 echo "</td>";
						  }
						  echo "</tr>";
					}

					// *************************************************************************************
					// *******************      List Also Bought.           ********************************
					// *************************************************************************************
					$also = alsobought($itemnumber , 4 , explode(',',$product['fitswith']) );

					if ($also > 0) : ?>
					<tr>
						<td class='legend' ><?php _e('Customers also bought','html5blank')?></td>
						<td valign = 'top' align='left'><?php linktable($also,80); ?></td>
					</tr>
					<?php endif; ?>


				</table>
				<p><?php _e('All measurements and drawings are nominal. For exact measurements - and for critical integration - please contact sales for detailed drawings. Carmo is not responsible for misprints and technical changes.','html5blank')?></p>
				<p><em><?php _e('Please note: Products in PVC are inherently susceptible to deformation due to elevated temperatures during storage and transportation. Carmo recommends storing and transporting components between 0 and 30 deg Celsius. In order to help minimise this risk, all Carmo products are shipped in extremely rigid cartons with a comparatively small number of parts per box.','html5blank')?></em></p>
				<hr />
				<p><em><?php _e('(*) Smaller quantities can be delivered on request for a repacking fee.','html5blank')?></em></p>
				<p><em><?php _e('(**) Stocklevels are indicative.','html5blank')?></em></p><?php
			 	if (is_medical()) : ?>
					<p><em><?php _e("(***) Carmo's Medical components are designed to be able to withstand common sterilisation. However, it is the responsibility of the customer to verify the product's resilience to any specific sterilisation process.","html5blank")?></em></p><?php
				endif;?>

			</article>
			<!-- /article -->


		</section>
		<!-- /section -->
	</main><?php get_sidebar('products'); ?>
	
	</div><!-- itemscope -->

</div><!-- /content -->
</div>
</div>
<?php get_footer(); ?>
