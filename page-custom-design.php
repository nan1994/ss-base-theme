<?php

//Template name: Custom Design

get_header();
?>

<div class="page-custom-design">
    <?php 
        $video_banner = get_field('video_banner');
        $hero_top_banner = get_field('hero_top_banner');
        $fallback_image = get_field('fallback_image', 'options');
    ?>
    <div class="hero-top-banner" style="background-image: url('<?= $hero_top_banner['image']['url'] ? $hero_top_banner['image']['url'] : $fallback_image['url'] ?>');">
        <div class="container">
            <div class="row align-items-end">
                <div class="col">
                    <div class="top-hero-box">
                        <h2 class="hero-box-title">
                            <?= $hero_top_banner['title']; ?>
                        </h2>
                        <?php if($video_banner): ?>
                            <div class="btn-wrap">
                                <button id="video_button"><?= $hero_top_banner['button']['label']; ?></button>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="video-banner">
        <div class="video-x"></div>
        <video autoplay="false" preload="none" id="myVideo" controls>
            <source src="<?= $video_banner['url'] ?>" type="video/mp4">
        </video>
    </div>
    <div class="custom-design-box">
        <div class='container'>
            <div class="row">
                <div class="col-12">
                    <nav class="nav--breadcrumbs" role="navigation">
                        <?php the_breadcrumb(); ?>
                    </nav>
                </div>
                <div class="col-md-8">
                    <h1 class="title"><?= get_the_title(); ?></h1>
                    <div class="text"><?= the_content(); ?></div>
                </div>
                <div class="col-md-4">
                    <?php include get_template_directory() . '/template-parts/sidebar.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <?php include get_template_directory() . '/template-parts/section-left-right.php'; ?>
</div>

<?php
get_footer();