<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package aa-base
 */

get_header();
?>

<main id="main" class="site-main">
	<section class="page-404">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-md-10">
					<div class="number-404">404</div>

					<p>The page you are looking for doesn't exist or another error occurred – you can go to the home page by clicking <a href="<?= get_site_url(); ?>">here</a></p>
					<?php 
						$contact_info = get_field('contact_info', 'options');
					?>
					<p>If further questions, please contact <?= get_bloginfo('name'); ?> by phone on <a href="tel:<?= $contact_info['phone'] ?>"><?= $contact_info['phone'] ?></a> or write an
					email to <a href="mailto:<?= $contact_info['email'] ?>"><?= $contact_info['email'] ?></a></p>
					<!--[if lte IE 9]>
					    <script src="https://api.cludo.com/scripts/xdomain.js" slave="https://api.cludo.com/proxy.html"></script>
					<![endif]-->
					<script id="cludo-404-script" data-cid="1993" data-eid="12230">
					(function () {
					    var s = document.createElement('script');
					    s.type = 'text/javascript';
					    s.async = true;
					    s.src = 'https://customer.cludo.com/scripts/404/cludo-404.js';
					    var x = document.getElementsByTagName('script')[0];
					    x.parentNode.insertBefore(s, x);
					})();
					</script>

				</div>
			</div>
		</div>
	</section>
</main>

<?php
get_footer();
