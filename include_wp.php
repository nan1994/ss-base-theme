<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);



function _bot_detected() {

  if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider|ECCP|Riddler/i', $_SERVER['HTTP_USER_AGENT'])) {
    return TRUE;
  }
  else {
    return FALSE;
  }

}

function findelectrodeusedwith($electrode)
{
	$string = "";
	$sql = "SELECT DISTINCT A.itemnumber AS electrode,left(B.itemnumber,6) AS used_with FROM variants A join variants B ON B.electrode = A.itemnumber WHERE A.itemnumber = '".$electrode."' ORDER BY A.itemnumber ,left(A.itemnumber,6)";

	$result = mysqli_query($sql);
 
	//	if ($result == 0) echo "Error " . mysqli_errno() . ": ". mysqli_error();
	
	if (mysqli_num_rows($result) > 0)
    {    
		while($row = mysqli_fetch_array($result))
		{
			if (strlen($string)>0){$string = $string.", ";}
			$string = $string.$row['used_with'];
		}
	}
	mysqli_free_result($result); 
	return $string;
}

function alsobought($item,$antal,$excludearray)
{
	global $wpdb;
	global $sprog;
	global $link;
	
	$string = "";
	$excludestring = "'".implode("','",$excludearray)."'";

		$sql = $wpdb->prepare("SELECT DISTINCT AlsoBought as item, t.webname AS catalogname, cat.new_path AS webfolder 
			FROM alsobought_design LEFT JOIN products p ON p.item = alsobought_design.AlsoBought 
			LEFT JOIN texts t on t.item = alsobought_design.AlsoBought and t.sprog=%s
			left join categories cat on cat.categoryid = p.webcategory and cat.sprog=%s
			left join (select item,COUNT(itemnumber) as n from variants group by item) v on v.item = p.item
			WHERE design =%s and AlsoBought not in (%s) and left(AlsoBought,2) <> '07'
			and ifnull(p.blocked,0) = 0
			and ifnull(p.status,1) = 1
			and ifnull(v.n,0) > 0
			ORDER BY Customers DESC, Orders DESC LIMIT %d",
			$sprog,
			$sprog,
			$item,
			$excludestring,
			$antal);

	$result = $wpdb->get_results($sql);
 
	return $result;
}

function fitswith($fitswith_string)
{
	global $wpdb;
	global $sprog;

	$string = "";
	$array = explode(',',$fitswith_string);	
	$array = array_map( 'trim', $array );
	$instring = implode("','",$array);

/*
		// get a set of "special" entries
	// $special_entries = array(1, 3, 5, 8, 13, [...]);
	$special_entries = get_option('my_special_entries');

	// how many entries will we select?
	$how_many = count($special_entries);

	// prepare the right amount of placeholders
	// if you're looing for strings, use '%s' instead
	$placeholders = array_fill(0, $how_many, '%d');

	// glue together all the placeholders...
	// $format = '%d, %d, %d, %d, %d, [...]'
	$format = implode(', ', $placeholders);

	// and put them in the query
	$query = "SELECT ID, post_title, post_name, post_parent FROM $wpdb->posts WHERE post_parent IN($format)";

	// now you can get the results
	$results = $wpdb->get_results( $wpdb->prepare($query, $special_entries) );
*/
/*
	$sql = $wpdb->prepare("	SELECT  DISTINCT p.item, t.webname as catalogname, categories.new_path as webfolder 	
				FROM _products p 
				LEFT JOIN _texts t on t.item = p.item
				LEFT JOIN categories ON p.webcategory = categories.categoryid AND categories.sprog = t.sprog
				left join (select item,COUNT(itemnumber) as n from variants group by item) v on p.includes LIKE concat('%',v.item,'%')
				WHERE p.item  in (%s) and t.sprog=%s 
					and (blocked is null or blocked = 0) 
					and (status is null or status = 1) 
					and ifnull(v.n,0) > 0
					ORDER BY item limit 4",
					$instring,
					$sprog);
*/
	$sql = "	SELECT  DISTINCT p.item, t.webname as catalogname, categories.new_path as webfolder 	
				FROM products p 
				LEFT JOIN texts t on t.item = p.item
				LEFT JOIN categories ON p.webcategory = categories.categoryid AND categories.sprog = t.sprog
				left join (select item,COUNT(itemnumber) as n from variants group by item) v on p.includes LIKE concat('%',v.item,'%')
				WHERE p.item  in ('".$instring."') and t.sprog='".$sprog."' 
					and (blocked is null or blocked = 0) 
					and (status is null or status = 1) 
					and ifnull(v.n,0) > 0
					ORDER BY item limit 4";

	//echo($sql);
	$result = $wpdb->get_results($sql);
 		
	return $result;
}

// returns the filename of the country specific version of the global image picture
// If the local version does not exists, it will be created from the global image
// And renamed using the localized text
function replace_accents($var){ //replace for accents catalan spanish and more
    $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ'); 
    $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
    $var= str_replace($a, $b,$var);
    return $var; 
}

function filenamesafe($filename)
{
	$specialchars = array('&','À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','×','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ','œ','Œ','½','¼','°','"');
	$replacechars = array('+','A','A','A','A','A','AA','AE','C','E','E','E','E','I','I','I','I','D','N','O','O','O','O','OE','x','OE','U','U','U','U','Y','ss','a','a','a','a','a','aa','ae','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','oe','u','u','u','u','y','y','oe','OE','1/2','1/4','deg','inch');

			$filename = replace_accents($filename);
			$filename = str_replace($specialchars,$replacechars,$filename);
			$filename = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $filename);
			$filename = str_replace(" ","-",$filename);
			$filename = str_replace(".","-",$filename);
			$filename = str_replace("/","-",$filename);
			$filename = str_replace("\\","-",$filename);
			$filename = str_replace(":","-",$filename);
			$filename = str_replace(";","-",$filename);
			$filename = str_replace("+","-",$filename);
			$filename = str_replace(",","-",$filename);
			$filename = str_replace('"','inch',$filename); 
			$filename = str_replace("'",'inch',$filename); 
			$filename = str_replace("(",'-',$filename);  // Not unsafe per se, but remove them sp we reserve for speciel interpretation in Thumbs
			$filename = str_replace(")",'-',$filename);  // Not unsafe per se, but remove them sp we reserve for speciel interpretation in Thumbs
			
			$filename = preg_replace('{([-,])\1+}','$1',$filename); // Remove any repeating '-', e.g.'--'
			$filename = strtolower($filename);
			
	return $filename;
}

function color_mkwebsafe ( $in )
{
    // put values into an easy-to-use array
    $vals['r'] = hexdec( substr($in, 0, 2) );
    $vals['g'] = hexdec( substr($in, 2, 2) );
    $vals['b'] = hexdec( substr($in, 4, 2) );

    // loop through
    foreach( $vals as $val )
    {
        // convert value
        $val = ( round($val/51) * 51 );
        // convert to HEX
        $out .= str_pad(dechex($val), 2, '0', STR_PAD_LEFT);
    }

    return $out;
}

function linktable ($rows,$cellwidth){
	global $picturepath;


	//print_r($rows);

	foreach($rows as $q){?>
		<div class="linkcell">

		<div class="linkheader"><?=$q->item?></div>
		<div class="linkimg"><?php

				//echo $_SERVER['DOCUMENT_ROOT'].$picturepath.$q->item.".jpg";
				if (file_exists($_SERVER['DOCUMENT_ROOT'].$picturepath.$q->item.".jpg")) :
				
					//$picture = $_SERVER['DOCUMENT_ROOT'].$picturepath.$q[0].".jpg";
					$picture = $picturepath.$q->item.".jpg";
					$size = getimagesize($_SERVER['DOCUMENT_ROOT'].$picture);
					$ratio = ($size[1]/$size[0]);
					
					// Option 1 : Link 2 the overall category page
						//$link = 	"https://".$_SERVER["HTTP_HOST"].$q[2]."#p".$q[0]
						
					// Option 2 : Link to the specific product page
					$link = site_url().$q->webfolder;
						
					// For ordinary products add the itemname.jpg as destination
					// For all machined, Carmo Seal and Clip grommet tool only add the web category folder (dedicated productpages, not automatically generated by the template
					//if (preg_match("/^\(?!07-512|06-926)(d{2,2}-\d{3,3})$/", $q[0]) ) {$link .= $q[0]."/";}
					if ( preg_match("/^\d{2,2}-\d{3,3}$/", $q->item) && !preg_match("/^(07-512|06-926)$/", $q->item) ) {$link .= $q->item."/";}
					?>                             
						<a class='thumbnail' href='<?=$link?>' onclick="_gaq.push(['_trackEvent','Link','Linktable','<?=$row['menu']?>']);">
						<picture>
							<source type="image/webp" srcset="<?=site_url();?>/thumbs/<?=$cellwidth?>x<?=$cellwidth?>xBW<?=str_replace(".jpg", ".webp", $picture) ?>">
							<source type="image/jpg" srcset="<?=site_url();?>/thumbs/<?=$cellwidth?>x<?=$cellwidth?>xBW<?=$picture ?>">		
							<img src="<?=site_url();?>/thumbs/<?=$cellwidth?>x<?=$cellwidth?>xBW<?=$picture ?>" width="<?=$cellwidth?>" height="<?=intval($cellwidth*$ratio)?>" alt="<?=$q->catalogname?>" border="0" />
						</picture>
						<div class='noprint'>
							<piicture>
								<source type="image/webp" srcset="<?=site_url();?>/thumbs/150x150<?=str_replace(".jpg", ".webp", $picture) ?>">
								<source type="image/jpg" srcset="<?=site_url();?>/thumbs/150x150<?=$picture ?>">	
						    	<img src="<?=site_url();?>/thumbs/150x150<?=$picture?>" width="150" height="<?=intval(150*$ratio)?>" alt="<?=$q->catalogname?>" border="0" />
						    </piicture>
						    <div class="caption"><?=$q->catalogname?></div>
						</div>
						</a><?php
				endif; ?>
		</div><!-- linkimage-->
	</div><!--linkcell--><?php
	}
}

/*
function linktable2 ($mysqlresult,$cellwidth)
{
	global $picturepath;
	if ($cellwidth == 0) $cellwidth = 30;
	$picheight = intval($cellwidth * 2 / 3);
	$kolonner = mysqli_num_rows($mysqlresult);

	//mysqli_data_seek ( $mysqlresult , 0 ); // reset result
	?>
	<table class='fitswith'>
	<tr>
	<?php
		//echo "<table class='fitswith'>";

		//echo "<tr>";
		for ($i = 0; $i<$kolonner; $i++) { $q = mysqli_fetch_array($mysqlresult); ?><th width = '<?=$cellwidth?>'><?=$q[0]?></th> <?php ;}
		echo "</tr>";
		
		//mysqli_data_seek ( $mysqlresult , 0 ); // reset result
		
		echo "<tr>";
			for ($i = 0; $i<$kolonner; $i++) 
			{
				$q = mysqli_fetch_array($mysqlresult);
				//echo $_SERVER['DOCUMENT_ROOT'].$picturepath.$q[0].".jpg";
				if (file_exists($_SERVER['DOCUMENT_ROOT'].$picturepath.$q[0].".jpg")) 
				{
					//$picture = $_SERVER['DOCUMENT_ROOT'].$picturepath.$q[0].".jpg";
					$picture = $picturepath.$q[0].".jpg";
					$size = getimagesize($_SERVER['DOCUMENT_ROOT'].$picture);
					$ratio = ($size[1]/$size[0]);
					
					// Option 1 : Link 2 the overall category page
						//$link = 	"https://".$_SERVER["HTTP_HOST"].$q[2]."#p".$q[0]
						
					// Option 2 : Link thttpso the specific product page
						$link = site_url().$q[2];
						
					// For ordinary products add the itemname.jpg as destination
					// For all machined, Carmo Seal and Clip grommet tool only add the web category folder (dedicated productpages, not automatically generated by the template
					//if (preg_match("/^\(?!07-512|06-926)(d{2,2}-\d{3,3})$/", $q[0]) ) {$link .= $q[0]."/";}
					if ( preg_match("/^\d{2,2}-\d{3,3}$/", $q[0]) && !preg_match("/^(07-512|06-926)$/", $q[0]) ) {$link .= $q[0]."/";}
					?>
						<td>                                
                            <a class='thumbnail' href='<?=$link?>' onclick="_gaq.push(['_trackEvent','Link','Linktable','<?=$row['menu']?>']);">
                            <img src="<?=site_url();?>/thumbs/<?=$cellwidth?>x<?=$cellwidth?>xBW<?=$picture ?>" width="<?=$cellwidth?>" height="<?=intval($cellwidth*$ratio)?>" alt="<?=$q[1]?>" border="0" />
                            <div class='noprint'>
                                <img class='noprint' src="<?=site_url();?>/thumbs/150x150<?=$picture?>" width="150" height="<?=intval(150*$ratio)?>" alt="<?=$q[1]?>" border="0" />
                                <div class="caption"><?=$q[1]?></div>
                            </div>
							</a>
                       </td>
			<?php
				}
				else 
                { ?> <img src='FFFFFF-1.png' width='<?=$cellwidth?>' alt=''/> <?php }
			}
		echo "</tr>";
	  
echo "</table>";
}
*/
function coloursquare($border,$fill)
{
	if ($border < 0) $border = 0;
	//if ($border > #FFFFFF) $border = #FFFFFF;
	if (strlen($border) > 0) {$border = substr($border,-6);} else $border ='C0C0C0';
	if (strlen($fill) > 0) {$fill = substr($fill,-6);} else $fill = 'C0C0C0';
	$fname = 'images/coloursquare-'.$border.'-'.$fill.'.png';

	if (file_exists($fname)==0)
	{
		$im = ImageCreate(11,11);
		$border = ImageColorAllocate($im,hexdec( substr($border, 0, 2) ),hexdec( substr($border, 2, 2) ),hexdec( substr($border, 4, 2) ));
		$fill = ImageColorAllocate($im,hexdec( substr($fill, 0, 2) ),hexdec( substr($fill, 2, 2) ),hexdec( substr($fill, 4, 2) ));
		$grey = ImageColorAllocate($im,153,153,153);
		$white = ImageColorAllocate($im,255,255,255);
		//ImageRectangle($im,0,0,10,10,$fill);
		ImageFilledRectangle($im,0,0,10,10,$fill);
		imagesetthickness($image, 2);
		ImageRectangle($im,0,0,10,10,$border);

		if ($fill == 'FFFFFF') // Transparent
		{
		 ImageLine($im,0,0,10,10,$border);
		 ImageLine($im,0,10,10,0,$border);
		}
		else ImageRectangle($im,1,1,9,9,$border);
		header('Content-Type: image/png');

		// Save the image to file.png 
		//ImagePNG($im,$fname,0,NULL);

		// Destroy image
		imagedestroy($img); 
		//$base64 = base64_encode($im,$filename,90);
	}
    return $fname;
} 


function categorypicture($category)
{
 
 	global $sprog;
 
 	switch ($sprog) {
    case 'de':
        $sql = "SELECT * from categories where sprog ='de' and categoryid = ".$category;
        break;
    case 'fr':
        $sql = "SELECT * from categories where sprog ='fr' and categoryid = ".$category;
        break;
    default:
        $sql = "SELECT * from categories where sprog ='gb' and categoryid = ".$category;
	}

 	$link =  opendb(); 
 

	$result = mysqli_query($link,$sql);

	if (mysqli_errno()) echo "Error " . mysqli_errno() . ": ". mysqli_error();		
		if (mysqli_num_rows($result) == 0) {  die("No category found!!"); }
	else { 
			$row = mysqli_fetch_array($result,MYSQLI_ASSOC);

	
	?>	
	<p><a href="<?=site_url();?>/thumbs/600x600xBR<?=$row['new_path'].filenamesafe($row['imagename']).'-('.$row['categoryid'].')'?>.jpg" rel="prettyPhoto" class="maintext_small_gray" title="<?=strip_tags(htmlspecialchars($row['menu']))?>">
		<img src="<?=site_url();?>/thumbs/200x200xZC<?=$row['new_path'].filenamesafe($row['imagename']).'-('.$row['categoryid'].')'?>.jpg" alt="<?=strip_tags(htmlspecialchars($row['imagealt']))?>" border="0" width="200" height="200"/>
		</a>
	</p>
	<meta property=”og:image” content="<?=site_url();?>/thumbs/600x600xZC<?=$row['new_path'].filenamesafe($row['imagename']).'-('.$row['categoryid'].')'?>.jpg" />           
	<?php }
	
	mysqli_free_result($result);
	closedb($link);

	unset($sql,$result);
}
 
function usplist($uspidlist)
{
 	global $sprog;
 
 	$string = "";
	$array = explode(',',$uspidlist);	
	$array = array_map( 'trim', $array );
	$inlist = "(".implode(",",$array).")";

 
 	switch ($sprog) {
	    case 'de':
	        $sql = "SELECT txtde as usp from usp where active=1 and uspid in ".$inlist;
	        break;
	    case 'fr':
	        $sql = "SELECT txtfr as usp from usp where active=1 and uspid in ".$inlist;
	        break;
	    default:
	        $sql = "SELECT txtuk as usp from usp where active=1 and uspid in ".$inlist;
	}
	//die($sql);
	//$relatedresult = $wpdb->get_results($sql,ARRAY_A);
	?><ul class='usplist'><?php
	//$wpdb->get_results($sql);
	
	if ($wpdb->query($sql) === FALSE) {
		wp_die( __('Crap! well that’s screwed up: ' . $wpdb->last_error) ); 
	} else
	foreach( $wpdb->get_results($sql) as $key => $row) {
		// each column in your row will be accessible like this
		echo "<li class='usplist' >".$row->usp."</li>";
	}
	?></ul><?php
}

?>