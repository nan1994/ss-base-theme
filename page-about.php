<?php

//Template name: About

get_header();
?>

<main id="main" class="site-main">
    <?php 
        $hero_banner = get_field('hero_banner');
        $box1 = get_field('box1');
        $box2 = get_field('box2');
    ?>
    <div class='container'>
        <div class="row">
            <div class="col-12">
                <h1><?= the_title(); ?></h1>
            </div>
            <div class="col-12">
                <div class="about-banner b-lazy" data-src="<?= $hero_banner['url'] ?>"></div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="about-box">
                    <h2><?= $box1['title']; ?></h2>
                    <div class="text">
                        <?= $box1['text']; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="about-box">
                    <h2><?= $box2['title']; ?></h2>
                    <div class="text">
                        <?= $box2['text']; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php
get_footer();