SS Base Theme Carmo version

Changelog
----------------------------------
1.1.2   2021-06-12  acf-json updates
1.1.1   2021-06-09  Services template + minor fixes
1.1.0	2021-06-06	Pagination & Mobile lang. shifter
1.0.0				Intitial version

-----------------------------------

-----------------------------------
1.1.2
	- acf-json fields updated to accomodate slider video files and alt-text

-----------------------------------
1.1.1
	- Draft template Services
	- New folder acf-json for storng and updateing / syncing ACF filed definitions (ACF v5+)
	- Minor update to template-item.php to reference '/services/sproejtestoebning/'' rather than '/faq/sproejtestoebning/'' (in Danish)

-----------------------------------
1.1.0
	- Pagination on News
	- News layout
	- Language selector mobile
	- Slider with video playback
-----------------------------------
