<?php
/**
 * Detailed download output
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/** @var DLM_Download $dlm_download */

?><a class="download-link filetype-icon <?php echo 'filetype-' . $dlm_download->get_version()->get_filetype(); ?>"
   title="<?php if ( $dlm_download->get_version()->has_version_number() ) {
	   printf( __( 'Version %s', 'download-monitor' ), $dlm_download->get_version()->get_version_number() );
   } ?> onclick="
				dataLayer.push({
				    'event': 'GAevent',
				    'eventCategory': 'Download',
				    'eventAction': 'Downloadmanager-simple',
				    'eventLabel': '<?php $dlm_download->the_title(); ?>'
				});" href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow">
	<?php echo $dlm_download->get_title(); ?>
</a>