<?php
/**
 * Detailed download output
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/** @var DLM_Download $dlm_download */

?>

<table width="650px" border="0" cellspacing="0" cellpadding="0" style="padding-top:25px;"> 
<tr> 
	<th width="100" scope="col">&nbsp;</th> 
	<th width="350" scope="col">&nbsp;</th> 
	<th width="150" scope="col"><div align="left">Date</div></th> 
	<th width="100" scope="col"><div align="left">Type</div></th> 	
</tr> 
<tr> 
	<td rowspan="2">
		<?php $dlm_download->the_image(); ?>
		<a class="" href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow" onclick="_gaq.push(['_trackEvent', 'Download', 'PDF', '<?php $dlm_download->the_title(); ?>']);"></a>
	</td> 
	<td colspan="3"><h4><?php $dlm_download->the_title(); ?></h4></td> 	
</tr>
<tr> 
		<td valign="top"><?php $dlm_download->the_excerpt(); ?></td> 
		<td valign="top"><?php //echo $dlm_download->get_version()->get_date(); ?></td> 
		<td valign="top">
			<p><?php echo $dlm_download->get_version()->get_filetype(); ?> (<?php echo $dlm_download->get_version()->get_filesize_formatted(); ?>)</p>
		</td> 
		
</tr> 
<tr>
	<td></td>
	<td colspan="3">
		<input type="checkbox" name="catalog1" id="catalog1"> Keep me updated
	</td>
</tr> 
</table>

