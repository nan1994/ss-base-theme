<?php
/**
 * Detailed download output
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/** @var DLM_Download $dlm_download */

?><!--div class="downloads">
        <div style="grid-area: a;"><?php $dlm_download->the_image(); ?></div>
        <div style="grid-area: b;"><h2><?php $dlm_download->the_title(); ?></h2></div>
        <div style="grid-area: c;"><?php $dlm_download->the_excerpt(); ?></div>
        <div style="grid-area: c;">TEST</div>
        <div style="grid-area: c;"><?php $dlm_download->get_version()->has_version_number() ?? printf( __( 'Ver. %s', 'download-monitor' ), $dlm_download->get_version()->get_version_number() ) ?></div>
		<div style="grid-area: c;" ><?=$dlm_download->get_version()->get_date()->format('Y-m-d'); ?></div>
		<div style="grid-area: c;" ><?=$dlm_download->get_version()->get_filetype(),' (',$dlm_download->get_version()->get_filesize_formatted(),')'; ?></div> 
		<div style="grid-area: d;" ><a href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow" onclick="_gaq.push(['_trackEvent', 'Download', 'PDF', '<?php $dlm_download->the_title(); ?>']);"><span class="icon icon--4x icon--download"></span></a>
</div-->


<table class="downloads">
	<tr>
		<td><a class="download track" href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow" 
			onclick="
				dataLayer.push({
				    'event': 'GAevent',
				    'eventCategory': 'Download',
				    'eventAction': 'Downloadmanager',
				    'eventLabel': '<?php $dlm_download->the_title(); ?>'
				});"><?php $dlm_download->the_image(); ?></a></td>
		<td><a class="download track" href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow" onclick="
				dataLayer.push({
				    'event': 'GAevent',
				    'eventCategory': 'Download',
				    'eventAction': 'Downloadmanager',
				    'eventLabel': '<?php $dlm_download->the_title(); ?>'
				});"><h2><?php $dlm_download->the_title(); ?></h2></a>
	        <div><?php $dlm_download->the_excerpt(); ?></div>
	        <div><?php $dlm_download->get_version()->has_version_number() ? printf( __( 'Ver. %s', 'download-monitor' ), $dlm_download->get_version()->get_version_number() )  : '' ?></div>
			<div><?=$dlm_download->get_version()->get_date()->format('Y-m-d'); ?></div>
			<div class="download-link filetype-icon <?php echo 'filetype-' . $dlm_download->get_version()->get_filetype(); ?>">
				<a class="download track" href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow" onclick="
					dataLayer.push({
						'event': 'GAevent',
						'eventCategory': 'Download',
						'eventAction': 'Downloadmanager',
						'eventLabel': '<?php $dlm_download->the_title(); ?>'
					});">
					<?=$dlm_download->get_version()->get_filetype(),' (',$dlm_download->get_version()->get_filesize_formatted(),')'; ?>
				</a>
			</div> 

		</td>
		<!--td><a href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow" onclick="_gaq.push(['_trackEvent', 'Download', 'PDF', '<?php $dlm_download->the_title(); ?>']);"><span class="icon icon--4x icon--download"></span></a></td-->
	</tr>
</table>
