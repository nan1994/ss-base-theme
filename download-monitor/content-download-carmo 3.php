<?php
/**
 * Detailed download output
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/** @var DLM_Download $dlm_download */

?><div role="grid" style="display: table" class="downloads">
    <!--div role="row" style="display: table-row">
        <div role="gridcell" style="display: table-cell"></div>
        <div role="gridcell" style="display: table-cell"></div>
        <div role="gridcell" style="display: table-cell"></div>
    </div-->

    <a role="row" style="display: table-row" href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow" onclick="_gaq.push(['_trackEvent', 'Download', 'PDF', '<?php $dlm_download->the_title(); ?>']);">
        <div role="gridcell" style="display: table-cell"><?php $dlm_download->the_image(); ?></div>
        <div role="gridcell" style="display: table-cell"><h3><?php $dlm_download->the_title(); ?></h3></div>
        <div role="gridcell" style="display: table-cell"><?php $dlm_download->the_excerpt(); ?></div>
        <div role="gridcell" style="display: table-cell"><?php $dlm_download->the_excerpt(); ?></div>
        <?php if ( $dlm_download->get_version()->has_version_number() ) {
			printf( __( 'Ver. %s', 'download-monitor' ), $dlm_download->get_version()->get_version_number() );
		} ?>
		<div role="gridcell" style="display: table-cell"><?php echo $dlm_download->get_version()->get_date()->format('Y-m-d'); ?></div>
		<div role="gridcell" style="display: table-cell" ><?php echo $dlm_download->get_version()->get_filetype(); ?> (<?php echo $dlm_download->get_version()->get_filesize_formatted(); ?>)</div> 
    </a>
</div>
<? /*
<table class="download_table"> 
<thead> 
	<th width="100" scope="col">&nbsp;</th> 
	<th width="350" scope="col">&nbsp;</th> 
	<th width="150" scope="col"><div align="left"></div></th> 
	<th width="100" scope="col"><div align="left"></div></th> 	
</thead> 
<tr> 
	<a class="" href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow" onclick="_gaq.push(['_trackEvent', 'Download', 'PDF', '<?php $dlm_download->the_title(); ?>']);">
	<td rowspan="2">
		<?php $dlm_download->the_image(); ?>
		
	</td> 
	<td colspan="3"><h3><?php $dlm_download->the_title(); ?></h3></td> 	
</tr>
<tr> 
		<td valign="top"><?php $dlm_download->the_excerpt(); ?></td> 
		<td valign="top">
			<?php if ( $dlm_download->get_version()->has_version_number() ) {
			printf( __( 'Ver. %s', 'download-monitor' ), $dlm_download->get_version()->get_version_number() );
		} ?>
			<?php echo $dlm_download->get_version()->get_date()->format('Y-m-d'); ?>
		</td> 
		<td valign="top"><?php echo $dlm_download->get_version()->get_filetype(); ?> (<?php echo $dlm_download->get_version()->get_filesize_formatted(); ?>)</td> 
		
</tr>
</a> 
<!--tr>
	<td></td>
	<td colspan="3">
		<input type="checkbox" name="catalog1" id="catalog1"> Keep me updated
	</td>
</tr--> 
</table>
*/ ?>