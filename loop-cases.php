<?php if(get_field('case_stories')): ?>
	<?php while(has_sub_field('case_stories')): ?>
	<div class="slides__item">
		<div class="box box--case">
			<h3 class="title"><?php _e( 'Case Story', 'html5blank' ); ?></h3>
			<p><?php the_sub_field('case_description'); ?></p>
			<a class="read-more" href="<?php the_sub_field('case_url'); ?>">Read more</a>
		</div><!-- /box -->

		<a class="box__image" href="<?php the_sub_field('case_url'); ?>"><?php

		$image = get_sub_field('case_image');

        // vars
		$url = $image['url'];

        // thumbnail
		$size = 'thumbnail';
		$thumb = $image['sizes'][ $size ];
		$width = $image['sizes'][ $size . '-width' ];
		$height = $image['sizes'][ $size . '-height' ];

		if( !empty($image) ): ?>

		<img src="<?php echo $thumb; ?>" alt="" width="360" height="160" />

	<?php endif; ?></a>


</div><!-- /slides__item -->
<?php endwhile; ?>
<?php endif; ?>