<?php
include_once ('./include/cloudinary/src/Cloudinary.php');
include_once ('./include/cloudinary/settings.php');

$sprog = (ICL_LANGUAGE_CODE == 'uk' ? '' : '/'.ICL_LANGUAGE_CODE); 

if ( is_page_template( array('template-product.php','template-item.php' ) ) ) 
{

	// Get a random contact from the contacts on the contact page (only sales)

	//$repeater = get_field('sales_contacts',icl_object_id(1690, 'page', false,ICL_LANGUAGE_CODE));
	$repeater = get_field('sales_contacts',apply_filters( 'wpml_object_id', 1690, 'page', false, ICL_LANGUAGE_CODE));
	
	$contacts = array($repeater[array_rand($repeater,1)]); 
	//$contacts = get_field('sales_contacts',icl_object_id(347, 'page', false,ICL_LANGUAGE_CODE));


} elseif (!have_rows('sales-contacts')) {

	// Get contact from front page
	$contacts = get_field('sales_contacts',icl_object_id(347, 'page', false,ICL_LANGUAGE_CODE));

} else {
	$contacts = get_field('sales_contacts');
}

if( $contacts ): ?>

	<div class="contacts">
    <?php foreach( $contacts as $contact): the_row();// variable must be called $post (IMPORTANT) ?>
	    <div class="contact">
	        <!--img class="contact__image" src="<?php the_field('contact_profile_picture',$contact->ID); ?>" alt="<?=$contact->ID?>" width="43" height="43"-->
			<img class="contact__image" src="<?=cloudinary_url("People/".get_field('contact_picture_url', $contact->ID), 
				array(
					"gravity"=>"face",
					"effect"=>"grayscale",
					"quality"=>"jpegmini",
					"sign_url"=>true, 
					"height"=>60, 
					"radius"=>60, 
					"width"=>60, 
					"crop"=>"thumb",
					"zoom"=>0.80,
					array("effect"=>"sharpen:100")
					))?>" alt="<?=$contact->ID?>" width="60" height="60" />
				<div class="addresscard contact__details">
	            <div class="fn"><?php echo get_the_title( $contact->ID ); ?></div><?php
	            if( get_field('contact_phone',$contact->ID) ) : ?>
					<div><?php _e('Tel.','html5blank')?> : <span class="tel"><a href="tel:<?php the_field('contact_phone', $contact->ID); ?>"><?php the_field('contact_phone',$contact->ID); ?></a></span></div><?php
				endif;
				if( get_field('contact_fax',$contact->ID) ) : ?>
					<div><?php _e('Fax.','html5blank'); ?> : <span class="fax"><?php the_field('contact_fax',$contact->ID); ?></span></div><?php
				endif; ?>
				<a href="<?=get_permalink(apply_filters( 'wpml_object_id', 1690, 'page', false,ICL_LANGUAGE_CODE))?>"><?php _e('Send a request','html5blank') ?></a>
	        </div><!-- /contact__details -->
	    </div>
    <?php endforeach; ?>
	</div>
    <?php //wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>





