<?php
function itemtableweb($itemnumber)
{

require_once($_SERVER["DOCUMENT_ROOT"].'/_catalog/include.php');
//include_once('electrodewebfunction.php');
	
global $wpdb;
global $link;
global $sprog;
global $iconpath;
global $picturepath;
global $iconpath;
global $drawingpath;
global $datasheets;
global $drawingwidth;
global $picturewidth;
global $catalogwidth;	

$drawingwidth=100;
$picturewidth=100;


				
$texts = array ('Click for larger picture','Nominal measurements in mm','fits with:','mounts with:','Customers also buy','Interested in other colors or materials? Please call for quote...','Full description','Download Productsheet',"Material","Color");
};


// Write sql to get the selected product --- and check if 
// 	a.it has a valid state (not in develoment and not discontinued) and 
// 	b. is not blocked to specific customers	
// 	c. has actual variants open in NAV
	$sql = $wpdb->prepare("SELECT p.item, categories.new_path as webfolder, t.webname, t.descriptionfull as description, t.descriptionshort as description_short, fitswith, p.drawing,p.tabletype,p.includes, p.mountswith,
				GROUP_CONCAT( DISTINCT (v.material) SEPARATOR ', ') AS materials,
				GROUP_CONCAT( DISTINCT (v.electrode) SEPARATOR ', ') AS electrode,
				IF(MAX(v.color1) IS NULL,NULL,IF(MAX(v.color2) IS NULL,1,2)) as nocolors,
				COUNT(DISTINCT v.itemnumber) as nVariants
				FROM _products p 
				LEFT join variants v ON p.includes LIKE concat('%',v.item,'%') COLLATE utf8_unicode_ci
				LEFT JOIN _texts t on t.item = p.item
				LEFT JOIN categories ON p.webcategory = categories.categoryid AND categories.sprog = t.sprog
				WHERE p.item = %s and t.sprog= %s
				and IFNULL(p.blocked,0) = 0 
				and p.status in (1,2)
				group by p.item, categories.new_path, t.webname, t.descriptionfull, t.descriptionshort, fitswith, p.drawing,p.tabletype,p.includes, p.mountswith
				having nVariants > 0",
				$itemnumber,
				$sprog);
	
	$result1 = $wpdb->get_row($sql);
	
	

	if (mysqli_num_rows($result1) > 0)
	{    
		//echo "Read rows from DB";
		
	
		
		$product = mysqli_fetch_array($result1);
		$electr = $product['electrode'];
		
		$includes = explode(',',$product['includes']);	
		$includes = array_map( 'trim', $includes );
		$includes = "'".implode("','",$includes)."'";

/********************************************************************************************************* 
*
*								Treat Electrodes seperately
*
***********************************************************************************************************/
if ($product['tabletype'] == 2 && $product['item'] <> '06-926')  // machines & electrodes But EXCL Carmo SEAL
{
	//echo('electrode');
	electrodetableweb($product);
}

else // products
{

if ($sprog == 'de' ) {		
		$sql2 = "SELECT variants.*, nameDE as name,farve1.D AS farve1,farve2.D AS farve2 FROM variants 
		LEFT join farvekoder farve1 ON farve1.code = variants.color1
		LEFT JOIN farvekoder farve2 ON farve2.code = variants.color2
		WHERE item in (".$includes.") ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";		
		
		$sql3 = "SELECT DISTINCT v.col1borderhex,v.col1fillhex,v.col2borderhex,v.col2fillhex,farve1.D AS farve1,farve2.D AS farve2 
		FROM variants v
		LEFT join farvekoder farve1 ON farve1.code = v.color1
		LEFT JOIN farvekoder farve2 ON farve2.code = v.color2
		WHERE item IN (".$includes.")";}	// ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";}

elseif ($sprog == 'fr' ) {	
		$sql2 = "SELECT variants.*, nameFR as name,farve1.FR AS farve1,farve2.FR AS farve2 FROM variants 
		LEFT join farvekoder farve1 ON farve1.code = variants.color1
		LEFT JOIN farvekoder farve2 ON farve2.code = variants.color2
		WHERE item in (".$includes.") ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";	
		
		$sql3 = "SELECT DISTINCT v.col1borderhex,v.col1fillhex,v.col2borderhex,v.col2fillhex,farve1.FR AS farve1,farve2.FR AS farve2 
		FROM variants v
		LEFT join farvekoder farve1 ON farve1.code = v.color1
		LEFT JOIN farvekoder farve2 ON farve2.code = v.color2
		WHERE item IN (".$includes.")";}	// ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";}

else	{		
		$sql2 = "SELECT variants.*, webname as name,farve1.UK AS farve1,farve2.UK AS farve2 FROM variants 
		LEFT join farvekoder farve1 ON farve1.code = variants.color1
		LEFT JOIN farvekoder farve2 ON farve2.code = variants.color2
		LEFT JOIN _texts t on t.item = variants.item and t.sprog='gb'
		WHERE variants.item in (".$includes.") ORDER BY variants.item, material ASC, stock DESC, col1name ASC, itemnumber asc";	
		
		$sql3 = "SELECT DISTINCT v.col1borderhex,v.col1fillhex,v.col2borderhex,v.col2fillhex,farve1.UK AS farve1,farve2.UK AS farve2 
		FROM variants v
		LEFT join farvekoder farve1 ON farve1.code = v.color1
		LEFT JOIN farvekoder farve2 ON farve2.code = v.color2
		WHERE item IN (".$includes.")";}	// ORDER BY item, material ASC, stock DESC, col1name ASC, itemnumber asc";}		



		
		$result2 = mysqli_query($link,$sql2);
		if (mysqli_errno($link))
		echo "Error " . mysqli_errno($link) . ": ". mysqli_error($link);		
/*		if (mysqli_num_rows($result1) > 0)
	{  
*/
if($sql3) { $result3 = mysqli_query($link,$sql3);
		if (mysqli_errno($link))
		echo "Error " . mysqli_errno($link) . ": ". mysqli_error($link);	}
	{

/********************************************************************************************************* 
*
*								Simpel table for Carmo Seal
*
***********************************************************************************************************/
if ($product['tabletype'] == 2 && $product['item'] == '06-926' )  // machines & electrodes But EXCL Carmo SEAL
{
/*		$result2 = mysqli_query($sql2);
		if ($result2 == 0)
		echo "Error " . mysqli_errno() . ": ". mysqli_error();		
*/		if (mysqli_num_rows($result2) > 0)
	{  
?>
	<table width='100%' border='0' cellpadding='0' class='item' >
  <tr>
	<th  style="border-right-width:0px;" width = '<?=$picturewidth?>px' ><?=$product['item']?> - <?=$product['webname']?></span>

    
    </th>

  </tr>
  
  <?php // Insert description 
	if (strlen($product['description'])>0) 
	  {
	  ?><tr><td><span class='description' ><?=$product['description_short']?></span></td></tr><?php
	  }
?>
<tr><td>

<table width='100%' class='variant'>
        <th  style='width:60px;' bgcolor='<?=$headingcolor?>'>Order#</th>
        <th   bgcolor='<?=$headingcolor?>'>Description</th>
        <th  style='width:25px;' bgcolor='<?=$headingcolor?>'>From stock</th>
        <th  bgcolor='<?=$headingcolor?>' >comments</th>
        </tr> 
 
<?php 
          $i = 0;
		  while($row = mysqli_fetch_array($result2))
            {
				$i++;
				  ($i%2 == 0)?$rowclass="grey_row":$rowclass="white_row";
				?>
				<tr class ='<?=$rowclass?>' onMouseOver="this.className='highlight'" onMouseOut="this.className='<?=$rowclass?>'">
				<td class ='leftcell'  align='left' valign='middle' ><?=$row['itemnumber']?></td>
				<td class ='innercell'  align='left' valign='middle' ><?=$row['name']?></td>
				<td  class ='innercell'  align='middle' valign='middle' >
					<?php if ($row ['stock']== 'Yes') {
					//echo "Yes";} 
					echo "<p class = 'thumbnail'><img src='".$iconpath."yes.png' alt=''/><span>Delivered from stock</span></p>";}
					else {echo "<p class = 'thumbnail'><img src='".$iconpath."call.png' alt=''/><span>Produced to order - Call for quote</span></p>";}?>
				</td>
                
                
				<td class ='rightcell'  align='left' valign='middle' ></td>
				</tr><?php
            }
?>
<!--
		<tr>
        <td height="4"><hr /></td>
        <td height="4"><hr /></td>
        <td height="4"><hr /></td>
		<td height="4"><hr /></td>
        </tr>
-->
    </table>  
</table>
  <?php

	}
}
/********************************************************************************************************* 
*
*								Full table for all other products
*
***********************************************************************************************************/
elseif (mysqli_num_rows($result2) > 0) {
?>


	<div itemscope itemtype="http://schema.org/Product">
	<a name="<?=$itemnumber?>" id="p<?=$itemnumber?>"></a>
    <meta itemprop='productID' content='sku:<?=$itemnumber?>'/>
    <meta itemprop='manufacturer' content='Carmo A/S'/>
    <meta itemprop='brand' content='Carmo'/>
    <meta itemprop='url' content="http://<?php echo $_SERVER["HTTP_HOST"].$product['webfolder'].$product['item']?>/"/>
    
	<table width='100%' border='0' cellpadding='0' class='item' >
  <tr>
	<th  style="border-right-width:6px;" width = '<?=$picturewidth?>px' ><?=$itemnumber?></th>
	<th colspan="2" ><span itemprop="name"><?=$product['webname']?></span>

    
    </th>

  </tr>

 <tr>
 <!-- Insert product photo -->
 <?php if ($product['tabletype'] == 1 || $product['tabletype'] != 2) { ?>
	<td width = '<?=$picturewidth?>' valign='top'>

<?php
	if (file_exists($_SERVER['DOCUMENT_ROOT'].$picturepath.$itemnumber.".jpg")) 
	{
		$imagefilename = $product['webfolder'].filenamesafe($product['webname']).'_'.$product['item'].'.jpg';
		//$size1 = getimagesize($_SERVER['DOCUMENT_ROOT'].'thumbs/120x120'.$picturepath.$itemnumber.'.jpg');
		//$ratio = ($size[1]/$size[0]);
		$size1 = getimagesize($_SERVER['DOCUMENT_ROOT'].'/thumbs/120x120'.$imagefilename);
		$size2 = getimagesize($_SERVER['DOCUMENT_ROOT'].'/thumbs/200x200'.$imagefilename);

		?><span class="thumbnail2"><?php 
		if (preg_match("/^\d{2,2}-\d{3,3}$/", $itemnumber)) echo "<a href='http://".$_SERVER["HTTP_HOST"].$product['webfolder'].$product['item']."/' >" ?>
        	<img class='mainthumb' itemprop="image" src="http://<?=$_SERVER["HTTP_HOST"]?>/thumbs/120x120<?=$imagefilename?>" width="<?=$size1[0]?>" height="<?=$size1[1]?>" alt="<?=strip_tags(htmlspecialchars($product['webname'].'. '.$product['description_short']))?>" border="0"/><?php
		if (preg_match("/^\d{2,2}-\d{3,3}$/", $itemnumber)) echo "</a>" ?>
     
					<span class="noprint" style='font-style: italic;width:200px;'><img src="http://<?=$_SERVER["HTTP_HOST"]?>/thumbs/200x200<?=$imagefilename?>" alt="<?=strip_tags(htmlspecialchars($product['webname'].'. '.$product['description_short']))?>" width="<?=$size2[0]?>" height="<?=$size2[1]?>" class="noprint" /> <br /> 
	  <?=$product['item'].' '.$product['webname']?></span></span> 
        
        <?php         
//		echo "<img class='main' src='".$picturepath.$itemnumber.".jpg' width='$picturewidth' alt='".$itemnumber." ".$product['catalognameuk']."' />"; 
		
	} 
		?>
        
     
        
        
	 </td>
     <?php } // Ends section if photo is needed?> 
<!-- Here begins middle detail part -->	
 
	<td class='description' valign='top' <?php if ($product['tabletype'] == 2) echo 'colspan="2"' ?> >
	
<?php // Insert description 
	if (strlen($product['description'])>0) 
	  {
	  ?><p ><span class='description' itemprop="description"><?=$product['description_short']?></span></p><?php
	  }
?>
<table width="100%" class="properties">
  <tr>
    <td width="50px" valign="top" ><strong><?=$texts[8]?></strong></td>
    <td><?=$product['materials']?></td>
  </tr>
  <tr>
    <td valign="top"><strong><?=$texts[9]?></strong></td>
    <td>
 
       <?php
          $i = 0;
		  while($row = mysqli_fetch_array($result3))
                  { 
				  if ($i++ >0 && $product['nocolors'] == 2 ) echo "|";
 
?> 
    <span class = 'thumbnail1'><div class="colorsprite x<?=substr($row ['col1borderhex'],-6)?>x<?=substr($row ['col1fillhex'],-6)?>" ></div><span class='noprint'><?=$row ['farve1']?></span></span>			 
			 <?php if ($row ['farve2']<>null) {?> <span class = 'thumbnail1'><div class="colorsprite x<?=substr($row ['col2borderhex'],-6)?>x<?=substr($row ['col2fillhex'],-6)?>" ></div><span class='noprint'><?=$row ['farve2']?></span></span> <?php } ?>
    
    
    
    <?php } ?>
    
    
    </td>
  </tr>


<?php
		// List fits with if relevant
		if (strlen($product['fitswith'])>1)        
		  {	echo "<tr>";
			  echo "<td width = '65' valign = 'top' align='left' ><strong>".$texts[2]."</strong></td> ";
			  ?><td><?php
				$fitswithresult = fitswith($product['fitswith']);
				if (mysqli_num_rows($fitswithresult) > 0)
				{
					if ($cellwidth == 0) $cellwidth = 30;
					$picheight = intval($cellwidth * 2 / 3);
					$kolonner = mysqli_num_rows($fitswithresult);
					
					mysqli_data_seek ( $fitswithresult , 0 ); // reset result
					
							for ($i = 0; $i<$kolonner; $i++) 
							{ 
								$q = mysqli_fetch_array($fitswithresult); 
								
								$picture = $picturepath.$q[0].".jpg";
								if (file_exists($_SERVER['DOCUMENT_ROOT'].$picture)) 
									{
										
										$size = getimagesize($_SERVER['DOCUMENT_ROOT'].$picture);
										$ratio = ($size[1]/$size[0]);		
										?><a class='thumbnail' style ="font-size:11px; border-bottom: 3px double; color: black;" href="http://<?=$_SERVER["HTTP_HOST"].$q[2]."#p".$q[0] ?>" >
                                      
												<?=$q[0]?>
												<span class='noprint' style='font-style: italic;width:150px;font-size:11px;'>
													<img class='noprint' src="http://<?=$_SERVER["HTTP_HOST"]?>/thumbs/150x150<?=$picture?>" width="150" height="<?=intval(150*$ratio)?>" alt="<?=$q[1]?>" border="0" /><br />
													<?=$q[1]?>
												</span></a><?php
									}
									else 
									{ ?> <img src='/_catalog/emptysquare.php?width=<?=$cellwidth?>' width=<?=$cellwidth?>' alt=''/> <?php }
								}  
				 
			  }
			  mysqli_free_result($fitswithresult);
		  } ?>
   </td>
   </tr>
   </table>       
          
          


<?php
			if ($product['tabletype'] == 1) echo "<p class = 'description'>".$texts[5]."</p>";
			/*
		  echo "</br>";
		  $datasheetfile = urlencode($datasheets.$product["dataleaf"]);
		  echo $datasheetfile;
		  if ( (strlen($product["dataleaf"]) > 0 ) && (file_exists($datasheetfile) == 1) )
		  {
			?><img src='<?=$iconpath."pdficon.jpg"?>' alt='' /> Download <a href='file://<?=$datasheetfile?>' target='_blank'>datasheet</a><?php
			}
			*/
?>

<?php if($GLOBALS['NonLimit']) { ?><span style = "vertical-align:middle; font-size:inherit;"><span class='iconsprite carmosprite pdf2' style="padding-right:3px;vertical-align:middle;"></span><a href='/_catalog/datasheet.php?item=<?=$itemnumber?>&lang=<?=$sprog?>' style="font-size:inherit;" target="_blank" rel="nofollow noopener noreferrer" onclick="_gaq.push(['_trackEvent','Download','Productsheet-overview','<?=$product['item']?>']);"><?=$texts[7]?></a></span> <?php } ?>

</td>
<!-- Here begins third drawing part -->
<td valign='top'>
 <?php
			if (strlen($product["drawing"]) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'].$drawingpath.$product["drawing"]))
			{
				//echo $product["drawing"]."<br>";
				//echo $media;
				$size1 = getimagesize($_SERVER['DOCUMENT_ROOT'].'/thumbs/120x120'.$drawingpath.$product["drawing"]);
				$size2 = getimagesize($_SERVER['DOCUMENT_ROOT'].'/thumbs/200x200'.$drawingpath.$product["drawing"]);
				//$ratio = ($size[1]/$size[0]);
				?>
				<span class="thumbnail" >
                	<?php if (preg_match("/^\d{2,2}-\d{3,3}$/", $itemnumber)) echo "<a href='http://".$_SERVER["HTTP_HOST"].$product['webfolder'].$product['item']."/' >" ?>
					<img class='mainthumb' src='http://<?=$_SERVER["HTTP_HOST"]?>/thumbs/120x120<?=$drawingpath.$product["drawing"]?>' width='<?=$size1[0]?>' height='<?=$size1[1]?>' alt='Small drawing of <?= ($product["item"]." ".$product['webname'])?>' />
					<?php if (preg_match("/^\d{2,2}-\d{3,3}$/", $itemnumber)) echo "</a>" ?>
					<span class="noprint" style='font-style: italic;width:200px;'><img class="noprint" src="http://<?=$_SERVER["HTTP_HOST"]?>/thumbs/200x200<?=$drawingpath.$product["drawing"]?>" width ='<?=$size2[0]?>' height='<?=$size2[1]?>' alt='Big drawing of <?=($product["item"]." ".$product['webname'])?>'/><br /><?=$texts[1]?></span>
				</span>
				
				<p class='drawingfootnote'><?=$texts[1]?></p>
				 <?php /*if (preg_match("/^\d{2,2}-\d{3,3}$/", $itemnumber)) echo "<a href='http://".$_SERVER["HTTP_HOST"].parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH).$itemnumber."/' ><span class='button' >".$texts[6]."</span></a>"; 
			}
		   else {} */

 ?>  
 
 				 <?php 
			}
		   else {}

if (preg_match("/^\d{2,2}-\d{3,3}$/", $itemnumber)) echo "<a href='http://".$_SERVER["HTTP_HOST"].$product['webfolder'].$product['item']."/' onclick=\"_gaq.push(['_trackEvent','Link','Detailbutton','".$product['item']."']);\"><span class='button' >".$texts[6]."</span></a>"; 
 ?>  
 </td>



</tr>


</table>
</div>
<?php
} // ends only for products
}
mysqli_free_result($result2);}

} 
mysqli_free_result($result1);
?>


