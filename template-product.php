<?php 
/* 
Template Name: Product Page
Template Post Type: page, product
*/

require_once('include_wp.php');

$pictureusagepath = "/images/products/usage/";

$category = get_field('webcategory');
 	
$sprog = (ICL_LANGUAGE_CODE == 'en' ?  'gb' : ICL_LANGUAGE_CODE);

$sql = "SELECT * from categories where sprog ='".$sprog."' and categoryid = ".$category;
$row = $wpdb->get_row( $sql , ARRAY_A);

get_header(); ?>

<div class="container">
	<div class="row">
		<div class="content">

			<nav class="nav--breadcrumbs" role="navigation">
				<?php the_breadcrumb(); ?>
			</nav>

			<header>
				<h1 class="producttemplate"><?php the_title(); ?></h1>
			</header>
			
			<aside class="sidebar" role="complementary">
				
				<!-- Webcategory picture -->
				<?php
				if ( $row ) : ?>
					<p>
					<picture>
					  <source type="image/webp" srcset="<?=site_url();?>/thumbs/230x230xSH<?=$row['new_path'].sanitize_file_name($row['imagename']).'-('.$row['categoryid'].')'?>.webp">
					  <source type="image/jpeg" srcset="<?=site_url();?>/thumbs/230x230xSH<?=$row['new_path'].sanitize_file_name($row['imagename']).'-('.$row['categoryid'].')'?>.jpg">
					  <img src="<?=site_url();?>/thumbs/230x230xSH<?=$row['new_path'].sanitize_file_name($row['imagename']).'-('.$row['categoryid'].')'?>.jpg" alt="<?=strip_tags(htmlspecialchars($row['imagealt']))?>" title="<?=strip_tags(htmlspecialchars($row['menu']))?>" border="0" width="230"/>
					</picture>
					</p>
				<?php endif; ?>	
				<!-- /Webcategory picture -->
				<?php
				// Insert usage image if exists
				if (strlen($row["imageusage"]) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'].$pictureusagepath.$row["imageusage"]))
				{
					$size = getimagesize($_SERVER['DOCUMENT_ROOT'].$pictureusagepath.$row["imageusage"]);
					$ratio = ($size[1]/$size[0]);
					?><img class='' src='<?=site_url();?>/thumbs/230x230<?=$pictureusagepath.$row["imageusage"] ?>' width='230' height='<?=intval(230*$ratio)?>' alt='' style="padding-top:20px;" align="middle"/>
					<?php	
					
				} 	?>		
				<?php the_field('post_sidebar'); ?>
			
			</aside><!-- /sidebar --><main role="main" class="main block">
			
				<section>



				<?php if (have_posts()): while (have_posts()) : the_post(); ?>

					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<?php the_content(); ?>

						<?php comments_template( '', true ); // Remove if you don't want comments ?>

						<br class="clear">

						<?php edit_post_link(); ?>

						<!-- Product tables start here --><?php
						// check for rows (parent repeater)
						if( have_rows('product_family') ): 
							require_once('itemtable.php');?>
								<h2><?php _e('Ordering and variant information' , 'html5blank'); ?></h2>
								<p><em><?php _e('All measurements and drawings are nominal. For exact measurements - and for critical integration - please contact sales for detailed drawings.', 'html5blank'); ?></em></p>
								
								<div><?php 
									// loop through rows (parent repeater)
									while( have_rows('product_family') ): the_row(); ?>
										<h3><?php echo get_sub_field('title'); ?></h3><hr>
										<?php 
											// check for rows (sub repeater)
											//if( have_rows('products') ) :
												// loop through rows (sub repeater)
												while( have_rows('products') ) : the_row();
													if (get_sub_field('active') ) {
															//echo (get_sub_field('dessin'));
															itemtableweb(get_sub_field('dessin')); 

														}
												endwhile;
											//endif; //if( get_sub_field('items') ): ?>
										<?php 
									endwhile; // while( has_sub_field('to-do_lists') ): ?>
								</div>
								<hr>
								<h5><?php _e('(*) Smaller quantities can be delivered on request for a repacking fee.' , 'html5blank');?></h5>
								<?php 

						endif; // if( get_field('to-do_lists') ): ?>

						<!-- End of product tables -->
						


					</article>
					<!-- /article -->

				<?php endwhile; ?>



						
				<?php else: ?>

					<!-- article -->
					<article>

						<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

					</article>
					<!-- /article -->

				<?php endif; ?>

				</section>
				<!-- /section -->
			</main><?php get_sidebar('products'); ?>
		</div><!-- /content -->
	</div>
</div>

<?php
get_footer();