<?php

//Template name: Contact

include_once ('./include/cloudinary/src/Cloudinary.php');
include_once ('./include/cloudinary/settings.php');

get_header();
?>

<div class="page-custom-design">
    <main id="main" class="site-main">
        <?php 
            $info_title = get_field('info_title');
            $form_title = get_field('form_title');
            $acf_map = get_field('acf_map');
            $general_information = get_field('general_information', 'options');
        ?>
        <div class="custom-design-box">
            <div class='container'>
                <div class='row'>
                    <div class='col-12'>
                        <div class="title">
                            <?= the_title(); ?>
                        </div>
                    </div>
                    <?php if(has_post_thumbnail()): ?>
                        <div class="col-12">
                            <div class="banner-image b-lazy" data-src="<?= get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>"></div>
                            <?php if(false): ?>
                                <div class="acf-map">
                                    <div class="marker" data-lat="<?= $acf_map['lat']; ?>" data-lng="<?= $acf_map['lng']; ?>">
                                        <a target="" href="#"><?= $general_information['address']; ?></a>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php $sales_contacts = get_field('sales_contacts') ?>
                            <?php if($sales_contacts): ?>
                                <div class="contact-wrap">
                                    <div class="title"><?= $sidebar_labels_contact_sales_label; ?></div>
                                    <?php foreach($sales_contacts as $contact): ?>
                                        <?php $contact_id = $contact->ID ?>
                                        <?php $contact_phone = get_field('contact_phone', $contact_id) ?>
                                        <?php $contact_img = get_field('contact_profile_picture', $contact_id) ?>

                                        <div class="content-box">
                                            <div class="img-wrap">
                                                <img src="<?=cloudinary_url("People/".get_field('contact_picture_url', $contact->ID), 
                                                    array(
                                                        "gravity"=>"face",
                                                        "effect"=>"grayscale",
                                                        "quality"=>"jpegmini",
                                                        "sign_url"=>true, 
                                                        "height"=>70, 
                                                        "radius"=>70, 
                                                        "width"=>70, 
                                                        "crop"=>"thumb",
                                                        "zoom"=>0.80,
                                                        array("effect"=>"sharpen:100")
                                                        ))?>" alt="<?= $contact->post_title; ?>" width="70" height="70" />
                                            </div>
                                            <div class="info-box">
                                                <div class="name"><?= $contact->post_title; ?></div>
                                                <div class="phone"><?= $contact_phone ?></div>
                                                <div class="btn-link">
                                                    <a href="<?= $sidebar_labels_send_a_request_link; ?>"><?= $sidebar_labels_send_a_request_label; ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>

                        </div>
                    <?php endif; ?>
                </div>
                <div class="contact-info-row row justify-content-center">
                    <div class="col-md-8">
                        <h3><?= $info_title; ?></h3>
                        <div class="content">
                            <?= the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

<?php
get_footer();