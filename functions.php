<?php


/**
 * aa-base functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package aa-base
 */

require get_template_directory() . '/inc/cpt.php';

if ( ! function_exists( 'aa_base_setup' ) ) :

	function aa_base_setup() {

		load_theme_textdomain( 'ss_text_domain', get_template_directory() . '/languages' ); // Translations for new part
		load_theme_textdomain( 'html5blank', get_template_directory() . '/languages' ); // Translations for old part

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );

        add_image_size('avatar', 43, 43, true); // Square Product Image
        add_image_size('square', 230, 230, true); // Square Product Image
        add_image_size('hero', 1000, 350, true); // Hero Slider Image
        add_image_size('featured', 960, 250, true); // Custom Header Size call using the_post_thumbnail('featured');

		//add these for woocommerce support
		add_theme_support( 'woocommerce' );
		add_theme_support( 'customize-selective-refresh-widgets' );
		add_theme_support( 'html5', array( 'search-form', 'gallery', 'caption', ) );

		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );

		add_theme_support( 'align-wide' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'header-menu'    => esc_html__( 'Header', 'aa-base' ),
			'footer-menu'    => esc_html__( 'Footer', 'aa-base' ),
			'footer-menu-1'  => esc_html__( 'Footer1', 'aa-base' ),
			'footer-menu-2'  => esc_html__( 'Footer2', 'aa-base' ),
			'footer-menu-3'  => esc_html__( 'Footer3', 'aa-base' ),
		) );

	}

endif;

add_action( 'wp_enqueue_scripts', 'old_style' );
function old_style(){

    if ( is_page_template( 'template-product.php' ) || is_page_template( 'template-item.php' ) ) {
        wp_enqueue_style( 'old-style', get_template_directory_uri() . '/assets/css/admin/old-style.css', array(), filemtime(get_template_directory() . '/assets/css/admin/old-style.css') );
    } else {
        // about.php is not used
    }
}

add_action( 'after_setup_theme', 'aa_base_setup' );

/**
 * Enqueue admin scripts and styles.
 */
function aa_admin_scripts() {
	//wp_enqueue_script( 'multi', get_template_directory_uri() . '/assets/js/admin/multi.js', array(), '1.0.0', false );
	//wp_enqueue_script( 'admin_custom_scrip', get_template_directory_uri() . '/assets/js/admin/admin_custom_script.js', array(), '2.0.0', false );
	//wp_enqueue_style( 'admin-style', get_template_directory_uri() . '/assets/css/admin/admin-style.css', array(), '1.0' );
	//wp_enqueue_style( 'multi', get_template_directory_uri() . '/assets/css/admin/multi.css', array(), '1.0' );

	wp_localize_script( 'wp-embed', 'ajax_url',
		admin_url( 'admin-ajax.php' ) );
}

add_action( 'admin_enqueue_scripts', 'aa_admin_scripts', 999 );

/**
 * Enqueue scripts and styles.
 */
function aa_base_scripts() {
	wp_dequeue_script( 'wp-embed' );
	wp_enqueue_style( 'aa-base-wp-style', get_stylesheet_uri() );

	wp_localize_script( 'wp-embed', 'ajax_url',
		admin_url( 'admin-ajax.php' ) );
}

add_action( 'wp_enqueue_scripts', 'aa_base_scripts', 999 );


/**
 * Register widget area.
 */
function ss_base_widgets_init() {

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'aa-base' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'aa-base' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Group Footer Boxes', 'aa-base' ),
		'id'            => 'group-footer-boxes',
		'description'   => esc_html__( 'Add widgets here.', 'aa-base' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Box Left', 'aa-base' ),
		'id'            => 'footer-box-left',
		'description'   => esc_html__( 'Add widgets here.', 'aa-base' ),
		'before_widget' => '<div id="%1$s" class="footer-box">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="box-title">',
		'after_title'   => '</div>',
		'before_text'   => '<div class="text">',
		'after_text'    => '</div>',
		'before_link'   => '<a href="',
		'after_link'    => '">',
		'link_close'    => '</a>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Box Middle', 'aa-base' ),
		'id'            => 'footer-box-middle',
		'description'   => esc_html__( 'Add widgets here.', 'aa-base' ),
		'before_widget' => '<div id="%1$s" class="footer-box">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="box-title">',
		'after_title'   => '</div>',
		'before_text'   => '<div class="text">',
		'after_text'    => '</div>',
		'before_link'   => '<a href="',
		'after_link'    => '">',
		'link_close'    => '</a>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Box Right', 'aa-base' ),
		'id'            => 'footer-box-right',
		'description'   => esc_html__( 'Add widgets here.', 'aa-base' ),
		'before_widget' => '<div id="%1$s" class="footer-box">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="box-title">',
		'after_title'   => '</div>',
		'before_text'   => '<div class="text">',
		'after_text'    => '</div>',
		'before_link'   => '<a href="',
		'after_link'    => '">',
		'link_close'    => '</a>',
	) );

}

add_action( 'widgets_init', 'ss_base_widgets_init' );


// Creating the widget
class wpb_widget extends WP_Widget {

	function __construct() {
		parent::__construct(

		// Base ID of your widget
			'wpb_widget',

			// Widget name will appear in UI
			__( 'Footer Box', 'wpb_widget_domain' ),

			// Widget description
			array( 'description' => __( 'A single box for the footer box area', 'wpb_widget_domain' ), )
		);
	}

	// Creating widget front-end

	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$text  = apply_filters( 'widget_text', $instance['text'] );
		$link  = apply_filters( 'widget_link', $instance['link'] );

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];


		if ( ! empty( $link ) ) {
			echo $args['before_link'] . $link . $args['after_link'];
		}

		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}


		if ( ! empty( $text ) ) {
			echo $args['before_text'] . $text . $args['after_text'];
		}

		// This is where you run the code and display the output

		echo $args['link_close'];

		echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {

		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}

		if ( isset( $instance['text'] ) ) {
			$text = $instance['text'];
		} else {
			$text = __( 'New text', 'wpb_widget_domain' );
		}

		if ( isset( $instance['link'] ) ) {
			$link = $instance['link'];
		} else {
			$link = __( 'New link', 'wpb_widget_domain' );
		}


		// Widget admin form
		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
                   name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
                   value="<?php echo esc_attr( $title ); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Text:' ); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>"
                      name="<?php echo $this->get_field_name( 'text' ); ?>" rows="9">
   <?php echo esc_attr( $text ); ?>
   </textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'Link:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>"
                   name="<?php echo $this->get_field_name( 'link' ); ?>" type="text"
                   value="<?php echo esc_attr( $link ); ?>"
            />
        </p>
		<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance          = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['text']  = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
		$instance['link']  = ( ! empty( $new_instance['link'] ) ) ? strip_tags( $new_instance['link'] ) : '';

		return $instance;
	}

	// Class wpb_widget ends here
}


// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}

add_action( 'widgets_init', 'wpb_load_widget' );


function dequeue_jquery_migrate( $scripts ) {
	if ( ! is_admin() && ! empty( $scripts->registered['jquery'] ) ) {
		$scripts->registered['jquery']->deps = array_diff(
			$scripts->registered['jquery']->deps,
			[ 'jquery-migrate' ]
		);
	}
}

add_action( 'wp_default_scripts', 'dequeue_jquery_migrate' );


function sso_include_jquery() {
    if (!is_admin()) {
        wp_deregister_script('jquery'); 
        wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/jquery/jquery.min.js', array(), '1.12.3', false );
    }
}
add_action('wp_enqueue_scripts', 'sso_include_jquery', 1);


//Remove Gutenberg Block Library CSS from loading on the frontend
function smartwp_remove_wp_block_library_css() {
	if ( ! is_admin() ) {
		wp_dequeue_style( 'wp-smart-crop-renderer' );
	}

}

add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css' );


//Include the additional PHP scripts
require get_template_directory() . '/inc/disable-comments.php';
require get_template_directory() . '/inc/header-functions.php';
require get_template_directory() . '/inc/template-functions.php';



/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Rename Posts to News
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

// Remove tags from Posts
/* add_action( 'init', 'unregister_post_tags', 50 );
function unregister_post_tags() {
    register_taxonomy( 'post_tag', array( 'news' ) );
} */

/*------------------------------------*\
    Add additional columns to post view
    CSI 25.05.2018
    https://code.tutsplus.com/articles/add-a-custom-column-in-posts-and-custom-post-types-admin-screen--wp-24934
\*------------------------------------*/
// GET FEATURED IMAGE
function ST4_get_featured_image($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');
        return $post_thumbnail_img[0];
    }
}

// ADD NEW COLUMN
function ST4_columns_head($defaults) {
    $defaults['featured_image'] = 'Featured Image';
    return $defaults;
}
 
// SHOW THE FEATURED IMAGE
function ST4_columns_content($column_name, $post_ID) {
    if ($column_name == 'featured_image') {
        $post_featured_image = ST4_get_featured_image($post_ID);
        if ($post_featured_image) {
            echo '<img src="' . $post_featured_image . '" />';
        }
    }
}


/*------------------------------------*\
    Remove WP Menu Items
\*------------------------------------*/

function remove_menus(){

  //remove_menu_page( 'index.php' );                  //Dashboard
  //remove_menu_page( 'edit.php' );                   //Posts
  //remove_menu_page( 'upload.php' );                 //Media
  //remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  //remove_menu_page( 'themes.php' );                 //Appearance
  //remove_menu_page( 'plugins.php' );                //Plugins
  //remove_menu_page( 'users.php' );                  //Users
  //remove_menu_page( 'tools.php' );                  //Tools
  //remove_menu_page( 'options-general.php' );        //Settings

}
add_action( 'admin_menu', 'remove_menus' );

/**
 * Hide ACF menu item from the admin menu
 */

function remove_acf_menu()
{

    // provide a list of usernames who can edit custom field definitions here
    $admins = array(
        'admin','claus','oa','Pageone', 'WPML admin'
    );

    // get the current user
    $current_user = wp_get_current_user();

    // match and remove if needed
    if( !in_array( $current_user->user_login, $admins ) )
    {
        remove_menu_page('edit.php?post_type=acf');
        remove_menu_page('wpseo_dashboard');
        remove_menu_page('sitepress-multilingual-cms/menu/languages.php');
         remove_menu_page('customize.php');
        remove_submenu_page ('themes.php', 'themes.php');
         remove_submenu_page ('themes.php', 'theme-editor.php');
         remove_submenu_page ('index.php', 'update-core.php');
    }

}


//Below commented by CSI 29.01.2018 to allow access to icanlocalize service
add_action( 'admin_menu', 'remove_acf_menu', 999 );


// Remove Widgets

function remove_widgets() {
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Search');
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Tag_Cloud');
}

add_action( 'widgets_init', 'remove_widgets' );

// Change footer text
function remove_footer_admin () {
  echo '<em>Developed by <a href="http://udret.dk">UDRET</a> for Carmo A/S</em>';
}
add_filter('admin_footer_text', 'remove_footer_admin');

// WPML fixes

// Don't load WPML CSS
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);


/*------------------------------------*\
    Enable Wistia oEmbed
\*------------------------------------*/
//https://wistia.com/support/integrations/wordpress
//wp_oembed_add_provider( '/https?:\/\/(^.)+\.(wistia\.com|wi\.st)\/.*/', 'http://fast.wistia.com/oembed', true);
wp_oembed_add_provider( '/https?:\/\/(.+)?(wistia.com|wi.st)\/(medias|embed)\/.*/', 'http://fast.wistia.com/oembed', true);



/*------------------------------------*\
    ACF GoogleMaps
\*------------------------------------*/
function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyCEiW1T40agMRFouisK6korajQMKwJHTq8';
	
	return $api;
	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

/*------------------------------------*\
    Gravity Forms
\*------------------------------------*/

// Graviyyform add ability to hide label
//https://chriseggleston.com/use-gravity-forms-placeholders/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );


/**
 * Add all Gravity Forms capabilities to Editor role.
 * Runs when this theme is activated.
 *
 * https://ryanbenhase.com/giving-editors-access-to-gravity-forms/
 * 
 * @access public
 * @return void
 */
function grant_gforms_editor_access() {
  
  $role = get_role( 'editor' );
  $role->add_cap( 'gravityforms_view_entries' );
}
// Tie into the 'after_switch_theme' hook
add_action( 'after_switch_theme', 'grant_gforms_editor_access' );

/**
 * Remove Gravity Forms capabilities from Editor role.
 * Runs when this theme is deactivated (in favor of another).
 * 
 * @access public
 * @return void
 */
function revoke_gforms_editor_access() {
  
  $role = get_role( 'editor' );
  $role->remove_cap( 'gravityforms_view_entries' );
}
// Tie into the 'switch_theme' hook
add_action( 'switch_theme', 'revoke_gforms_editor_access' );

// List countries as 2 letter ISO to facilitate Mailchimp transfer
// https://docs.gravityforms.com/gform_countries/
/*
add_filter( 'gform_countries', function ( $countries ) {
    $new_countries = array();
 
    foreach ( $countries as $country ) {
        $code                   = GF_Fields::get( 'address' )->get_country_code( $country );
        $new_countries[ $code ] = $country;
    }
 
    return $new_countries;
} );
*/

/*------------------------------------*\
    Allow shortcodes in sidebar
    Used to allow wp download lists in left sidebars (ACF TextArea)
    https://wpfixit.com/wordpress-shortcodes-in-sidebar/
\*------------------------------------*/
function my_acf_format_value( $value, $post_id, $field ) {
    
    // run do_shortcode on all textarea values
    if (!is_admin()) {$value = do_shortcode($value);}
    
    
    // return
    return $value;
}


/*------------------------------------*\
    Breadcrumbs
    https://www.thewebtaylor.com/articles/wordpress-creating-breadcrumbs-without-a-plugin
\*------------------------------------*/
// Breadcrumbs
// itemprop="item" itemid="https://example.com/books/sciencefiction"
function the_breadcrumb_seo() {
    global $product; //inherit $product from wpdb call in template-item
       
    // Settings
    $separator          = '&gt;';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = __('Homepage','html5blank');
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
	
	$position = 1;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ol id="' . $breadcrums_id . '" class="' . $breadcrums_class . '" itemscope itemtype="http://schema.org/BreadcrumbList">';
           
        // Home page
        echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="item-home"><img src="'.get_template_directory_uri() . '/assets/images/icons/home_u917.svg' .'" ><a itemscope itemtype="http://schema.org/Thing" itemprop="item" itemid="' . home_url('/') . '" class="bread-link bread-home" href="' . home_url('/') . '" title="' . $home_title . '">' . '<span itemprop="name">' . $home_title . '</span></a><meta itemprop="position" content="'.$position++.'"/></li>';
        //echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a itemscope itemtype="http://schema.org/Thing" itemprop="item" class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                //echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    //$cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                    //echo('.');
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                //echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
        

        } else if ( is_page_template( 'template-item.php' ) ) {
            // Item page based on virtual information outside WordPress post
            // Retrieves breadcrumb based on the category data from the webcategory parameter of the dessin number
            //echo (' This is template-item');
            global $product;

            $exclude = ['gb','de','fr', 'da'];
            //$link ="https://www.carmo.dk";
            $link = rtrim(site_url(),"/");

            $anc = explode('/',trim($product['webfolder']," /")); 
            $parents='';

            foreach ( $anc as $ancestor ) {
                $link .= '/'.$ancestor;
                if (in_array($ancestor, $exclude) === false) {
                    //echo $ancestor;
                    // Beautify the string
                    $ancestor = ucfirst(str_replace("-"," ",$ancestor));
                    //Build the entry
                    $parents .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="item-parent item-parent-' . $ancestor . '"><a itemscope itemtype="http://schema.org/Thing" itemprop="item" class="bread-parent bread-parent-' . $ancestor . '" href="' . $link . '/" title="' . $ancestor . '">' . '<span itemprop="name">' . $ancestor . '</span></a><meta itemprop="position" content="'.$position++.'" /></li>';
                    //$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
            } 

            // Display parent pages
            echo $parents;
                   
            // Current page
            echo ('<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="item-current item-' . $product['item'] . '"><span itemscope itemtype="http://schema.org/Thing" itemprop="item" title="' . $product['item'] . '" id="' . $product['item'] . '">' . '<span itemprop="name">' .$product['item'] . '</span></span><meta itemprop="position" content="'.$position++.'" /></li>');


        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="item-parent item-parent-' . $ancestor . '"><a itemscope itemtype="http://schema.org/Thing" itemprop="item" itemid="' . get_permalink($ancestor) . '" class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . ( get_field('breadcrumb_title',$ancestor) ?? get_the_title($ancestor) ) . '">' . '<span itemprop="name">' . ( get_field('breadcrumb_title',$ancestor) ?? get_the_title($ancestor) ) . '</span></a><meta itemprop="position" content="'.$position++.'" /></li>';
					//$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="item-current item-' . $post->ID . '"><span itemscope itemtype="http://schema.org/Thing" itemprop="item" itemid="' . get_permalink($ancestor) . '"title="' . (get_field('breadcrumb_title') ?? get_the_title() ) . '"><span itemprop="name">' . (get_field('breadcrumb_title') ?? get_the_title() ) . '</span></span><meta itemprop="position" content="'.$position++.'" /></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . (get_field('breadcrumb_title') ?? get_the_title() ) . '</strong><meta itemprop="position" content="'.$position++.'" /></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ol>';
           
    }
       
}

function the_breadcrumb() {
    global $product; //inherit $product from wpdb call in template-item
       
    // Settings
    $separator          = '&gt;';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = __('Homepage','html5blank');
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
	
	$position = 1;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ol id="' . $breadcrums_id . '" class="' . $breadcrums_class . '" >';
           
        // Home page
        //echo '<li class="item-home"><img src="'.get_template_directory_uri() . '/assets/images/icons/home_u917.svg' .'" ><a class="bread-link bread-home" href="' . home_url('/') . '" title="' . $home_title . '">' . '<span>' . $home_title . '</span></a></li>';
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . home_url('/') . '" title="' . $home_title . '">' . '<span>' . $home_title . '</span></a></li>';

        //echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a itemscope itemtype="http://schema.org/Thing" itemprop="item" class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                //echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    //$cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                    //echo('.');
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                //echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
        

        } else if ( is_page_template( 'template-item.php' ) ) {
            // Item page based on virtual information outside WordPress post
            // Retrieves breadcrumb based on the category data from the webcategory parameter of the dessin number
            //echo (' This is template-item');
            global $product;

            $exclude = ['gb','de','fr', 'da','en'];
            //$link ="https://www.carmo.dk";
            $link = rtrim(site_url(),"/");

            $anc = explode('/',trim($product['webfolder']," /")); 
            $parents='';

            foreach ( $anc as $ancestor ) {
                $link .= '/'.$ancestor;
                if (in_array($ancestor, $exclude) === false) {
                    //echo $ancestor;
                    // Beautify the string
                    $ancestor = ucfirst(str_replace("-"," ",$ancestor));
                    //Build the entry
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . $link . '/" title="' . $ancestor . '">' . '<span>' . $ancestor . '</span></a></li>';
                    //$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
            } 

            // Display parent pages
            echo $parents;
                   
            // Current page
            echo ('<li class="item-current item-' . $product['item'] . '"><span>' . '<span>' .$product['item'] . '</span></span></li>');


        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . ( get_field('breadcrumb_title',$ancestor) ?? get_the_title($ancestor) ) . '">' . '<span>' . ( get_field('breadcrumb_title',$ancestor) ?? get_the_title($ancestor) ) . '</span></a></li>';
					//$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><span><span>' . (get_field('breadcrumb_title') ?? get_the_title() ) . '</span></span></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . (get_field('breadcrumb_title') ?? get_the_title() ) . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ol>';
           
    }
       
}

function is_medical() {
    return (preg_match('/medical-components|composants-medicaux|medizinische-komponenten|medico/', $_SERVER['REQUEST_URI'])===1 ? true : false );
	//return (preg_match('/medical-components|medicaux|medizinische-komponenten/', $_SERVER['REQUEST_URI'])===1 ? true : false );
}

function is_technical() {
    return ( (preg_match('/technical-components|composants-techniques|technische-komponenten|teknisk/', $_SERVER['REQUEST_URI'])===1 && !is_medical() ) ? true : false );

	//return ( (preg_match('/products|produits|produkte/', $_SERVER['REQUEST_URI'])===1 && !is_medical() ) ? true : false );
}





/*------------------------------------*\
    Custom WordPress Login Logo
\*------------------------------------*/

function change_my_wp_login_image() {
    echo "
    <style>
    body.login #login h1 a {
    background: url('".get_bloginfo('template_url')."/img/logo.png') top center no-repeat transparent;
    height:73px;
    width:320px; }
    </style>
    ";
    }
    add_action("login_head", "change_my_wp_login_image");
    
    
    
    /*------------------------------------*\
        Item template
    \*------------------------------------*/
    
     //   Fixes for making template-item work with customproprietary datastructure
    
    
    
    function pmg_rewrite_activation() {

		// Updated 09.04.2021 CSI - 'products' section removed


    	add_rewrite_rule( '^(?!\/(thumbs|productsheet)?).*(medical-components|technical-components|composants-medicaux|composants-techniques|medizinische-komponenten|technische-komponenten|machines-tools-hf-welding|machines-outils-soudure-hf|maschinen-hf-schweisen|hf-svejsning|teknisk|medico).*(\d{2,2}-\d{3,3})\/?$', 'index.php?pagename=item-template&dessin=$matches[3]/','top' );

    	//add_rewrite_rule( '^(?!\/(thumbs|productsheet)?).*(products|produkte|produits).*(\d{2,2}-\d{3,3})\/?$', 'index.php?pagename=item-template&dessin=$matches[3]/','top' );
        
        //add_rewrite_rule( '^(?!\/(thumbs|productsheet)?).*(products|de\/produkte|fr\/produits).*(\d{2,2}-\d{3,3})\/?$', 'index.php?page_id=2694&dessin=$matches[3]','top' );
        //add_rewrite_rule( '^(?!\/(thumbs|productsheet)?).*(products).*(\d{2,2}-\d{3,3})\/?$', 'index.php?pagename=item-template&dessin=$matches[3]','top' );
        //add_rewrite_rule( '^(?!\/(thumbs|productsheet)?).*(de\/produkte).*(\d{2,2}-\d{3,3})\/?$', '/de/index.php?pagename=item-template&dessin=$matches[3]','top' );
        //add_rewrite_rule( '^(?!\/(thumbs|productsheet)?).*(fr\/produits).*(\d{2,2}-\d{3,3})\/?$', '/fr/index.php?pagename=item-template&dessin=$matches[3]','top' );
        //add_rewrite_rule( '(products|de\/produkte|fr\/produits).*(\d{2,2}-\d{3,3})\/?$', 'index.php?pageid=2694&dessin=$matches[3]','top' );
        //add_rewrite_rule( '^.*\/(\d{2,2}-\d{3,3})\/?$', 'index.php?pagename=item-template&dessin=$matches[3]','top' );
    }
    add_action('init', 'pmg_rewrite_activation', 10, 0);
    
    function custom_carmo_rewrite_tag() {
        add_rewrite_tag('%op%', '(crawl)');
        add_rewrite_tag('%dessin%', '(\d{2}-\d{3})');
    }
    add_action('init', 'custom_carmo_rewrite_tag', 10, 0);
    
    /**
     * FIX problem with WPML returning 404 Error on index.php?pagename=item-template&dessin=12-345
     * Solution found here: https://wpml.org/forums/topic/rewrite-rule/
     */
    add_filter( 'wpml_is_redirected', 'wpmlcore_4704_block_redirect', 10, 3 );
    function wpmlcore_4704_block_redirect( $redirect, $post_id, $q ) {
        if ( isset( $q->query_vars['pagename'] ) && 'item-template' === $q->query_vars['pagename'] ) {
            return false;
        }
        return $redirect;
    }
    
    /* ONLY WORKS ON FRENCH/GERMAN PAGES 
     * Fix WPML language switcher not working on item pages */
    add_filter( 'WPML_filter_link', 'custom_filter_link', 10, 2 );
    function custom_filter_link( $lang_url, $lang ) {
        $dessin = get_query_var("dessin");
        if(isset($dessin)) {
            global $product;
            if (isset($product) and array_key_exists("item",$product)) {
                switch ($lang["code"]) {
                	case "da":
                		$lang_url = $product['webfolderda'] . $product['item'].'/';
                        break;
                    case "de":
                        $lang_url = $product['webfolderde'] . $product['item'].'/';
                        break;
                    case "fr":
                        $lang_url = $product['webfolderfr'] . $product['item'].'/';
                        break;
                    default:
                        $lang_url = $product['webfolderuk'] . $product['item'].'/';
                        break;
                }
            }
            /*$tmp_language = apply_filters( 'wpml_current_language', NULL );
            do_action( 'wpml_switch_language', 'fr' );
            $product_url = preg_replace('(\d{2}-\d{3}/)', '', $_SERVER[REQUEST_URI]);
            $product_id = url_to_postid($p_url);
            // switch language to get the right permalink
            do_action( 'wpml_switch_language', $lang['code'] );
            $product_url = get_permalink($p_id);
            $my_url = apply_filters( 'wpml_permalink', $product_url, $lang['code'] );
            // switch back to original language
            do_action( 'wpml_switch_language', $tmp_language );
            return $my_url . $dessin;*/
        }
        return $lang_url;
    }
    
    // Get the dessin number before rewrites
    add_action( 'init', 'get_dessin' );
    function get_dessin() {
        global $dessinnumber;
    
         if( isset( $_GET['dessin'] ) ) {
            if (preg_match("/^\d{2,2}-\d{3,3}$/", $_GET['dessin'])) $dessinnumber = $_GET['dessin'];
         }
    }
    
    
    
    // SEO Yoast override for Item template (draws data from standalone tables)

    	// SEO Breadcrumbs
    
		add_filter( 'wpseo_breadcrumb_links', 'wpseo_breadcrumb_remove_limited' );
		function wpseo_breadcrumb_remove_limited( $breadcrumbs ) {

		    if ( is_page_template( 'template-item.php' )) {
				// Item page based on virtual information outside WordPress post
	            // Retrieves breadcrumb based on the category data from the webcategory parameter of the dessin number
	            //echo (' This is template-item');
	            global $product;

	            $exclude = ['gb','de','fr','da'];
	            //$link ="https://www.carmo.dk";
	            $link = rtrim(site_url(),"/");


	            $anc = explode('/',trim($product['webfolder']," /")); 
	            $parents='';
	            $newbreadcrumbs[] = array(
	            	'url' => site_url(),
				    'text' => "Homepage"
	            );


	            foreach ( $anc as $ancestor ) {
	                $link .= '/'.$ancestor;
	                if (in_array($ancestor, $exclude) === false) {
	                    //echo $ancestor;
	                    // Beautify the string
	                    $ancestor = ucfirst(str_replace("-"," ",$ancestor));
	                    //Build the entry

                        array_push($newbreadcrumbs, array(
				            'url' => $link . '/',
				            'text' => $ancestor
				        	)
                    	);
	                 
	                }
	            } 

				array_push($newbreadcrumbs, array(
				            'url' => $link . '/' . $product['item'] . '/',
				            'text' => $product['item']
				        	)
                    	);

	            //var_dump($newbreadcrumbs); die();
	            return $newbreadcrumbs;
		    }
			else {
				return $breadcrumbs;
			}
		}

		//Page Title
		function template_item_add_title( $str ) {
            if (is_page_template( 'template-item.php' )) {
                global $product;
                return strip_tags(htmlspecialchars($product['webmetatitle']));
            } else return $str;
        }
		add_filter( 'wp_title', 'template_item_add_title', 10, 2 );


    
        // Meta Title
        function yoast_add_title( $str ) {
            if (is_page_template( 'template-item.php' )) {
                global $product;
                return strip_tags(htmlspecialchars($product['webmetatitle']));
            } else return $str;
        }
        add_filter( 'wpseo_title', 'yoast_add_title' );
    
        // Meta Description
        function yoast_add_description( $str ) {
            if (is_page_template( 'template-item.php' )) {
                global $product;
                return strip_tags(htmlspecialchars($product['webmetadescription']));
            } else return $str;
        }
        add_filter( 'wpseo_metadesc', 'yoast_add_description');
    
    
        // Meta Keywords
        /*
        function yoast_add_keywords( $str ) {
            if (is_page_template( 'template-item.php' )) {
                return $str.',yoast';
            }
        }
        add_filter( 'wpseo_metakey', 'yoast_add_keywords');
        */
    
    
        // Canonical
        function yoast_add_canonical( $str ) {
            if (is_page_template( 'template-item.php' )) {
                global $product;
                return site_url().$product['webfolder'].$product['item'].'/';
            }  else return $str;
        }
        add_filter( 'wpseo_canonical', 'yoast_add_canonical', 10, 1 );
    
        // og:url
        function yoast_add_og_url( $str ) {
            if (is_page_template( 'template-item.php' )) {
                global $product;
                return site_url().$product['webfolder'].$product['item'].'/';
            }  else return $str;
        }
        add_filter( 'wpseo_opengraph_url', 'yoast_add_og_url', 10, 1 );
    
    

    
        // **** og:Title ****
        function yoast_add_og_title( $str ) {
            if (is_page_template( 'template-item.php' )) {
                global $product;
                return strip_tags(htmlspecialchars($product['webmetatitle']));
            } else return $str;
        }
        add_filter('wpseo_opengraph_title', 'yoast_add_og_title' );
    
        // **** og:type ****
        function yoast_add_og_type( $str ) {
            if (is_page_template( 'template-item.php' )) {
                //return $str.',product.item';
                return 'product.item';
            }  else return $str;
        }
        add_filter( 'wpseo_opengraph_type', 'yoast_add_og_type', 11, 1 );
    
        // **** og:image ****
        function yoast_add_og_image( $str ) {
            if (is_page_template( 'template-item.php' )) {
                global $product;
                return site_url().'/thumbs/600x600'.$product['webfolder'].filenamesafe($product['webname']).'_'.$product['item'].'.jpg';
            } elseif (is_page_template( 'template-product.php' )){
                global $row;
                return site_url().'/thumbs/600x600xZC'.$row['new_path'].sanitize_file_name($row['imagename']).'-('.$row['categoryid'].').jpg'; 
            } else return $str;
        }
        add_filter( 'wpseo_opengraph_image', 'yoast_add_og_image');
    
    
        // **** og:description ****
        function yoast_add_og_description( $str ) {
            if (is_page_template( 'template-item.php' )) {
                global $product;
                return strip_tags(htmlspecialchars($product['webmetadescription']));
            }  else return $str;
        }
        add_filter('wpseo_opengraph_desc', 'yoast_add_og_description' );
    
    
        // **** add custom code at end of header ****
        function yoast_add_to_head( $str ) {
            if (is_page_template( 'template-item.php' )) {
                //echo ('*****************');
            }
        }           
        //add_filter( 'wpseo_head', 'yoast_add_to_head', 11, 1 );
    
         // **** HREF LANG *****
        function my_custom_head_langs( $url ) { 
            if (is_page_template( 'template-item.php' )) : global $product; ?>
            	<link rel="alternate" hreflang="da-DK" href="<?=site_url().$product['webfolderda'].$product['item']?>/" />
                <link rel="alternate" hreflang="en-US" href="<?=site_url().$product['webfolderuk'].$product['item']?>/" />
                <link rel="alternate" hreflang="de-DE" href="<?=site_url().$product['webfolderde'].$product['item']?>/" />
                <link rel="alternate" hreflang="fr-FR" href="<?=site_url().$product['webfolderfr'].$product['item']?>/" />
                <link rel="alternate" hreflang="x-default" href="<?=site_url().$product['webfolderuk'].$product['item']?>/" /><?php
            else : return $url;
            endif; 
        }
        add_filter( 'wpml_head_langs', 'my_custom_head_langs', 10, 2 );


    /* ---------------------------------------------------------------------------
     * Manage Yoast json-ld graph
     * --------------------------------------------------------------------------- */
    add_filter( 'disable_wpseo_json_ld_search', 'yoast_disable_json_ld_search' );
	function yoast_disable_json_ld_search( $profiles ) {
	    return true;
	}
    
    
    /* ---------------------------------------------------------------------------
     * Set hreflang="x-default" with WPML
     * --------------------------------------------------------------------------- */
    add_filter('wpml_alternate_hreflang', 'wps_head_hreflang_xdefault', 10, 2);
    function wps_head_hreflang_xdefault($url, $lang_code) {
          
        if($lang_code == 'en') : ?>
            <link rel="alternate" hreflang="x-default" href="<?=$url ?>" /><?php
        endif;
          
        return $url;
    }

    function wps_head_hreflang_xdefault_en($url, $lang_code) {
    	echo ('<link rel="alternate" hreflang="x-default" href="'.apply_filters( 'wpml_permalink', $url , 'en' ).'" >');
        return $url;
    }


    /*------------------------------------*\
        Cleanup Header - https://crunchify.com/how-to-clean-up-wordpress-header-section-without-any-plugin/
    \*------------------------------------*/
    remove_action( 'wp_head', 'wp_shortlink_wp_head');
	remove_action( 'wp_head', 'rsd_link');
	remove_action( 'wp_head', 'wlwmanifest_link');
	remove_action( 'wp_head', 'rest_output_link_wp_head', 10);
	remove_action( 'template_redirect', 'rest_output_link_header', 11, 0);

	//remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);

	function crunchify_remove_version() {
		return '';
	}
	add_filter('the_generator', 'crunchify_remove_version');

	








    
// Custom Excerpts
function html5wp_index($length) // Create 15 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 13 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 13;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}
    

add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}